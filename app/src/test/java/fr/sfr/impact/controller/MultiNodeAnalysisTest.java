package fr.sfr.impact.controller;

import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.dto.NodeDto;
import fr.sfr.impact.dto.NodeDtoSubset;
import fr.sfr.impact.service.bottomup.BottomUpResultService;
import org.junit.Test;
import org.neo4j.graphdb.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public abstract class MultiNodeAnalysisTest extends AbstractScenarioTest {

    @Autowired
    protected BottomUpResultService service;

    protected abstract Set<Long> getNodesToAnalyze();

    protected void verifyImpactedClients(Node... expected) {
        NodeDtoSubset actual = service.getImpactedClients(getNodesToAnalyze(), PagingCriteria.EVERYTHING);
        assertEquals(expected.length, actual.getTotalNumber());
        verify(actual.getDtos(), expected);
    }

    protected void verifyImpactedServices(NodeDto... expected) {
        NodeDtoSubset actual = service.getImpactedServices(getNodesToAnalyze(), PagingCriteria.EVERYTHING);
        assertEquals(expected.length, actual.getTotalNumber());
        verify(actual.getDtos(), expected);
    }

    protected void verifyNodesOnPath(Node... expected) {
        NodeDtoSubset actual = service.getNodesOnPath(getNodesToAnalyze(), PagingCriteria.EVERYTHING);
        assertEquals(expected.length, actual.getTotalNumber());
        verifyIgnoringOrder(actual.getDtos(), expected);
    }

    protected void verifyServicesForClient(final Node client, NodeDto... expected) {
        NodeDtoSubset actual = service.getImpactedServicesForClient(getNodesToAnalyze(), client.getId(), PagingCriteria.EVERYTHING);
        assertEquals(expected.length, actual.getTotalNumber());
        verify(actual.getDtos(), expected);
    }
}
