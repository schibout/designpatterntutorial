package fr.sfr.impact.controller;

import fr.sfr.impact.dto.NodeDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import static fr.sfr.impact.domain.Constants.*;
import static fr.sfr.impact.domain.bottomup.NodeStatus.KO;
import static fr.sfr.impact.domain.bottomup.NodeStatus.OK;
import static fr.sfr.impact.domain.bottomup.ServiceStatus.NOT_SECURE;
import static fr.sfr.impact.test.PrePopulatedDatabase.*;

@ContextConfiguration(locations = {"classpath:services.xml", "classpath:test-database.xml", "classpath:test-controllers.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class MultiImpactScenario2Test extends MultiNodeAnalysisTest {

    @Override
    protected Set<Long> getNodesToAnalyze() {
        return new TreeSet<Long>(Arrays.asList(db.node(CARD, 14).getId(), db.node(AU4, 75).getId()));
    }

    @Test
    public void verifyImpactedClients() {
        verifyImpactedClients(db.node("A Client", 1), db.node(CLIENT, 100));
    }

    @Test
    public void verifyServicesAndServicesForClient() {
        NodeDto serviceDto1 = zzServiceDto("A Client", 1, NOT_SECURE);
        NodeDto circuitDto1 = circuit3Dto("A Client", 1, NOT_SECURE);
        NodeDto primary1 = rcMain5Dto("A Client", 1, KO);
        NodeDto secondary1 = rcSpare6Dto("A Client", 1, OK);

        NodeDto serviceDto2 = service80Dto(CLIENT, 100, NOT_SECURE);
        NodeDto circuitDto2 = circuit63Dto(CLIENT, 100, NOT_SECURE);
        NodeDto primary2 = rcMain60Dto(CLIENT, 100, OK);
        NodeDto secondary2 = rcSpare62Dto(CLIENT, 100, KO);

        NodeDto serviceDto3 = zzServiceDto(CLIENT, 100, NOT_SECURE);
        NodeDto circuitDto3 = circuit3Dto(CLIENT, 100, NOT_SECURE);
        NodeDto primary3 = rcMain5Dto(CLIENT, 100, KO);
        NodeDto secondary3 = rcSpare6Dto(CLIENT, 100, OK);

        verifyServicesForClient(db.node("A Client", 1), serviceDto1, circuitDto1, primary1, secondary1);
        verifyServicesForClient(db.node(CLIENT, 100), serviceDto2, circuitDto2, primary2, secondary2, serviceDto3, circuitDto3, primary3, secondary3);
        verifyImpactedServices(serviceDto1, circuitDto1, primary1, secondary1, serviceDto2, circuitDto2, primary2, secondary2, serviceDto3, circuitDto3, primary3, secondary3);
    }

    @Test
    public void verifyNodesOnPaths() {
        verifyNodesOnPath(db.node(AU4, 11), db.node(CARD, 14), db.node(PORT, 13),
                db.node(CONDUIT, 7), db.node(ROUTECIRCUIT + MAIN, 5), db.node(ROUTECIRCUIT, 4), db.node(CIRCUIT, 3),
                db.node("ZZ Service", 2), db.node("A Client", 1), db.node(CLIENT, 100), db.node(AU4, 75), db.node(CONDUIT, 66),
                db.node(ROUTECIRCUIT + SPARE, 62), db.node(ROUTECIRCUIT, 61), db.node(CIRCUIT, 63), db.node(SERVICE, 80));
    }
}
