package fr.sfr.impact.controller;

import fr.sfr.impact.dto.NodeDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.neo4j.graphdb.Node;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static fr.sfr.impact.domain.Constants.*;
import static fr.sfr.impact.domain.bottomup.NodeStatus.KO;
import static fr.sfr.impact.domain.bottomup.NodeStatus.OK;
import static fr.sfr.impact.domain.bottomup.ServiceStatus.NOT_SECURE;
import static fr.sfr.impact.test.PrePopulatedDatabase.MAIN;
import static fr.sfr.impact.test.PrePopulatedDatabase.ROUTECIRCUIT;

@ContextConfiguration(locations = {"classpath:services.xml", "classpath:test-database-extended.xml", "classpath:test-controllers.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class SingleIncompleteExtremityBottomUpScenarioTest extends SingleNodeAnalysisEndToEndTest {

    @Override
    protected Node getNodeToAnalyze() {
        return db.node(CARD, 14);
    }

    @Test
    public void verifyImpactedClients() {
        verifyImpactedClients(db.node("A Client", 1), db.node(CLIENT, 100), db.node(CLIENT, 1005));
    }

    @Test
    public void verifyServicesAndServicesForClient() {
        NodeDto serviceDto1000 = new NodeDto(db.node(SERVICE, 1000))
                .setProperty(STATUS, KO.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId("A Client", 1));

        NodeDto conduitDto7 = new NodeDto(db.node(CONDUIT, 7))
                .setProperty(STATUS, KO.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId("A Client", 1))

                .setIdAndNeoId(AU4, A, idAndNeoId(AU4, 11))
                .setNameAndNeoId(PORT, A, nameAndNeoId(PORT, 13))
                .setNameAndNeoId(CARD, A, nameAndNeoId(CARD, 14))
                .setNameAndNeoId(EQUIPMENT, A, nameAndNeoId(EQUIPMENT, 15))

                .setIdAndNeoId(AU4, B, idAndNeoId(AU4, 12))
                .setNameAndNeoId(PORT, B, nameAndNeoId(PORT, 22))
                .setNameAndNeoId(CARD, B, nameAndNeoId(CARD, 23))
                .setNameAndNeoId(EQUIPMENT, B, nameAndNeoId(EQUIPMENT, 24));

        NodeDto serviceDto1 = zzServiceDto("A Client", 1, NOT_SECURE);

        NodeDto circuitDto1 = new NodeDto(db.node(CIRCUIT, 3))
                .setProperty(STATUS, NOT_SECURE.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId("A Client", 1))
                .setProperty(AU4 + U_NAME + A, "MULTIPLE");

        NodeDto primary1 = rcMain5Dto("A Client", 1, KO);
        NodeDto secondary1 = rcSpare6Dto("A Client", 1, OK);
        NodeDto serviceDto2 = zzServiceDto(CLIENT, 100, NOT_SECURE);

        NodeDto circuitDto2 = new NodeDto(db.node(CIRCUIT, 3))
                .setProperty(STATUS, NOT_SECURE.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId(CLIENT, 100))
                .setProperty(AU4 + U_NAME + A, "MULTIPLE");

        NodeDto primary2 = rcMain5Dto(CLIENT, 100, KO);
        NodeDto secondary2 = rcSpare6Dto(CLIENT, 100, OK);

        NodeDto serviceDto1002 = new NodeDto(db.node(SERVICE, 1002))
                .setProperty(STATUS, KO.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId(CLIENT, 1005));

        NodeDto conduitDto1004 = new NodeDto(db.node(CIRCUIT, 1004))
                .setProperty(STATUS, KO.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId(CLIENT, 1005))
                .setIdAndNeoId(AU4, A, idAndNeoId(AU4, 1001));

        verifyServicesForClient(db.node("A Client", 1), serviceDto1000, conduitDto7, serviceDto1, circuitDto1, primary1, secondary1);
        verifyServicesForClient(db.node(CLIENT, 100), serviceDto2, circuitDto2, primary2, secondary2);
        verifyServicesForClient(db.node(CLIENT, 1005), serviceDto1002, conduitDto1004);
        verifyImpactedServices(serviceDto1000, conduitDto7, serviceDto1, circuitDto1, primary1, secondary1, serviceDto2, circuitDto2, primary2, secondary2, serviceDto1002, conduitDto1004);
    }

    @Test
    public void verifyNodesOnPaths() {
        verifyNodesOnPath(db.node(AU4, 11), db.node(CARD, 14), db.node(PORT, 13),
                db.node(CONDUIT, 7), db.node(ROUTECIRCUIT + MAIN, 5), db.node(ROUTECIRCUIT, 4), db.node(CIRCUIT, 3),
                db.node("ZZ Service", 2), db.node("A Client", 1), db.node(CLIENT, 100),
                db.node(SERVICE, 1002), db.node(CIRCUIT, 1004), db.node(CLIENT, 1005), db.node(SERVICE, 1000), db.node(ROUTECIRCUIT, 1006));
    }
}
