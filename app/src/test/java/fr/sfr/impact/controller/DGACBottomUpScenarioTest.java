package fr.sfr.impact.controller;

import fr.sfr.impact.domain.bottomup.ServiceStatus;
import fr.sfr.impact.service.bottomup.BottomUpResultServiceImpl;
import fr.sfr.impact.dto.NodeDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static fr.sfr.impact.domain.Constants.*;
import static fr.sfr.impact.domain.bottomup.NodeStatus.KO;
import static fr.sfr.impact.domain.bottomup.NodeStatus.OK;
import static fr.sfr.impact.domain.bottomup.ServiceStatus.NOT_SECURE;
import static fr.sfr.impact.test.PrePopulatedDatabase.MAIN;
import static fr.sfr.impact.test.PrePopulatedDatabase.ROUTECIRCUIT;

/**
 * DGAC is special in the sense that even if there is a backup, its services should be KO.
 */
@ContextConfiguration(locations = {"classpath:services.xml", "classpath:test-database.xml", "classpath:test-controllers.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext (classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class DGACBottomUpScenarioTest extends SingleNodeAnalysisEndToEndTest {

    @Autowired
    private BottomUpResultServiceImpl bottomUpResultService;

    @Before
    public void changeClientNameToDGAC() {
        Transaction tx = db.beginTx();
        db.node("A Client", 1).setProperty(NAME, "DGAC");
        tx.success();
        tx.finish();
        bottomUpResultService.clearCache();
    }

    @Override
    protected Node getNodeToAnalyze() {
        return db.node(CARD, 14);
    }

    @Test
    public void verifyImpactedClients() {
        verifyImpactedClients(db.node(CLIENT, 100), db.node("A Client", 1));
    }

    @Test
    public void verifyServicesAndServicesForClient() {
        NodeDto serviceDto1 = zzServiceDto("A Client", 1, ServiceStatus.KO);
        NodeDto circuitDto1 = circuit3Dto("A Client", 1, ServiceStatus.KO);
        NodeDto primary1 = rcMain5Dto("A Client", 1, KO);
        NodeDto secondary1 = rcSpare6Dto("A Client", 1, OK);
        NodeDto serviceDto2 = zzServiceDto(CLIENT, 100, NOT_SECURE);
        NodeDto circuitDto2 = circuit3Dto(CLIENT, 100, NOT_SECURE);
        NodeDto primary2 = rcMain5Dto(CLIENT, 100, KO);
        NodeDto secondary2 = rcSpare6Dto(CLIENT, 100, OK);

        verifyServicesForClient(db.node("A Client", 1), serviceDto1, circuitDto1, primary1, secondary1);
        verifyServicesForClient(db.node(CLIENT, 100), serviceDto2, circuitDto2, primary2, secondary2);
        verifyImpactedServices(serviceDto2, circuitDto2, primary2, secondary2, serviceDto1, circuitDto1, primary1, secondary1);
    }

    @Test
    public void verifyNodesOnPaths() {
        verifyNodesOnPath(db.node(AU4, 11), db.node(CARD, 14), db.node(PORT, 13),
                db.node(CONDUIT, 7), db.node(ROUTECIRCUIT + MAIN, 5), db.node(ROUTECIRCUIT, 4), db.node(CIRCUIT, 3),
                db.node("ZZ Service", 2), db.node("A Client", 1), db.node(CLIENT, 100));
    }
}
