package fr.sfr.impact.controller;

import fr.sfr.impact.domain.bottomup.BottomUpAnalysis;
import fr.sfr.impact.domain.bottomup.NodeStatus;
import fr.sfr.impact.domain.bottomup.ServiceStatus;
import fr.sfr.impact.dto.NodeDto;
import fr.sfr.impact.test.PrePopulatedDatabase;
import org.neo4j.graphdb.Node;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

import static fr.sfr.impact.domain.Constants.*;
import static fr.sfr.impact.domain.Constants.ID;
import static fr.sfr.impact.test.PrePopulatedDatabase.*;
import static org.junit.Assert.assertEquals;

public abstract class AbstractScenarioTest {

    @Autowired
    protected PrePopulatedDatabase db;

    protected BottomUpAnalysis result;

    protected void verify(List<NodeDto> actual, Node... expected) {
        List<NodeDto> expectedDtos = new LinkedList<NodeDto>();
        for (Node node : expected) {
            expectedDtos.add(new NodeDto(node));
        }

        assertEquals(expectedDtos, actual);
        assertEquals(expectedDtos.size(), actual.size());
    }

    protected void verifyIgnoringOrder(List<NodeDto> actual, Node... expected) {
        Set<NodeDto> expectedDtos = new HashSet<NodeDto>();
        for (Node node : expected) {
            expectedDtos.add(new NodeDto(node));
        }

        Set<NodeDto> nodes = new HashSet<NodeDto>(actual);

        assertEquals(expectedDtos, nodes);
        assertEquals(expectedDtos.size(), actual.size());
    }

    protected void verify(List<NodeDto> actual, NodeDto... expected) {
        List<NodeDto> expectedDtos = new LinkedList<NodeDto>();
        Collections.addAll(expectedDtos, expected);

        assertEquals(expectedDtos, actual);
        assertEquals(expectedDtos.size(), actual.size());
    }

    protected String id(String name, long id) {
        return db.node(name, id).getProperty(ID).toString();
    }

    protected String name(String name, long id) {
        return db.node(name, id).getProperty(NAME).toString();
    }

    protected String neoId(String name, long id) {
        return String.valueOf(db.node(name, id).getId());
    }

    protected String[] nameAndNeoId(String name, long id) {
        return new String[]{name(name, id), neoId(name, id)};
    }

    protected String[] idAndNeoId(String name, long id) {
        return new String[]{id(name, id), neoId(name, id)};
    }

    protected NodeDto rcSpare6Dto(String clientName, int clientId, NodeStatus nodeStatus) {
        return new NodeDto(db.node(ROUTECIRCUIT + SPARE, 6))
                .setProperty(PARTAGE, SECONDARY)
                .setProperty(STATUS, nodeStatus.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId(clientName, clientId));
    }

    protected NodeDto rcSpare62Dto(String clientName, int clientId, NodeStatus nodeStatus) {
        return new NodeDto(db.node(ROUTECIRCUIT + SPARE, 62))
                .setProperty(PARTAGE, SECONDARY)
                .setProperty(STATUS, nodeStatus.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId(clientName, clientId));
    }

    protected NodeDto rcMain5Dto(String clientName, int clientId, NodeStatus nodeStatus) {
        return new NodeDto(db.node(ROUTECIRCUIT + MAIN, 5))
                .setProperty(PARTAGE, PRIMARY)
                .setProperty(STATUS, nodeStatus.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId(clientName, clientId));
    }

    protected NodeDto rcMain60Dto(String clientName, int clientId, NodeStatus nodeStatus) {
        return new NodeDto(db.node(ROUTECIRCUIT + MAIN, 60))
                .setProperty(PARTAGE, PRIMARY)
                .setProperty(STATUS, nodeStatus.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId(clientName, clientId));
    }

    protected NodeDto circuit3Dto(String clientName, int clientId, ServiceStatus serviceStatus) {
        return new NodeDto(db.node(CIRCUIT, 3))
                .setProperty(STATUS, serviceStatus.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId(clientName, clientId))

                .setIdAndNeoId(AU4, A, idAndNeoId(AU4, 18))
                .setNameAndNeoId(PORT, A, nameAndNeoId(PORT, 17))
                .setNameAndNeoId(CARD, A, nameAndNeoId(CARD, 16))
                .setNameAndNeoId(EQUIPMENT, A, nameAndNeoId(EQUIPMENT, 15))

                .setIdAndNeoId(AU4, B, idAndNeoId(AU4, 34))
                .setNameAndNeoId(PORT, B, nameAndNeoId(PORT, 33))
                .setNameAndNeoId(CARD, B, nameAndNeoId(CARD, 32))
                .setNameAndNeoId(EQUIPMENT, B, nameAndNeoId(EQUIPMENT, 31));
    }

    protected NodeDto circuit63Dto(String clientName, int clientId, ServiceStatus serviceStatus) {
        return new NodeDto(db.node(CIRCUIT, 63))
                .setProperty(STATUS, serviceStatus.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId(clientName, clientId))

                .setIdAndNeoId(AU4, A, idAndNeoId(AU4, 65))
                .setNameAndNeoId(PORT, A, nameAndNeoId(PORT, 64))
                .setNameAndNeoId(CARD, A, nameAndNeoId(CARD, 63))
                .setNameAndNeoId(EQUIPMENT, A, nameAndNeoId(EQUIPMENT, 31))

                .setIdAndNeoId(AU4, B, idAndNeoId(AU4, 69))
                .setNameAndNeoId(PORT, B, nameAndNeoId(PORT, 68))
                .setNameAndNeoId(CARD, B, nameAndNeoId(CARD, 67))
                .setNameAndNeoId(EQUIPMENT, B, nameAndNeoId(EQUIPMENT, 44));
    }

    protected NodeDto zzServiceDto(String clientName, int clientId, ServiceStatus serviceStatus) {
        return new NodeDto(db.node("ZZ Service", 2))
                .setProperty(STATUS, serviceStatus.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId(clientName, clientId));
    }

    protected NodeDto service80Dto(String clientName, int clientId, ServiceStatus serviceStatus) {
        return new NodeDto(db.node(SERVICE, 80))
                .setProperty(STATUS, serviceStatus.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId(clientName, clientId));
    }
}
