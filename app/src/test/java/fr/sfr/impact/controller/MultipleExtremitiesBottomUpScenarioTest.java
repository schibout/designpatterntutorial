package fr.sfr.impact.controller;

import fr.sfr.impact.domain.bottomup.ServiceStatus;
import fr.sfr.impact.dto.NodeDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.neo4j.graphdb.Node;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static fr.sfr.impact.domain.Constants.*;
import static fr.sfr.impact.domain.bottomup.NodeStatus.KO;
import static fr.sfr.impact.domain.bottomup.NodeStatus.OK;
import static fr.sfr.impact.domain.bottomup.ServiceStatus.NOT_SECURE;
import static fr.sfr.impact.test.PrePopulatedDatabase.*;

@ContextConfiguration(locations = {"classpath:services.xml", "classpath:test-database-extended.xml", "classpath:test-controllers.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class MultipleExtremitiesBottomUpScenarioTest extends SingleNodeAnalysisEndToEndTest {

    @Override
    protected Node getNodeToAnalyze() {
        return db.node(CARD, 40);
    }

    @Test
    public void verifyImpactedClients() {
        verifyImpactedClients(db.node("A Client", 1), db.node(CLIENT, 100));
    }

    @Test
    public void verifyServicesAndServicesForClient() {
        NodeDto serviceDto1 = zzServiceDto("A Client", 1, NOT_SECURE);

        NodeDto circuitDto1 = new NodeDto(db.node(CIRCUIT, 3))
                .setProperty(STATUS, NOT_SECURE.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId("A Client", 1))
                .setProperty(AU4 + U_NAME + A, "MULTIPLE");

        NodeDto primary1 = rcMain5Dto("A Client", 1, OK);
        NodeDto secondary1 = rcSpare6Dto("A Client", 1, KO);
        NodeDto serviceDto2 = zzServiceDto(CLIENT, 100, NOT_SECURE);

        NodeDto circuitDto2 = new NodeDto(db.node(CIRCUIT, 3))
                .setProperty(STATUS, NOT_SECURE.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId(CLIENT, 100))

                .setProperty(AU4 + U_NAME + A, "MULTIPLE");

        NodeDto primary2 = rcMain5Dto(CLIENT, 100, OK);
        NodeDto secondary2 = rcSpare6Dto(CLIENT, 100, KO);
        NodeDto serviceDto3 = service80Dto(CLIENT, 100, ServiceStatus.NOT_SECURE);
        NodeDto circuitDto3 = circuit63Dto(CLIENT, 100, ServiceStatus.NOT_SECURE);
        NodeDto primary3 = rcMain60Dto(CLIENT, 100, KO);
        NodeDto secondary3 = rcSpare62Dto(CLIENT, 100, KO);

        verifyServicesForClient(db.node("A Client", 1), serviceDto1, circuitDto1, primary1, secondary1);
        verifyServicesForClient(db.node(CLIENT, 100), serviceDto3, circuitDto3, primary3, secondary3, serviceDto2, circuitDto2, primary2, secondary2);
        verifyImpactedServices(serviceDto1, circuitDto1, primary1, secondary1, serviceDto3, circuitDto3, primary3, secondary3, serviceDto2, circuitDto2, primary2, secondary2);
    }

    @Test
    public void verifyNodesOnPaths() {
        verifyNodesOnPath(db.node(CARD, 40), db.node(PORT, 39), db.node(AU4, 38), db.node(CONDUIT, 10), db.node(ROUTECIRCUIT + SPARE, 6),
                db.node(ROUTECIRCUIT, 4), db.node(CIRCUIT, 3), db.node("ZZ Service", 2), db.node("A Client", 1), db.node(CLIENT, 100),
                db.node(CONDUIT, 51), db.node(CONDUIT, 50), db.node(ROUTECIRCUIT + MAIN, 60), db.node(ROUTECIRCUIT, 61), db.node(CIRCUIT, 63), db.node(SERVICE, 80),
                db.node(PORT, 71), db.node(AU4, 72), db.node(CONDUIT, 66), db.node(ROUTECIRCUIT + SPARE, 62));
    }
}
