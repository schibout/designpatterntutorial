package fr.sfr.impact.controller;

import fr.sfr.impact.dto.NodeDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.neo4j.graphdb.Node;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static fr.sfr.impact.domain.Constants.*;
import static fr.sfr.impact.domain.bottomup.NodeStatus.KO;
import static fr.sfr.impact.domain.bottomup.NodeStatus.OK;
import static fr.sfr.impact.domain.bottomup.ServiceStatus.NOT_SECURE;
import static fr.sfr.impact.test.PrePopulatedDatabase.ROUTECIRCUIT;
import static fr.sfr.impact.test.PrePopulatedDatabase.SPARE;

@ContextConfiguration(locations = {"classpath:services.xml", "classpath:test-database-extended.xml", "classpath:test-controllers.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class StrangeClientBottomUpScenarioTest extends SingleNodeAnalysisEndToEndTest {

    @Override
    protected Node getNodeToAnalyze() {
        return db.node(AU4, 21);
    }

    @Test
    public void verifyImpactedClients() {
        verifyImpactedClients(db.node("A Client", 1), db.node(CLIENT, 100), db.node(CLIENT, 1007));
    }

    @Test
    public void verifyServicesAndServicesForClient() {
        NodeDto serviceDto1 = zzServiceDto("A Client", 1, NOT_SECURE);

        NodeDto circuitDto1 = new NodeDto(db.node(CIRCUIT, 3))
                .setProperty(STATUS, NOT_SECURE.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId("A Client", 1))
                .setProperty(AU4 + U_NAME + A, "MULTIPLE");

        NodeDto primary1 = rcMain5Dto("A Client", 1, OK);
        NodeDto secondary1 = rcSpare6Dto("A Client", 1, KO);
        NodeDto serviceDto2 = zzServiceDto(CLIENT, 100, NOT_SECURE);

        NodeDto circuitDto2 = new NodeDto(db.node(CIRCUIT, 3))
                .setProperty(STATUS, NOT_SECURE.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId(CLIENT, 100))
                .setProperty(AU4 + U_NAME + A, "MULTIPLE");

        NodeDto primary2 = rcMain5Dto(CLIENT, 100, OK);
        NodeDto secondary2 = rcSpare6Dto(CLIENT, 100, KO);

        NodeDto au421 = new NodeDto(db.node(AU4, 21))
                .setProperty(STATUS, KO.name())
                .setNameAndNeoId(CLIENT, nameAndNeoId(CLIENT, 1007));

        verifyServicesForClient(db.node("A Client", 1), serviceDto1, circuitDto1, primary1, secondary1);
        verifyServicesForClient(db.node(CLIENT, 100), serviceDto2, circuitDto2, primary2, secondary2);
        verifyServicesForClient(db.node(CLIENT, 1007), au421);
        verifyImpactedServices(serviceDto1, circuitDto1, primary1, secondary1, serviceDto2, circuitDto2, primary2, secondary2, au421);
    }

    @Test
    public void verifyNodesOnPaths() {
        verifyNodesOnPath(db.node(AU4, 21),
                db.node(CONDUIT, 9), db.node(ROUTECIRCUIT + SPARE, 6), db.node(ROUTECIRCUIT, 4), db.node(CIRCUIT, 3),
                db.node("ZZ Service", 2), db.node("A Client", 1), db.node(CLIENT, 1007), db.node(CLIENT, 100));
    }
}
