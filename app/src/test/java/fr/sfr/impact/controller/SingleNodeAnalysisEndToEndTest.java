package fr.sfr.impact.controller;

import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.dto.NodeDto;
import fr.sfr.impact.web.controller.BottomUpController;
import org.junit.Test;
import org.neo4j.graphdb.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import java.util.List;

import static org.junit.Assert.assertEquals;

@SuppressWarnings("unchecked")
public abstract class SingleNodeAnalysisEndToEndTest extends AbstractScenarioTest {

    @Autowired
    protected BottomUpController controller;

    protected abstract Node getNodeToAnalyze();

    @Test
    public void correctNodeAnalysedIsReturned() {
        Model model = new ExtendedModelMap();
        controller.getImpactedClients(model, getNodeToAnalyze().getId(), PagingCriteria.EVERYTHING);
        NodeDto nodeDetail = ((List<NodeDto>) model.asMap().get("nodeDetail")).get(0);
        assertEquals(new NodeDto(getNodeToAnalyze()), nodeDetail);
    }

    protected void verifyImpactedClients(Node... expected) {
        verify(new ModelPopulator() {
            @Override
            public Model populateModel(Model model) {
                controller.getImpactedClients(model, getNodeToAnalyze().getId(), PagingCriteria.EVERYTHING);
                return model;
            }
        }, expected);
    }

    protected void verifyImpactedServices(NodeDto... expected) {
        verify(new ModelPopulator() {
            @Override
            public Model populateModel(Model model) {
                controller.getImpactedServices(model, getNodeToAnalyze().getId(), PagingCriteria.EVERYTHING);
                return model;
            }
        }, expected);
    }

    protected void verifyNodesOnPath(Node... expected) {
        verifyIgnoringOrder(new ModelPopulator() {
            @Override
            public Model populateModel(Model model) {
                controller.getNodesOnPath(model, getNodeToAnalyze().getId(), PagingCriteria.EVERYTHING);
                return model;
            }
        }, expected);
    }

    protected void verifyServicesForClient(final Node client, NodeDto... expected) {
        verify(new ModelPopulator() {
            @Override
            public Model populateModel(Model model) {
                controller.getImpactedServicesForClient(model, getNodeToAnalyze().getId(), client.getId(), PagingCriteria.EVERYTHING);
                return model;
            }
        }, expected);
    }

    private void verify(ModelPopulator modelPopulator, Node... expected) {
        Model model = modelPopulator.populateModel(new ExtendedModelMap());
        assertEquals(expected.length, model.asMap().get("totalResults"));
        super.verify((List<NodeDto>) model.asMap().get("nodes"), expected);
    }

    private void verifyIgnoringOrder(ModelPopulator modelPopulator, Node... expected) {
        Model model = modelPopulator.populateModel(new ExtendedModelMap());
        assertEquals(expected.length, model.asMap().get("totalResults"));
        super.verifyIgnoringOrder((List<NodeDto>) model.asMap().get("nodes"), expected);
    }

    private void verify(ModelPopulator modelPopulator, NodeDto... expected) {
        Model model = modelPopulator.populateModel(new ExtendedModelMap());
        assertEquals(expected.length, model.asMap().get("totalResults"));
        super.verify((List<NodeDto>) model.asMap().get("nodes"), expected);
    }

    interface ModelPopulator {
        Model populateModel(Model model);
    }
}
