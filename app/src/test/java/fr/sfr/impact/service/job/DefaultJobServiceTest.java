package fr.sfr.impact.service.job;

import fr.sfr.impact.domain.job.JobDescription;
import fr.sfr.impact.domain.job.JobStatus;
import fr.sfr.impact.domain.job.JobTest;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@ContextConfiguration(locations = {"classpath:services.xml", "classpath:test-database.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class DefaultJobServiceTest {

    @Autowired
    private JobService jobService;

    @Test
    public void shouldProduceResultWithValidInput() throws IOException, InterruptedException {
        String jobId = jobService.createJob(JobTest.createFromFile("integration-valid.csv"));

        while (!jobService.getJobStatus(jobId).equals(JobStatus.Status.FINISHED) && !jobService.getJobStatus(jobId).equals(JobStatus.Status.ERROR)) {
            Thread.sleep(100);
            System.out.println("Waiting for job to finish, status: " + jobService.getJobProgress(jobId) + " %");
        }

        assertEquals(jobService.getJobStatus(jobId), JobStatus.Status.FINISHED);
        assertTrue(jobService.getJobResult(jobId).toByteArray().length > 0);
    }

    @Test
    public void shouldFailWithNonExistingNode() throws IOException, InterruptedException {
        String jobId = jobService.createJob(JobTest.createFromFile("integration-non-existing.csv"));
        Thread.sleep(100);

        assertEquals(jobService.getJobStatus(jobId), JobStatus.Status.ERROR);
    }

    @Test
    public void shouldFailWithNoNodes() throws IOException, InterruptedException {
        String jobId = jobService.createJob(JobTest.createFromFile("integration-none.csv"));
        Thread.sleep(100);

        assertEquals(jobService.getJobStatus(jobId), JobStatus.Status.ERROR);
    }

    @Test
    public void jobsAreCleanedUp() throws IOException, InterruptedException {
        String validJobId = jobService.createJob(JobTest.createFromFile("integration-valid.csv"));
        ((DefaultJobService) jobService).findJob(validJobId).setFinishedOrErrorJobMaxAgeMs(1000);
        String invalidJobId = jobService.createJob(JobTest.createFromFile("integration-non-existing.csv"));
        ((DefaultJobService) jobService).findJob(invalidJobId).setFinishedOrErrorJobMaxAgeMs(1000);

        Thread.sleep(1500);

        assertEquals(JobStatus.Status.FINISHED, jobService.getJobStatus(validJobId));
        assertEquals(JobStatus.Status.ERROR, jobService.getJobStatus(invalidJobId));

        Thread.sleep(10000);

        try {
            jobService.getJobStatus(validJobId);
            fail();
        } catch (RuntimeException e) {
            //OK
        }

        try {
            jobService.getJobStatus(invalidJobId);
            fail();
        } catch (RuntimeException e) {
            //OK
        }
    }

    @Test(expected = RuntimeException.class)
    public void shouldNotExceedMaxJobs() throws IOException {
        JobDescription jobDescription = JobTest.createFromFile("integration-valid.csv");
        for (int i = 0; i < 110; i++) {
            jobService.createJob(jobDescription);
        }
    }
}
