package fr.sfr.impact.service.node;

import fr.sfr.impact.domain.RelationshipTypeAndDirection;
import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.dto.NodeDto;
import fr.sfr.impact.dto.NodeDtoSubset;
import fr.sfr.impact.test.GraphDataBuilder;
import fr.sfr.impact.test.TransactionalGraphDataBuilder;
import org.junit.Before;
import org.junit.Test;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.test.ImpermanentGraphDatabase;

import java.io.IOException;
import java.util.Arrays;

import static fr.sfr.impact.domain.Constants.*;
import static fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes.*;
import static org.junit.Assert.assertEquals;

public class DefaultNodeDetailServiceTest {

    protected DefaultNodeDetailService nodeDetailService;
    protected GraphDatabaseService database;

    protected Node equipment1, card2, port3, au44, conduit5, routeCircuitSpare, equipment6, card7, port8, au49, conduit10, routeCircuitMain, routeCircuitAbstract, circuit15, service16, client17, site11;
    private static final String EQUIPEMENT = "EQUIPEMENT";
    private static final String ROUTECIRCUIT = "ROUTECIRCUIT";

    @Before
    public void setUp() throws IOException {
        database = new ImpermanentGraphDatabase();
        nodeDetailService = new DefaultNodeDetailService();
        nodeDetailService.setDatabase(database);
        populateDatabase();
    }

    @Test
    public void correctNodeDtoReturned() {
        assertEquals(new NodeDto(card2), nodeDetailService.getNodeDetail(card2.getId()));
        assertEquals(new NodeDto(port3), nodeDetailService.getNodeDetail(port3.getId()));
    }

    @Test(expected = NotFoundException.class)
    public void exceptionIsThrownWhenNodeDoesNotExist() {
        nodeDetailService.getNodeDetail(123);
    }

    @Test
    public void correctTypesAndDirectionsAreReturned() {
        assertEquals(Arrays.asList(
                new RelationshipTypeAndDirection(EST_INCLUS_DANS, Direction.OUTGOING),
                new RelationshipTypeAndDirection(EST_INCLUS_DANS, Direction.INCOMING)),
                nodeDetailService.getRelationshipTypesAndDirections(equipment1.getId()));
        assertEquals(Arrays.asList(
                new RelationshipTypeAndDirection(EST_COMPOSE_DE, Direction.OUTGOING),
                new RelationshipTypeAndDirection(EST_AFFECTE_A, Direction.OUTGOING)),
                nodeDetailService.getRelationshipTypesAndDirections(service16.getId()));
    }

    @Test
    public void correctSurroundingsAreReturned() {
        NodeDtoSubset expected = new NodeDtoSubset();
        expected.addDto(new NodeDto(card2));
        expected.setTotalNumber(1);
        assertEquals(expected, nodeDetailService.getNodeSurroundings(equipment1.getId(), new RelationshipTypeAndDirection(EST_INCLUS_DANS, Direction.INCOMING), new PagingCriteria()));

        expected = new NodeDtoSubset();
        expected.addDto(new NodeDto(equipment1));
        expected.addDto(new NodeDto(equipment6));
        expected.setTotalNumber(2);
        assertEquals(expected, nodeDetailService.getNodeSurroundings(site11.getId(), new RelationshipTypeAndDirection(EST_INCLUS_DANS, Direction.INCOMING), new PagingCriteria()));

        expected = new NodeDtoSubset();
        expected.addDto(new NodeDto(equipment1));
        expected.setTotalNumber(2);
        assertEquals(expected, nodeDetailService.getNodeSurroundings(site11.getId(), new RelationshipTypeAndDirection(EST_INCLUS_DANS, Direction.INCOMING), new PagingCriteria(1, 1)));

        expected = new NodeDtoSubset();
        expected.addDto(new NodeDto(equipment6));
        expected.setTotalNumber(2);
        assertEquals(expected, nodeDetailService.getNodeSurroundings(site11.getId(), new RelationshipTypeAndDirection(EST_INCLUS_DANS, Direction.INCOMING), new PagingCriteria(2, 1)));
    }

    protected void populateDatabase() {
        GraphDataBuilder b = new TransactionalGraphDataBuilder(database);

        equipment1 = b.createNode("1", "Equipment 1", EQUIPEMENT);
        card2 = b.createNode("2", "Card 2", "CARTE");
        b.createRelationship(card2, equipment1, EST_INCLUS_DANS);
        port3 = b.createNode("3", "Port 3", "PORT");
        b.createRelationship(port3, card2, EST_INCLUS_DANS);
        au44 = b.createNode("4", "Au4 4", "AU4");
        b.createRelationship(au44, port3, EST_INCLUS_DANS);
        conduit5 = b.createNode("5", "Conduit 5", "CONDUIT");
        b.createRelationship(conduit5, au44, EST_COMPOSE_DE);
        routeCircuitSpare = b.createNode("12", "Route Circuit Spare", ROUTECIRCUIT);
        b.createRelationship(routeCircuitSpare, conduit5, EST_COMPOSE_DE);

        equipment6 = b.createNode("6", "Equipment 6", EQUIPEMENT);
        card7 = b.createNode("7", "Card 7", "CARTE");
        b.createRelationship(card7, equipment6, EST_INCLUS_DANS);
        port8 = b.createNode("8", "Port 8", "PORT");
        b.createRelationship(port8, card7, EST_INCLUS_DANS);
        au49 = b.createNode("9", "Au4 9", "AU4");
        b.createRelationship(au49, port8, EST_INCLUS_DANS);
        conduit10 = b.createNode("10", "Conduit 10", "CONDUIT");
        b.createRelationship(conduit10, au49, EST_COMPOSE_DE);
        routeCircuitMain = b.createNode("13", "Route Circuit Main", ROUTECIRCUIT);
        b.createRelationship(routeCircuitMain, conduit10, EST_COMPOSE_DE);

        site11 = b.createNode("11", "Site 11", "SITE");
        b.createRelationship(equipment1, site11, EST_INCLUS_DANS);
        b.createRelationship(equipment6, site11, EST_INCLUS_DANS);

        routeCircuitAbstract = b.createAbstractNode("14", "Route Circuit Abstract", ROUTECIRCUIT);
        b.createRelationship(routeCircuitAbstract, routeCircuitSpare, EST_COMPOSE_DE, PARTAGE, SECONDARY);
        b.createRelationship(routeCircuitAbstract, routeCircuitMain, EST_COMPOSE_DE, PARTAGE, PRIMARY);

        circuit15 = b.createNode("15", "Circuit 15", "CIRCUIT");
        b.createRelationship(circuit15, routeCircuitAbstract, EST_COMPOSE_DE);
        service16 = b.createNode("16", "Service 16", SERVICE);
        b.createRelationship(service16, circuit15, EST_COMPOSE_DE);
        client17 = b.createNode("17", "Client 17", CLIENT);
        b.createRelationship(service16, client17, EST_AFFECTE_A);

        Node service18 = b.createNode("18", "Service 18", SERVICE);  //should not be impacted!
        b.createRelationship(service18, client17, EST_AFFECTE_A);
    }
}
