package fr.sfr.impact.domain.search;

import fr.sfr.impact.domain.search.PagingCriteria;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Quick unit test for {@link fr.sfr.impact.domain.search.PagingCriteria}.
 *
 * @author Michal Bachman
 */
public class PagingCriteriaTest {

    @Test
    public void testFirstAndLast() {
        assertEquals(0, new PagingCriteria(1, 20).getFirst());
        assertEquals(19, new PagingCriteria(1, 20).getLast());
        assertEquals(100, new PagingCriteria(3, 50).getFirst());
        assertEquals(149, new PagingCriteria(3, 50).getLast());
    }

    @Test
    public void testNumberOfPages() {
        assertEquals(1, new PagingCriteria(1, 1).getNumberOfPages(0));
        assertEquals(1, new PagingCriteria(1, 1).getNumberOfPages(1));
        assertEquals(2, new PagingCriteria(1, 1).getNumberOfPages(2));
        assertEquals(1, new PagingCriteria(1, 2).getNumberOfPages(1));
        assertEquals(1, new PagingCriteria(1, 2).getNumberOfPages(2));
    }
}
