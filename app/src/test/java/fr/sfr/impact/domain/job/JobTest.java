package fr.sfr.impact.domain.job;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;

import static java.util.Collections.singleton;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JobTest {

    @Test
    public void shouldRejectJobDescriptionNoNode() throws IOException {
        checkRejected("noNode.csv");
    }

    @Test
    public void shouldRejectJobDescriptionWithInvalidTask() throws IOException {
        checkRejected("invalidTask.csv");
    }

    @Test
    public void shouldRejectJobDescriptionWithInvalidTaskType() throws IOException {
        checkRejected("invalidTaskType.csv");
    }

    @Test
    public void shouldRejectJobDescriptionWithNoTasks() throws IOException {
        checkRejected("noTasks.csv");
    }

    @Test
    public void shouldRejectJobDescriptionWithTooManyTasks() throws IOException {
        checkRejected("tooManyTasks.csv");
    }

    private void checkRejected(String fileName) throws IOException {
        Job job = Job.withDescription(createFromFile(fileName));
        job.start();
        List<Task> tasks = job.getTasks();

        assertTrue(tasks.isEmpty());
        assertEquals(JobStatus.Status.ERROR, job.getStatus());
        assertNotNull(job.getErrorMessage());
        assertTrue(StringUtils.isNotBlank(job.getErrorMessage()));
        assertEquals(0, job.getPercentFinished());
    }

    @Test
    public void jobShouldHaveTheRightLocale() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        assertEquals(Locale.ENGLISH, job.getLocale());
    }

    @Test
    public void newJobShouldNotBeStarted() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));

        assertEquals(JobStatus.Status.NOT_STARTED, job.getStatus());
        assertNull(job.getErrorMessage());
        assertTrue(StringUtils.isBlank(job.getErrorMessage()));
        assertEquals(0, job.getPercentFinished());
    }

    @Test(expected = InvalidJobStateException.class)
    public void newJobShouldFailProducingTasks() throws IOException { //because it should be started first
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.getTasks();
    }

    @Test(expected = InvalidJobStateException.class)
    public void newJobShouldFailProducingResult() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.getJobResult();
    }

    @Test(expected = InvalidJobStateException.class)
    public void newJobShouldFailAddingResult() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.addResult("dummy", new byte[0]);
    }

    @Test(expected = InvalidJobStateException.class)
    public void newJobShouldFailFinishing() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.finish();
    }

    @Test
    public void startedJobShouldProduceTasks() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.start();

        List<Task> tasks = job.getTasks();
        assertEquals(4, tasks.size());
        assertEquals(new Task(singleton("ABC"), Task.TaskType.BU), tasks.get(0));
        assertEquals(new Task(singleton("DEF"), Task.TaskType.TD), tasks.get(1));
        assertEquals(new Task(singleton("XYZ"), Task.TaskType.BU), tasks.get(2));
        assertEquals(new Task(new TreeSet<String>(Arrays.asList("ABC", "XYZ")), Task.TaskType.BU), tasks.get(3));
    }

    @Test(expected = InvalidJobStateException.class)
    public void startedJobShouldFailStartingAgain() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.start();
        job.start();
    }

    @Test
    public void startedJobShouldBeInProgress() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.start();

        assertEquals(JobStatus.Status.IN_PROGRESS, job.getStatus());
        assertNull(job.getErrorMessage());
        assertTrue(StringUtils.isBlank(job.getErrorMessage()));
        assertEquals(0, job.getPercentFinished());
    }

    @Test(expected = InvalidJobStateException.class)
    public void jobInProgressShouldFailProducingResult() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.start();
        job.getJobResult();
    }

    @Test
    public void shouldCalculateCorrectPercentage() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.start();
        job.getTasks();
        job.addResult("test", new byte[]{123, 21});

        assertEquals(JobStatus.Status.IN_PROGRESS, job.getStatus());
        assertNull(job.getErrorMessage());
        assertTrue(StringUtils.isBlank(job.getErrorMessage()));
        assertEquals(25, job.getPercentFinished());
    }

    @Test
    public void finishedJobShouldBeFinished() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.start();
        job.getTasks();
        job.addResult("test", new byte[]{123, 21});
        job.finish();

        assertEquals(JobStatus.Status.FINISHED, job.getStatus());
        assertNull(job.getErrorMessage());
        assertTrue(StringUtils.isBlank(job.getErrorMessage()));
        assertEquals(100, job.getPercentFinished());
    }

    @Test
    public void finishedJobShouldProduceResult() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.start();
        job.getTasks();
        job.addResult("test.xls", new byte[]{123, 21});
        job.addResult("test2.xls", new byte[]{18, 1});
        job.finish();

        assertTrue(job.getJobResult().toByteArray().length > 0);
    }

    @Test(expected = InvalidJobStateException.class)
    public void finishedJobShouldNotStart() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.start();
        job.getTasks();
        job.addResult("test.xls", new byte[]{123, 21});
        job.finish();
        job.start();
    }

    @Test(expected = InvalidJobStateException.class)
    public void finishedJobShouldNotAcceptMoreData() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.start();
        job.getTasks();
        job.addResult("test.xls", new byte[]{123, 21});
        job.finish();
        job.addResult("test.xls", new byte[]{123, 21});
    }

    @Test(expected = InvalidJobStateException.class)
    public void finishedJobShouldNotFinishAgain() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.start();
        job.getTasks();
        job.addResult("test.xls", new byte[]{123, 21});
        job.finish();
        job.finish();
    }

    @Test
    public void errorJobShouldBeInErrorState() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.error("testMessage");
        assertEquals(JobStatus.Status.ERROR, job.getStatus());
        assertEquals("testMessage", job.getErrorMessage());
    }

    @Test(expected = InvalidJobStateException.class)
    public void errorJobShouldNotStart() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.error("testMessage");
        job.start();
    }

    @Test(expected = InvalidJobStateException.class)
    public void errorJobShouldNotFinish() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.error("testMessage");
        job.finish();
    }

    @Test(expected = InvalidJobStateException.class)
    public void errorJobShouldNotAcceptMoreData() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.error("testMessage");
        job.addResult("test.xls", new byte[]{123, 21});
    }

    @Test
    public void notStartedJobShouldNotBeRemoved() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        assertFalse(job.shouldBeRemoved());
    }

    @Test
    public void jobInProgressShouldNotBeRemoved() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.start();
        assertFalse(job.shouldBeRemoved());
    }

    @Test
    public void oldNotStartedJobShouldNotBeRemoved() throws IOException, InterruptedException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.setFinishedOrErrorJobMaxAgeMs(1);
        Thread.sleep(2);
        assertFalse(job.shouldBeRemoved());
    }

    @Test
    public void oldJobInProgressShouldNotBeRemoved() throws IOException, InterruptedException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.start();
        job.setFinishedOrErrorJobMaxAgeMs(1);
        Thread.sleep(2);
        assertFalse(job.shouldBeRemoved());
    }

    @Test
    public void youngFinishedJobShouldNotBeRemoved() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.start();
        job.getTasks();
        job.addResult("test.xls", new byte[]{123, 21});
        job.finish();

        assertFalse(job.shouldBeRemoved());
    }

    @Test
    public void youngJobInErrorShouldNotBeRemoved() throws IOException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.error("dummy");

        assertFalse(job.shouldBeRemoved());
    }

    @Test
    public void oldFinishedJobShouldBeRemoved() throws IOException, InterruptedException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.start();
        job.getTasks();
        job.addResult("test.xls", new byte[]{123, 21});
        job.finish();
        job.setFinishedOrErrorJobMaxAgeMs(1);
        Thread.sleep(2);

        assertTrue(job.shouldBeRemoved());
    }

    @Test
    public void oldJobInErrorShouldBeRemoved() throws IOException, InterruptedException {
        Job job = Job.withDescription(createFromFile("valid.csv"));
        job.error("dummy");
        job.setFinishedOrErrorJobMaxAgeMs(1);
        Thread.sleep(2);

        assertTrue(job.shouldBeRemoved());
    }

    public static JobDescription createFromFile(String fileName) throws IOException {
        JobDescription result = new JobDescription();
        result.setLocale(Locale.ENGLISH);

        CommonsMultipartFile mockFile = mock(CommonsMultipartFile.class);
        when(mockFile.getBytes()).thenReturn(IOUtils.toByteArray(new ClassPathResource("fr/sfr/impact/domain/job/" + fileName).getInputStream()));
        result.setFileContents(mockFile);
        return result;
    }
}
