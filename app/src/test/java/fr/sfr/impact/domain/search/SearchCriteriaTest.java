package fr.sfr.impact.domain.search;

import fr.sfr.impact.domain.search.SearchCriteria;
import org.junit.Test;

import static fr.sfr.impact.domain.search.SearchCriteria.*;
import static org.junit.Assert.assertEquals;

public class SearchCriteriaTest {

    @Test
    public void testEncodingForSearch() {
        assertEquals("TEST", encodeForSearch("test"));
        assertEquals("TEST*", encodeForSearch("test*"));
        assertEquals("TEST**", encodeForSearch("test**"));
        assertEquals("*TEST", encodeForSearch("*test"));
        assertEquals("*TEST*", encodeForSearch("*test*"));
        assertEquals("*TEST**", encodeForSearch("*test**"));
        assertEquals("*TEST***", encodeForSearch("*test***"));
        assertEquals("**TEST", encodeForSearch("**test"));
        assertEquals("TE*T", encodeForSearch("te*t"));
        assertEquals("TE*T+AND+T*ST", encodeForSearch("te*t and t*st"));
        assertEquals("T**ST", encodeForSearch("t**st"));
        assertEquals("T%23ST", encodeForSearch("t#st"));
    }

    @Test
    public void testQueryCreation() {
        assertEquals("ID:*", new SearchCriteria().buildQuery());
        assertEquals("NAME:TEST", new SearchCriteria("", "Test", "", null, null, null, null).buildQuery());
        assertEquals("ID:TESTID AND NAME:TESTNAME", new SearchCriteria("TestId", "TestName", "", null, null, null, null).buildQuery());
        assertEquals("ID:TESTID AND NAME:TESTNAME AND TYPE:TESTTYPE", new SearchCriteria("TestId", "TestName", "TestType", null, null, null, null).buildQuery());
        assertEquals("ID:TESTID AND NAME:*TESTNAME AND TYPE:TESTT*PE", new SearchCriteria("TestId", "*TestName", "TestT*pe", null, null, null, null).buildQuery());
    }
}
