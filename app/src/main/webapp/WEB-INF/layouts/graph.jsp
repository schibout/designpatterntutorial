<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<spring:url var="url" value="/js/raphael.js"/>
<script src="${url}"></script>
<spring:url var="url" value="/js/underscore.js"/>
<script src="${url}"></script>
<spring:url var="url" value="/js/jquery-1.7.1.js"/>
<script src="${url}"></script>
<spring:url var="url" value="/js/raphael-ext.js"/>
<script src="${url}"></script>
<spring:url var="url" value="/js/graph.js"/>
<script src="${url}"></script>
<title></title>

<body>
<table align="center" style="width: 100%" border="0">
    <caption/>
    <col/>
    <col/>
    <tbody>
    <tr>
        <spring:url var="logoUrl" value="/img/Logo-SFR.jpg"/>
        <spring:url var="logo2Url" value="/images/ANALYSE_IMPACT.png"/>
        <td style="text-align: left;" width="500"><img width="100%" src="${logoUrl}" alt="SFR"/></td>
        <td align="right"><img width="100%" src="${logo2Url}" alt="ANALYSE_IMPACT"/></td>
    </tr>
    </tbody>
</table>
<div id="main">
    <div>
        <script type="text/javascript">
            <spring:url var="popupUrl" value="/nodes/${nodeId}/topdown/graph/data?impactedNode=${impactedNode}" />
            $(function () {
                try {
                    $.getJSON('${popupUrl}', function (data) {
                        //var g = Graph.graph(data).init();
                        // var g = Graph.graph(data, {drawLevelLines:false}).init();
                        var g = Graph.graph(data, {drawLevelLines:false, renderMode:'i', height:2000}).init();
                        g.draw();
                    });
                } catch (e) {
                    alert(e);
                }
            });
        </script>
    </div>
    <div id="canvas"/>
</div>
<br/>

<p/><br/><br/><br/>
<table style="width: 100%" border="0">
    <caption/>
    <col/>
    <tbody>
    <tr>
        <td style="text-align: center;"><h4>Copyright @2011-2012, SFR</h4></td>
    </tr>
    </tbody>
</table>
</body>
</html>