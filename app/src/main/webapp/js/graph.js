var Graph = function () {


    // Utility functions
    function logObject(label, obj) {
        var l = "========"
        for(var i = 0; i < label.length; i++) {
            l += "=";
        }
        //console.log("=== " + label + " ===");
        //console.log(obj);
        //console.log(l);
    }

    // sort levels inversely by first letter
    var alphaNumericLevelSortFunc = function (key) {
        return -key.charCodeAt(0);
    }

    var numericLevelSortFunc = function (key) {
        return -1 * parseInt(key);
    }

    var combinedLevelSortFunc = function (key) {
        return numericLevelSortFunc(key) || alphaNumericLevelSortFunc(key);
    }

    var defaultConfig = {
        nodeColors:{
            CLIENT:'#ee9c00',
            SERVICE:'#fd00fa',
            ROP:'#fd0700',
            JARRETIERE:'#e41634',
            CONDUIT:'#f664f4',
            CIRCUIT:'#ba4bb8',
            LIEN:'#e451e1',
            PORT:'#3e4df6',
            EQUIPEMENT:'#8adcf0',
            CHASSIS:'#4391f6',
            CARTE:'#79adf0',
            SITE:'#98ee9b',
            SALLE:'#75ee67',
            BAIE:'#9fee75',
            DEFAULT:'#ecee8c'
        },
        relationshipColors:{
            EST_INCLUS_DANS:'green',
            EST_CONNECTE_A:'red',
            EST_COMPOSE_DE:'blue',
            EST_AFFECTE_A:'#ffa700',
            DEFAULT:'black'
        },
        distanceBetweenLevels:100,
        distanceBetweenNodes:100,
        drawLevelLines:true,
        levelSortFunc:combinedLevelSortFunc,
        connectionType:'l',
        width:1800,
        height:1000,
        surroundingOpacity:0.7,
        baseUrl:'',
        renderMode:'s',
        renderImageBase:'../../../img/',
        validateData: true,
        dynamicDimensions: true,
        spreadOutFactor: 3
    };

    function Level(y, n) {
        this.currentWidth = 0;
        this.x = 50;
        this.y = y;
        this.totalNodes = n;
    }

    Level.prototype.incWidth = function (x) {
        x = x || 1;
        this.currentWidth += x;
    }

    var graph = function (input, config) {

        config = config || {};

        _.defaults(config, defaultConfig);

        if (config.validateData && !(validateData(input) || confirm("Invalid Data received. Continue?"))) {
            throw new Error("Invalid data");
        }

        var width = config.width,
            height = config.height;

        var paper,
            _elements = [],
            elementsIndex = {},
            globalIndex,
            model;

        // === Data validation ===

        function validateData(data) {
            var n = _.chain(data.nodes).pluck('id').uniq().value().length,
                r = _.chain(data.relationships).pluck('id').uniq().value().length;
            return n === data.nodes.length && r === data.relationships.length;
        }

        // === Index ===

        // returns a global index from all index functions
        // index is read only once created. Its purpose is to help writing traversal functions
        function createGlobalIndex() {
            var index = {};
            index.nodes = createNodeIndex();
            index.inAdjacencyMatrix = createInAdjacencyMatrix();
            index.relationships = createRelationshipIndex();
            return index;
        }

        // refresh the global index by calling createGlobalIndex
        function refreshGlobalIndex() {
            globalIndex = createGlobalIndex(input);
        }

        // creates an index of input nodes by their id
        function createNodeIndex() {
            var index = {};
            _.each(input.nodes, function (node) {
                index[node.id] = node;
            });
            return index;
        }

        // creates an index of input relationships by their id
        function createRelationshipIndex() {
            var index = {};
            _.each(input.relationships, function (relatiohship) {
                index[relatiohship.id] = relatiohship;
            });
            return index;
        }

        // creates an adjacency matrix node id -> relationships id of inbound relationships
        function createInAdjacencyMatrix() {
            var matrix = {};
            _.each(input.nodes, function (node) {
                matrix[node.id] = _.chain(input.relationships).filter(
                    function (relationship) {
                        return relationship.end === node.id;
                    }).pluck('id').value();
            });
            return matrix;
        }

        // === Model ===

        // initialises the model that is going to be rendered
        function createModel() {
            var model = {};

            function initLevels() {
                model.levels = {},
                    // calculate level order using a configurable function
                    model.orderedLevelKeys = _.chain(input.nodes).pluck('LEVEL').unique().sortBy(config.levelSortFunc).value();
                var countMap = _.groupBy(input.nodes, 'LEVEL');

                _.chain(countMap).keys().each(function (l) {
                    countMap[l] = countMap[l].length;
                });


                _.each(model.orderedLevelKeys, function (l, i) {
                    var y = config.distanceBetweenLevels * (i + 1);
                    var n = countMap[l];
                    model.levels[l] = new Level(y, n);
                });
            }

            function initPartitions() {
                model.partition = partitionGraph();
            }

            initLevels();

            initPartitions();

            return model;
        }

        function initModel() {
            model = createModel();
            logObject("model", model)
        }

        function partitionGraph() {
            var groups = [],
                traversed = [],
                defaultGroup;

            // basically breadth-first traversal with filtering
            // returns a group with the root as the first element
            function traverse(start, types) {

                // nodes to traverse
                var queue = [start],
                    group = [];

                while (queue.length > 0) {
                    var current = queue.pop();
                    group.push(current);

                    // find all nodes to visit from current node that have not already been traversed
                    var inbound = globalIndex.inAdjacencyMatrix[current.id];
                    var nodes = _.chain(inbound)
                        .map(function (id) {
                            var relationship = globalIndex.relationships[id];
                            return relationship;
                        })
                        .pluck('start')
                        .filter(function (id) {
                            return traversed.indexOf(id) === -1;;
                        })
                        .map(function (id) {
                            var node = globalIndex.nodes[id];
                            return node;
                        }).
                        filter(function (node) {
                            var nodeTypeInTypes = types.indexOf(node['TYPE']) !== -1;
                            return nodeTypeInTypes;
                        }).value();

                    _.each(nodes, function (node) {
                        queue.push(node);
                        traversed.push(node.id);
                    });
                }
                return group;
            }

            // group nodes by sites
            _.chain(input.nodes)
                .filter(function (node) {
                    return node['TYPE'] === 'SITE';
                })
                .each(function (node) {
                    groups.push(traverse(node, ['SALLE', 'BAIE', 'EQUIPEMENT', 'PORT', 'SLOT']));
                })
                .value();

            // add everything else to default group
            var diff = _.difference(_.pluck(input.nodes, 'id'),
                _.chain(groups).flatten().pluck('id').value());
            defaultGroup = _.map(diff, function (id) {
                return globalIndex.nodes[id];
            });

            return {
                groups:groups,
                defaultGroup:defaultGroup
            };
        }

        function maxXOnLevel(l) {
            var level = _.groupBy(input.nodes, 'LEVEL')[l],
                xs = _.chain(level).pluck('_x').compact().value();
            return xs.length > 0 ? _.max(xs) : 0;
        }

        // assign position of nodes in model
        // assumes model is already built

        function assignPositions() {
            var x0 = 50;

            function assignGroupPositions() {

                _.each(model.partition.groups, assignOneGroup);

                function assignOneGroup(group) {
                    var levelMap = _.groupBy(group, 'LEVEL'),
                    // the max width (number of nodes) on any level of the group
                        maxElements = _.chain(levelMap).keys().map(
                            function (k) {
                                return levelMap[k].length;
                            }).max().value(),
                    //save current width for every group we want to update
                        currentLevels = _.chain(levelMap).keys().map(
                            function (k) {
                                return {k:k, w:model.levels[k].currentWidth};
                            }).value();

                    _.each(group, function (node) {
                        // calculate x from model levels
                        var level = model.levels[node['LEVEL']],
                            x = x0 + 2 * (level.currentWidth * config.distanceBetweenNodes);
                        node._x = x;
                        node._y = model.levels[node['LEVEL']].y;
                        level.incWidth();
                    });

                    // adjust width on each of the updated levels to maxElements
                    _.each(currentLevels, function (i) {
                        var level = model.levels[i.k],
                            currentWidth = i.w;

                        level.currentWidth = Math.max(currentWidth, currentWidth + maxElements);
                    });
                }
            }

            function assignDefaultGroupPositions() {
                _.each(model.partition.defaultGroup, function (node) {
                    var level = model.levels[node['LEVEL']];
                    var maxX = maxXOnLevel(node['LEVEL']);
                    var x = width / (level.totalNodes + 1);
                    node._x = maxX + x;
                    node._y = model.levels[node['LEVEL']].y;
                    level.incWidth(1);
                });
            }

            assignGroupPositions();
            assignDefaultGroupPositions();
        }

        var indexNode = function (node) {
            var level = node['LEVEL'];
            if (!elementsIndex[level]) {
                elementsIndex[level] = [];
            }
            elementsIndex[level].push(node);
        }

        var indexElement = function (element) {
            _elements.push(element);
        }

        // === Rendering ===

        function renderNode(mode, x, y, node, opacity) {
            switch(mode) {
                case 's':
                    return renderNodeAsShape(x, y, node, opacity);
                case 'i':
                    return renderNodeAsImage(x, y, node);
                default:
                    throw new Error('unkown render mode: ' + mode);
            }
        }

        function renderNodeAsShape(x, y, node, opacity) {
            return paper.ellipse(x, y, 45, 30)
                .attr({fill:nodeColor(node), stroke:'none', opacity:opacity})
                .data('id', node.id);
        }

        function renderNodeAsImage(x, y, node) {
            var img = config.renderImageBase || 'img/';

            switch (node['TYPE']) {
                case 'CONDUIT':
                    img += 'CONDUIT.PNG';
                    break;
                case 'SERVICE':
                    img += 'SERVICE.PNG';
                    break;
                case 'SLOT':
                    img += 'SLOT.PNG';
                    break;
                case 'CARTE':
                    img += 'SLOT.PNG';
                    break;
                case 'SITE':
                    img += 'SITE.PNG';
                    break;
                case 'PORT':
                    img += 'PORT.PNG';
                    break;
                case 'EQUIPEMENT':
                    img += 'EQUIP.PNG';
                    break;
                case 'BAIE':
                    img += 'BAIE.PNG';
                    break;
                case 'CABLE':
                    img += 'CABLE.PNG';
                    break;
                case 'CLIENT':
                    img += 'CLIENT.PNG';
                    break;
                case 'FIBRE':
                    img += 'FIBRE.PNG';
                    break;
                case 'CIRCUIT':
                    img += 'CIRCUIT.PNG';
                    break;
                case 'ROUTEUR':
                    img += 'ROUTEUR.PNG';
                    break;
                case 'SALLE':
                    img += 'SALLE.PNG';
                    break;
                case 'AU4':
                    img += 'AU4.PNG';
                    break;

                case 'LIEN':
                    img += 'LIEN.PNG';
                    break;
                case 'ROUTECIRCUIT':

                    //image en fonction du typo
                    if (node['TYPO'] != "ABSTRACT") {
                        img += 'ROUTECIRCUIT.PNG';
                    } else {
                        img += 'EQUIP.PNG';
                    }

                    break;
                case 'ROUTECONDUIT':

                    //image en fonction du typo
                    if (node['TYPO'] != "ABSTRACT") {
                        img += 'ROUTECONDUIT.PNG';
                    } else {
                        img += 'EQUIP.PNG';
                    }

                    break;

                default:
                    img += 'EQUIP.PNG';
            }

            return paper.image(img, x, y, 50, 50).data('id', node.id);
        }

        function renderNodeLabel(x, y, node) {
            return paper.text(x, y, node['id'] + ' - '+  node['NAME'] + '\n(' + node['TYPE'] + ')')
                .attr({fill:'black', stroke:'none'});
        }

        var nodeColor = function (node) {
            return config.nodeColors[node['TYPE']] || config.nodeColors.DEFAULT;
        }

        var relationshipColor = function (relationship) {
            return config.relationshipColors[relationship.type] || config.relationshipColors.DEFAULT;
        }

        var drawLevelLines = function () {
            _.each(model.orderedLevelKeys, function (key) {
                var y = model.levels[key].y;
                paper.path(['M', 0, y, 'L', width - 50, y].join(',')).toBack();
                paper.text(width - 30, y, key).attr({fill:'#000', stroke:'#000'});
            });
        }

        var init = function () {
            refreshGlobalIndex();

            initModel();


            if(config.dynamicDimensions) {
                // determine the max number of nodes on any level
                var map = _.groupBy(input.nodes, 'LEVEL'),
                    max = _.chain(map).keys().map(function(k){return map[k].length}).max().value();
                width = Math.min(config.distanceBetweenNodes * max * config.spreadOutFactor, config.width);
            }

            // assignPositions needs to be called after the dimensions are set
            // because it references the values width/height
            assignPositions();

            paper = Raphael("canvas", width, height);

            if (config.drawLevelLines) {
                drawLevelLines();
            }
            return this;
        };

        function draw() {
            try {
                _.each(input.nodes, function (node) {
                    createNode(node, node._x, node._y);
                });

                var relationships = _.groupBy(input.relationships, function (r) {
                    return r.start
                });
                for (var i in relationships) {
                    var groups = _.groupBy(relationships[i], function (r) {
                        return r.end;
                    });
                    for (var j in groups) {
                        createRelationshipsGroup(groups[j]);
                    }
                }
            } catch (e) {
                alert(JSON.stringify(e));
            }
        }

        function addSurroundingNodes(node) {
            $.getJSON(config.baseUrl + '/nodes/'+ node.id + '/topdown/graph/surroundings', function (data) {

                var angle = 2 * Math.PI / data.nodes.length,
                    r = config.distanceBetweenNodes,
                    x1,
                    y1,
                    t;

                _.chain(data.nodes)
                    .reject(function (n) {
                        return globalIndex.nodes[n.id];
                    })
                    .each(function (n, i) {
                        // put elements on a circle centered around the node
                        t = i * angle;
                        x1 = node._x + r * Math.cos(t);
                        y1 = node._y + r * Math.sin(t);
                        createNode(n, x1, y1, config.surroundingOpacity);

                        n._x = x1;
                        n.y = y1;

                        globalIndex.nodes[n.id] = n;

                        // create connections to and from the new node
                        _.chain(data.relationships)
                            .filter(function (r) {
                                return (r.start === n.id && r.end === node.id) || (r.start === node.id && r.end === n.id);
                            })
                            .each(function (r) {
                                createRelationship(r);
//                                globalIndex.relationships.push(r);
                            });
                    });
            });
        }

        var createNode = function (node, x, y, opacity) {
            opacity = opacity || 1;
            try {
                var elem = renderNode(config.renderMode, x, y, node, opacity);
                renderNodeLabel(x, y, node);

                elem.click(function () {
                        addSurroundingNodes(node);
                    }
                );

                indexElement(elem);
                indexNode(elem);

                return elem;
            } catch (e) {
                alert("createNode: " + e);
            }
        };

        var createRelationshipsGroup = function (group) {
            _.each(group, function (r, i) {
                createRelationship(r, i);
            });
        }

        var createRelationship = function (r, i) {
            try {
                var start = _.find(_elements, function (e) {
                    if (!e.data) return false;
                    return e.data('id') === r.start
                });

                var end = _.find(_elements, function (e) {
                    if (!e.data) return false;
                    return e.data('id') === r.end
                });
                if (start && end) {
                    var highlight = $.inArray(r.id, input.impactPath) >= 0;
                    var elem = paper.connection(config.connectionType, start, end, relationshipColor(r), false, highlight);
                    _elements.push(elem);
                    return elem;
                }
                return null;
            } catch (e) {
                alert("createRelationship: " + e);
            }
        };

        return {
            validateData: validateData,
            createModel:createModel,
            refreshGlobalIndex:refreshGlobalIndex,
            createRelationshipIndex:createRelationshipIndex,
            createNodeIndex:createNodeIndex,
            createInAdjacencyMatrix:createInAdjacencyMatrix,
            partitionGraph:partitionGraph,
            init:init,
            draw:draw
        };

    }
    return {
        graph:graph
    }
}();
