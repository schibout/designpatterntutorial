package fr.sfr.impact.dto;

import java.util.List;

/**
 * DTO representing a result of a bottom-up analysis.
 *
 * @author Michal Bachman
 */
public class BottomUpResultDto extends AnalysisResultDto {

    private List<NodeDto> clients;

    /**
     * Get all the impacted clients.
     *
     * @return impacted clients as DTOs.
     */
    public List<NodeDto> getClients() {
        return clients;
    }

    public void setClients(List<NodeDto> clients) {
        this.clients = clients;
    }
}
