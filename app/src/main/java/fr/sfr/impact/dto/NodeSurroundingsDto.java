package fr.sfr.impact.dto;

import fr.sfr.impact.domain.RelationshipTypeAndDirection;

import java.util.List;
import java.util.Map;

/**
 * DTO with node details and surroundings.
 *
 * @author Michal Bachman
 */
public class NodeSurroundingsDto implements Dto {

    private final String nodeName;
    private final Map<RelationshipTypeAndDirection, List<NodeDto>> surroundings;
    private final boolean singleRelationshipAndDirection;

    public NodeSurroundingsDto(String nodeName, Map<RelationshipTypeAndDirection, List<NodeDto>> surroundings, boolean singleRelationshipAndDirection) {
        this.nodeName = nodeName;
        this.surroundings = surroundings;
        this.singleRelationshipAndDirection = singleRelationshipAndDirection;
    }

    public String getNodeName() {
        return nodeName;
    }

    public Map<RelationshipTypeAndDirection, List<NodeDto>> getSurroundings() {
        return surroundings;
    }

    public boolean isSingleRelationshipAndDirection() {
        return singleRelationshipAndDirection;
    }
}
