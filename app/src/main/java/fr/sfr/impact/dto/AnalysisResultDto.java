package fr.sfr.impact.dto;

import java.util.List;

/**
 * A DTO representing a result of an analysis. To be used as a model for the GUI, or generating Excel sheets.
 *
 * @author Michal Bachman.
 */
public abstract class AnalysisResultDto implements Dto {

    private List<NodeDto> nodeDetails;
    private List<NodeDto> services;
    private List<NodeDto> nodesOnPath;

    /**
     * Get details of the analyzed nodes.
     *
     * @return analyzed nodes as DTOs.
     */
    public List<NodeDto> getNodeDetails() {
        return nodeDetails;
    }

    public void setNodeDetails(List<NodeDto> nodeDetails) {
        this.nodeDetails = nodeDetails;
    }

    /**
     * Get services impacted.
     *
     * @return impacted services as DTOs.
     */
    public List<NodeDto> getServices() {
        return services;
    }

    public void setServices(List<NodeDto> services) {
        this.services = services;
    }

    /**
     * Get all nodes on all paths of this analysis.
     *
     * @return all nodes on paths as DTOs.
     */
    public List<NodeDto> getNodesOnPath() {
        return nodesOnPath;
    }

    public void setNodesOnPath(List<NodeDto> nodesOnPath) {
        this.nodesOnPath = nodesOnPath;
    }
}
