package fr.sfr.impact.dto;

import java.util.List;

public class TopDownResultDto extends AnalysisResultDto {

    private List<NodeDto> equipment;

    public List<NodeDto> getEquipment() {
        return equipment;
    }

    public TopDownResultDto setEquipment(List<NodeDto> equipment) {
        this.equipment = equipment;
        return this;
    }
}
