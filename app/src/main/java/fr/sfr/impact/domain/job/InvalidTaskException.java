package fr.sfr.impact.domain.job;

/**
 * Exception declaring a {@link Task} invalid.
 *
 * @author Michal Bachman
 */
public class InvalidTaskException extends Exception {

    public InvalidTaskException(String s) {
        super(s);
    }

    public InvalidTaskException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
