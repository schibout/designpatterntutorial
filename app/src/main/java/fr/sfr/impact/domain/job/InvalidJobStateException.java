package fr.sfr.impact.domain.job;

/**
 * Runtime exception indicating wrong usage of the {@link Job} API, thus a bug.
 *
 * @author Michal Bachman
 */
public class InvalidJobStateException extends RuntimeException {

    public InvalidJobStateException() {
    }

    public InvalidJobStateException(String s) {
        super(s);
    }

    public InvalidJobStateException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public InvalidJobStateException(Throwable throwable) {
        super(throwable);
    }
}
