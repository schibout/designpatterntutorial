package fr.sfr.impact.domain;

import fr.sfr.impact.dto.NodeDto;
import org.neo4j.graphdb.GraphDatabaseService;

/**
 * Base subclass for classes that represent a node with extra information, but don't need any node details except its Neo4J ID.
 * <p/>
 * Please note that the Neo4J ID should not be exposed to third parties, because it can change when the application restarts.
 *
 * @author Michal Bachman
 */
public class LightNode {

    private final long nodeId;

    /**
     * Construct a new light node.
     *
     * @param nodeId Neo4J node ID of the node this light node represents.
     */
    public LightNode(long nodeId) {
        this.nodeId = nodeId;
    }

    /**
     * Get the Neo4J node ID of the node this light node represents.
     *
     * @return node ID.
     */
    public final long getNodeId() {
        return nodeId;
    }

    /**
     * Produce a DTO from this node.
     *
     * @param database reference to the database.
     * @return DTO representing this node.
     */
    public NodeDto produceDto(GraphDatabaseService database) {
        return new NodeDto(database.getNodeById(nodeId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LightNode nodeId = (LightNode) o;

        if (this.nodeId != nodeId.nodeId) return false;

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return (int) (nodeId ^ (nodeId >>> 32));
    }
}
