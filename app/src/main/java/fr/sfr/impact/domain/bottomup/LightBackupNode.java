package fr.sfr.impact.domain.bottomup;

import fr.sfr.impact.domain.LightNode;
import fr.sfr.impact.dto.NodeDto;
import org.neo4j.graphdb.GraphDatabaseService;

import static fr.sfr.impact.domain.Constants.*;

/**
 * {@link fr.sfr.impact.domain.LightNode} representing a backup node. Backup nodes are nodes attached to abstract nodes and serve as primary/secondary backup.
 * <p/>
 * This class is scoped by {@link LightImpactedService}; thus, it has a package protected constructor.
 *
 * @author Michal Bachman
 */
public class LightBackupNode extends LightNode implements Comparable<LightBackupNode> {

    /**
     * This is a string as this might have other values (like percentages) in the future.
     * Also, the name of the variable is not really in English, but is well understood in SFR.
     */
    private final String partage;
    private NodeStatus status;
    private final LightImpactedService impactedService;

    /**
     * Construct a new light backup node. Only to be called from {@link LightImpactedService}, thus package visibility.
     *
     * @param nodeId          of the node represented by this object.
     * @param partage         primary/secondary (for now).
     * @param status          status of the backup node (OK/KO).
     * @param impactedService for which this is a backup.
     */
    LightBackupNode(long nodeId, String partage, NodeStatus status, LightImpactedService impactedService) {
        super(nodeId);
        this.partage = partage;
        this.status = status;
        this.impactedService = impactedService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDto produceDto(GraphDatabaseService database) {
        NodeDto result = super.produceDto(database);

        //just for display on the GUI + Excel sheets
        NodeDto serviceDto = impactedService.produceDto(database);
        result.setProperty(CLIENT + U_NAME, serviceDto.getProperty(CLIENT + U_NAME));
        result.setProperty(CLIENT + U_NEO_ID, String.valueOf(serviceDto.getProperty(CLIENT + U_NEO_ID)));

        result.setProperty(PARTAGE, partage);
        result.setProperty(STATUS, getStatus().name());

        return result;
    }

    /**
     * Get the status of this backup node.
     *
     * @return the status as determined by the analysis.
     */
    public NodeStatus getStatus() {
        return status;
    }

    public void setStatus(NodeStatus status) {
        this.status = status;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        LightBackupNode that = (LightBackupNode) o;

        if (!partage.equals(that.partage)) return false;
        //status and service deliberately excluded, it's still the same node no matter what its perceived status on the current path of bottom-up

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + partage.hashCode();
        //status and service deliberately excluded, it's still the same node no matter what its perceived status on the current path of bottom-up
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(LightBackupNode lightBackupNode) {
        //a bit hacky, relies on the fact that "p" in "primary" is alphabetically before "s" in "secondary",
        //in achieving primary nodes being listed first.
        return this.partage.compareTo(lightBackupNode.partage);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("LightBackupNode");
        sb.append("{nodeId='").append(getNodeId()).append('\'');
        sb.append(", partage='").append(partage);
        sb.append(", status=").append(status);
        sb.append('}');
        return sb.toString();
    }
}
