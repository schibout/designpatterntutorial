package fr.sfr.impact.domain;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.RelationshipType;

/**
 * Class encapsulating {@link RelationshipType} and {@link Direction}.
 *
 * @author Michal Bachman
 */
public class RelationshipTypeAndDirection {

    private final RelationshipType relationshipType;
    private final Direction direction;

    public RelationshipTypeAndDirection(RelationshipType relationshipType, Direction direction) {
        this.relationshipType = relationshipType;
        this.direction = direction;
    }

    public RelationshipType getRelationshipType() {
        return relationshipType;
    }

    public Direction getDirection() {
        return direction;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RelationshipTypeAndDirection that = (RelationshipTypeAndDirection) o;

        if (direction != that.direction) return false;
        if (!relationshipType.name().equals(that.relationshipType.name())) return false;

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = relationshipType.name().hashCode();
        result = 31 * result + direction.hashCode();
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return relationshipType.name() + "_" + direction.name();
    }
}
