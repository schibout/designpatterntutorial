package fr.sfr.impact.domain.bottomup;

import fr.sfr.impact.domain.Constants;
import fr.sfr.impact.domain.LightNode;
import fr.sfr.impact.dto.NodeDto;
import org.apache.commons.lang.StringUtils;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static fr.sfr.impact.domain.Constants.*;
import static fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes.EST_COMPOSE_DE;
import static fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes.EST_INCLUS_DANS;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.neo4j.graphdb.Direction.OUTGOING;

/**
 * {@link fr.sfr.impact.domain.LightNode} representing a node directly below a service, typically a circuit.
 * <p/>
 * This class is scoped by {@link LightImpactedService}; thus, it has a package protected constructor.
 *
 * @author Michal Bachman
 */
public class LightCircuitNode extends LightNode {

    private static final Logger LOG = LoggerFactory.getLogger(LightCircuitNode.class);
    private static final String MULTIPLE = "MULTIPLE";
    private static final String[] SUFFIXES = new String[]{A, B};    //We assume 2 extremities, A and B.

    private LightImpactedService impactedService;  //Just so the DTO can contain service's info for GUI

    /**
     * Construct a new light circuit node. Only to be called from {@link LightImpactedService}, thus package visibility.
     *
     * @param nodeId of the node represented by this object.
     */
    LightCircuitNode(long nodeId) {
        super(nodeId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDto produceDto(GraphDatabaseService database) {
        NodeDto result = super.produceDto(database);

        //just for display on the GUI + Excel sheets
        NodeDto serviceDto = impactedService.produceDto(database);
        result.setProperty(CLIENT + U_NAME, serviceDto.getProperty(CLIENT + U_NAME));
        result.setProperty(CLIENT + U_NEO_ID, String.valueOf(serviceDto.getProperty(CLIENT + U_NEO_ID)));
        result.setProperty(Constants.STATUS, serviceDto.getProperty(STATUS));

        findAndRegisterExtremities(result, database);

        return result;
    }

    /**
     * Find and register the extremities for this circuit to the DTO.
     *
     * @param result   to add extremities to.
     * @param database reference to Neo, so it can be traversed.
     */
    private void findAndRegisterExtremities(NodeDto result, GraphDatabaseService database) {
        Node circuit = database.getNodeById(getNodeId());

        List<Node> extremities = findExtremities(circuit);

        if (extremities.size() > 2) {
            LOG.warn("There are more than 2 extremities for " + circuit.getProperty(ID, EMPTY).toString());
            result.setProperty(AU4 + U_NAME + A, MULTIPLE);
            return;
        }

        registerExtremities(result, extremities);
    }

    /**
     * Find all extremities for a circuit, ordered by name.
     *
     * @param circuit to find extremities for.
     * @return extremities, ordered by name.
     */
    private List<Node> findExtremities(Node circuit) {
        List<Node> extremities = new ArrayList<Node>();

        for (Relationship r : circuit.getRelationships(EST_COMPOSE_DE, OUTGOING)) {
            Node au4 = r.getOtherNode(circuit);
            if (isAu4(au4)) {
                extremities.add(au4);
            }
        }

        Collections.sort(extremities, new Comparator<Node>() {
            @Override
            public int compare(Node node, Node node1) {
                return node.getProperty(NAME, StringUtils.EMPTY).toString().compareTo(node1.getProperty(NAME, StringUtils.EMPTY).toString());
            }
        });

        return extremities;
    }

    /**
     * Register extremities on the result DTO with their port, card, and equipment.
     *
     * @param result      to add extremities to.
     * @param extremities extremities, there must be a maximum of 2.
     */
    private void registerExtremities(NodeDto result, Collection<Node> extremities) {
        if (extremities.size() > 2) {
            throw new IllegalArgumentException("There must be maximum 2 extremities, this is a bug!");
        }

        int i = 0;
        for (Node extremity : extremities) {
            result.setProperty(AU4 + U_ID + SUFFIXES[i], extremity.getProperty(ID, EMPTY).toString());
            result.setProperty(AU4 + U_NEO_ID + SUFFIXES[i], String.valueOf(extremity.getId()));

            Node port = registerNextIncludedIn(result, extremity, PORT, SUFFIXES[i]);
            if (port == null) {
                continue;
            }

            Node card = registerNextIncludedIn(result, port, CARD, SUFFIXES[i]);
            if (card == null) {
                continue;
            }

            registerNextIncludedIn(result, card, EQUIPMENT, SUFFIXES[i]);
            i++;
        }
    }

    /**
     * Register the next part that is included in the start node.
     *
     * @param result to register to.
     * @param start  to look for an included part.
     * @param type   to look for
     * @param suffix A or B, depending which loop we're in.
     * @return the node that was found to be included in the start node. Null if no such node has been found.
     */
    private Node registerNextIncludedIn(NodeDto result, Node start, String type, String suffix) {
        Relationship includedIn = start.getSingleRelationship(EST_INCLUS_DANS, OUTGOING);
        if (includedIn == null) {
            LOG.warn("No " + type + " is attached to " + start.getProperty(ID, EMPTY).toString());
            return null;
        }
        Node next = includedIn.getOtherNode(start);
        idAndNameToResult(result, type, suffix, next);
        return next;
    }

    private void idAndNameToResult(NodeDto result, String propertyName, String suffix, Node node) {
        result.setProperty(propertyName + U_NAME + suffix, node.getProperty(NAME, EMPTY).toString());
        result.setProperty(propertyName + U_NEO_ID + suffix, String.valueOf(node.getId()));
    }

    private boolean isAu4(Node node) {
        return AU4.equalsIgnoreCase(node.getProperty(TYPE, EMPTY).toString());
    }

    void setImpactedService(LightImpactedService impactedService) {
        this.impactedService = impactedService;
    }
}
