package fr.sfr.impact.domain.bottomup;

/**
 * Possible statuses of a {@link LightImpactedService}.
 */
public enum ServiceStatus {
    OK, NOT_SECURE, KO
}
