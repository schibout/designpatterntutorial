package fr.sfr.impact.domain.topdown;

import org.codehaus.jackson.annotate.JsonProperty;

public class NodeDesc {


    private long id;

    private String name;

    private String type;

    private String level;

    public NodeDesc(long id, String name, String type, String level) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.level = level;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("NAME")
    public String getName() {
        return name;
    }

    @JsonProperty("TYPE")
    public String getType() {
        return type;
    }

    @JsonProperty("LEVEL")
    public String getLevel() {
        return level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NodeDesc nodeDesc = (NodeDesc) o;

        if (id != nodeDesc.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
