package fr.sfr.impact.domain.bottomup;

import fr.sfr.impact.domain.Constants;
import fr.sfr.impact.domain.LightNode;
import org.apache.commons.lang.StringUtils;
import org.neo4j.graphdb.Node;

import java.util.*;

/**
 * {@link fr.sfr.impact.domain.LightNode} representing an impacted client, i.e. client directly in path of a bottom-up analysis.
 * <p/>
 * This class is scoped by {@link BottomUpAnalysis}; thus, it has a package protected constructor.
 *
 * @author Michal Bachman
 */
public class LightImpactedClient extends LightNode implements Comparable<LightImpactedClient> {

    private final String name;
    private final Map<Long, LightImpactedService> impactedServices = new HashMap<Long, LightImpactedService>();

    /**
     * Construct a new impacted client. Only to be called from {@link BottomUpAnalysis}, thus package visibility.
     *
     * @param nodeId ID of the node this client represents.
     * @param name   client name to sort by.
     */
    LightImpactedClient(long nodeId, String name) {
        super(nodeId);
        this.name = name;
    }

    /**
     * Get an impacted service for this client by either retrieving it, or creating a new one if one does not yet exist.
     *
     * @param serviceNode Neo4J node representing the impacted service.
     * @return a service, never null.
     */
    public LightImpactedService getOrCreateImpactedService(String startNodeId, Node serviceNode, Node circuitNode) {
        LightImpactedService impactedService = impactedServices.get(serviceNode.getId());
        if (impactedService == null) {
            LightCircuitNode lightCircuitNode = null;
            if (circuitNode != null) {
                lightCircuitNode = new LightCircuitNode(circuitNode.getId());
            }
            impactedService = new LightImpactedService(serviceNode.getId(), serviceNode.getProperty(Constants.NAME, StringUtils.EMPTY).toString(), this, lightCircuitNode);
            impactedServices.put(serviceNode.getId(), impactedService);
        }
        impactedService.addImpactingNodeId(startNodeId);
        return impactedService;
    }

    /**
     * Get all the impacted services for this client and their backups.
     *
     * @return a read-only list of all the impacted services with backups.
     */
    public Collection<LightNode> getImpactedServicesWithCircuitAndBackups() {
        List<LightNode> result = new LinkedList<LightNode>();             //NOTE: this is a list because some backups can repeat (services can share backups)
        for (LightImpactedService impactedService : getImpactedServices()) {
            result.add(impactedService);
            LightCircuitNode circuitNode = impactedService.getCircuitNode();
            if (circuitNode != null) {
                result.add(circuitNode);
            }
            result.addAll(impactedService.getBackupNodes());
        }
        return Collections.unmodifiableList(result);
    }

    /**
     * Get impacted services sorted by name.
     *
     * @return a sorted set of impacted services.
     */
    public Collection<LightImpactedService> getImpactedServices() {
        return new TreeSet<LightImpactedService>(impactedServices.values());
    }

    /**
     * Get the name of this client.
     *
     * @return name of the client.
     */
    public String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        LightImpactedClient that = (LightImpactedClient) o;

        if (!impactedServices.equals(that.impactedServices)) return false;
        if (!name.equals(that.name)) return false;

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(LightImpactedClient lightImpactedClient) {
        return this.name.compareTo(lightImpactedClient.name);
    }
}
