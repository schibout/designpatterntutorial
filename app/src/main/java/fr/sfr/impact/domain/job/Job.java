package fr.sfr.impact.domain.job;

import java.io.IOException;
import java.util.*;

/**
 * A job, defined by a {@link JobDescription}, which ultimately produces a byte stream representing a zip file.
 * Intended to be run asynchronously. The result of the job should be streamed to a user via HTTP response when the job has been finished.
 *
 * @author Michal Bachman
 */
public class Job {
    private static final int MAX_NUMBER_OF_TASKS = 100;
    private static final int FINISHED_OR_ERROR_JOB_MAX_AGE_MS = 60 * 60 * 1000; //1 hour

    private final JobDescription jobDescription;
    private final JobStatus jobStatus = new JobStatus();
    private final JobResult jobResult = new JobResult();
    private final long createdTimestamp = System.currentTimeMillis();

    private int finishedOrErrorJobMaxAgeMs = FINISHED_OR_ERROR_JOB_MAX_AGE_MS;

    private transient int numberOfTasks;
    private transient int finishedTasks = 0;

    /**
     * Construct a new job with the given job description.
     *
     * @param jobDescription to construct a new job with.
     * @return new job.
     */
    public static Job withDescription(JobDescription jobDescription) {
        return new Job(jobDescription);
    }

    /**
     * Private constructor so that the #withDescription factory method must be used.
     *
     * @param jobDescription to construct this job with.
     */
    private Job(JobDescription jobDescription) {
        this.jobDescription = jobDescription;
    }

    /**
     * Get the tasks that this job encapsulates.
     *
     * @return tasks. Empty collection if there are no tasks,
     *         or if an exception occurred while producing the tasks (typically due to invalid job description),
     *         in which case the status of this job will be set to ERROR as well.
     */
    public List<Task> getTasks() {
        if (!JobStatus.Status.IN_PROGRESS.equals(getStatus())) {
            throw new InvalidJobStateException("Job must be started before retrieving tasks, this is a bug!");
        }
        try {
            List<Task> tasks = jobDescription.produceTasks();
            numberOfTasks = tasks.size();
            if (numberOfTasks > MAX_NUMBER_OF_TASKS) {
                error("Maximum number of tasks has been exceeded (" + MAX_NUMBER_OF_TASKS + ")");
                return Collections.emptyList();
            }
            if (tasks.isEmpty()) {
                error("There are no tasks to execute");
            }
            return tasks;
        } catch (InvalidTaskException e) {
            error(e.getMessage());
            return Collections.emptyList();
        }
    }

    /**
     * Get locale of the user who created the Job, so that correct messages can be used in the output.
     *
     * @return locale.
     */
    public Locale getLocale() {
        return jobDescription.getLocale();
    }

    /**
     * Get current status of the job.
     *
     * @return status.
     */
    public JobStatus.Status getStatus() {
        return jobStatus.getStatus();
    }

    /**
     * Get percentage of this job that is finished.
     *
     * @return percentage, 0-100 inclusive.
     */
    public int getPercentFinished() {
        return jobStatus.getPercentFinished();
    }

    /**
     * Get an error message for this job.
     *
     * @return error message. Will be null unless the job's status is ERROR, in which case it will return the reason for the error.
     */
    public String getErrorMessage() {
        return jobStatus.getErrorMessage();
    }

    /**
     * Get the result of this job as a byte array intended to be streamed into an HTTP response.
     *
     * @return result as byte array.
     */
    public JobResult getJobResult() {
        if (!JobStatus.Status.FINISHED.equals(getStatus())) {
            throw new InvalidJobStateException("Job must be finished before retrieving results, this is a bug!");
        }
        return jobResult;
    }

    /**
     * Initialize the job and mark it as started.
     */
    public void start() {
        if (!JobStatus.Status.NOT_STARTED.equals(getStatus())) {
            throw new InvalidJobStateException("Job must be started once only when in NOT_STARTED state, this is a bug!");
        }
        jobResult.initialize();
        jobStatus.start();
    }

    /**
     * Add a result to this job, i.e. a file which has a name and contents. It will added to the final zip file.
     *
     * @param fileName     name.
     * @param fileContents contents.
     */
    public void addResult(String fileName, byte[] fileContents) {
        if (!JobStatus.Status.IN_PROGRESS.equals(getStatus())) {
            throw new InvalidJobStateException("Results can only be added to a job IN_PROGRESS, this is a bug!");
        }
        if (numberOfTasks == 0) {
            throw new InvalidJobStateException("The job thinks it has 0 tasks. Has the getTasks() method been called before adding results? This is a bug.");
        }
        jobResult.addFile(fileName, fileContents);
        jobStatus.setProgress((100 * ++finishedTasks) / numberOfTasks);
    }

    /**
     * Finalize the job and mark it finished.
     */
    public void finish() {
        if (!JobStatus.Status.IN_PROGRESS.equals(getStatus())) {
            throw new InvalidJobStateException("Job must be finished only when IN_PROGRESS, this is a bug!");
        }
        try {
            jobResult.terminate();
            jobStatus.finish();
        } catch (IOException e) {
            error(e.getMessage());
        }
    }

    /**
     * Mark the job as being in an ERROR state.
     *
     * @param errorMessage reason for the state.
     */
    public void error(String errorMessage) {
        jobStatus.error(errorMessage);
    }

    /**
     * Should the job be removed from memory?
     *
     * @return true if and only if it should be removed, i.e. it is old enough and either finished or error.
     */
    public boolean shouldBeRemoved() {
        return isFinishedOrError() && hasReachedMaxAge();
    }

    private boolean isFinishedOrError() {
        return JobStatus.Status.ERROR.equals(getStatus()) || JobStatus.Status.FINISHED.equals(getStatus());
    }

    private boolean hasReachedMaxAge() {
        return new Date().getTime() - createdTimestamp > finishedOrErrorJobMaxAgeMs;
    }

    public void setFinishedOrErrorJobMaxAgeMs(int finishedOrErrorJobMaxAgeMs) {
        this.finishedOrErrorJobMaxAgeMs = finishedOrErrorJobMaxAgeMs;
    }
}
