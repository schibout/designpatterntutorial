package fr.sfr.impact.domain.topdown;

import fr.sfr.impact.domain.AbstractAnalysis;
import fr.sfr.impact.domain.Analysis;
import fr.sfr.impact.domain.LightNode;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Top-down analysis for a node.
 * <p/>
 * The intention of this data structure is that it is very lightweight and as normalized as possible (so that it can be cached)
 * but must be able to answer all questions it needs using its data structures (without DB access), except when producing DTOs.
 *
 * @author Michal Bachman
 * //todo - this isn't really tested and should be
 */
public class TopDownAnalysis extends AbstractAnalysis implements Analysis {

    private final Set<LightNode> impactedServices = new HashSet<LightNode>();
    private final Set<LightNode> impactedEquipment = new HashSet<LightNode>();

    /**
     * {@inheritDoc}
     */
    public TopDownAnalysis(Long... nodeIds) {
        super(nodeIds);
    }

    /**
     * {@inheritDoc}
     */
    public TopDownAnalysis(Set<Long> nodeIds) {
        super(nodeIds);
    }

    /**
     * Get all impacted services.
     *
     * @return all impacted services as Neo4J node IDs.
     */
    public Set<LightNode> getServices() {
        return Collections.unmodifiableSet(impactedServices);
    }

    /**
     * Add an impacted service.
     *
     * @param impactedService Neo4J node ID of the impacted service.
     * @return true of and only if the node wasn't already present.
     */
    public boolean addImpactedService(long impactedService) {
        return impactedServices.add(new LightNode(impactedService));
    }

    /**
     * Get all impacted equipment.
     *
     * @return all impacted equipment as Neo4J node IDs.
     */
    public Set<LightNode> getEquipment() {
        return Collections.unmodifiableSet(impactedEquipment);
    }

    /**
     * Add an impacted equipment.
     *
     * @param impactedEquipment Neo4J node ID of the impacted equipment node.
     * @return true of and only if the node wasn't already present.
     */
    public boolean addImpactedEquipment(long impactedEquipment) {
        return this.impactedEquipment.add(new LightNode(impactedEquipment));
    }
}
