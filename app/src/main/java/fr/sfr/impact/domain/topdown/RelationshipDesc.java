package fr.sfr.impact.domain.topdown;

import org.codehaus.jackson.annotate.JsonProperty;

public class RelationshipDesc {

    private long id;

    private String type;

    private long start;

    private long end;

    public RelationshipDesc(long id, String type, long start, long end) {
        this.id = id;
        this.type = type;
        this.start = start;
        this.end = end;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("start")
    public long getStart() {
        return start;
    }

    @JsonProperty("end")
    public long getEnd() {
        return end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RelationshipDesc that = (RelationshipDesc) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
