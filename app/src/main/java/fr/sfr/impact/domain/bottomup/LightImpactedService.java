package fr.sfr.impact.domain.bottomup;

import fr.sfr.impact.domain.Constants;
import fr.sfr.impact.domain.LightNode;
import fr.sfr.impact.dto.NodeDto;
import org.apache.commons.lang.StringUtils;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import java.util.*;

import static fr.sfr.impact.domain.Constants.*;
import static fr.sfr.impact.domain.bottomup.NodeStatus.KO;
import static fr.sfr.impact.domain.bottomup.NodeStatus.NOT_SECURE;
import static fr.sfr.impact.domain.bottomup.NodeStatus.OK;

/**
 * {@link LightNode} representing a service impacted by the bottom-up analysis.
 * <p/>
 * Scoped by {@link LightImpactedClient}, this package protected constructor.
 *
 * @author Michal Bachman
 */
public class LightImpactedService extends LightNode implements Comparable<LightImpactedService> {

    private final String name; //to sort by
    private final LightImpactedClient impactedClient;
    private final LightCircuitNode circuitNode;
    private ServiceStatus status;
    private Set<String> impactingNodes = new LinkedHashSet<String>();

    /*  A set of all the impacted abstract nodes leading to this service.
        This set is needed because this service might be impacted from different independent paths
        and we need to keep track of all those to identify the final status of the node.
     */
    private final Set<Long> abstractNodeIds = new LinkedHashSet<Long>();

    /**
     * Backup nodes keyed by abstract node.
     */
    private final Map<Long, Set<LightBackupNode>> backupNodes = new HashMap<Long, Set<LightBackupNode>>();

    /**
     * Construct a new impacted service. Only to be called from {@link LightImpactedClient}.
     *
     * @param nodeId         Neo4J node ID of this service.
     * @param name           of the service for sorting.
     * @param impactedClient to which this service belongs.
     * @param circuitNode    immediately below the service, can be null if none exists.
     */
    LightImpactedService(long nodeId, String name, LightImpactedClient impactedClient, LightCircuitNode circuitNode) {
        super(nodeId);
        this.name = name;
        this.impactedClient = impactedClient;
        this.circuitNode = circuitNode;
        if (circuitNode != null) {
            this.circuitNode.setImpactedService(this);
        }
    }

    /**
     * Discover and add abstract and backup nodes to this impacted service.
     *
     * @param nodesOnPath all nodes on path causing this service to be impacted.
     */
    public void addBackupNodesFromPath(Set<Node> nodesOnPath) {
        for (Node abstractNode : getAbstractNodes(nodesOnPath)) {
            addAbstractNode(abstractNode);
            addBackupNodes(abstractNode, nodesOnPath);
        }
    }

    /**
     * Add an abstract node to this service.
     *
     * @param abstractNode Neo4J of the abstract node to add.
     * @return true if and only if the node didn't exist and was added.
     */
    private void addAbstractNode(Node abstractNode) {
        long abstractNodeId = abstractNode.getId();
        abstractNodeIds.add(abstractNodeId);
        if (!backupNodes.containsKey(abstractNode)) {
            //!!! TreeSet is needed here to display nodes in right order. Tests depends on that order too.
            backupNodes.put(abstractNodeId, new TreeSet<LightBackupNode>());
        }
    }

    /**
     * Discover and add all backup nodes to this impacted service.
     *
     * @param abstractNode that has an immediate relationship with the backup nodes.
     * @param nodesOnPath  all nodes on path causing this service to be impacted.
     */
    private void addBackupNodes(Node abstractNode, Set<Node> nodesOnPath) {
        for (Relationship r : abstractNode.getRelationships(Direction.OUTGOING)) {
            if (r.hasProperty(Constants.PARTAGE)) {
                Node backupNode = r.getOtherNode(abstractNode);
                //This does not take into account that there could be multiple layers of security:
                NodeStatus status = nodesOnPath.contains(backupNode) ? KO : OK;
                addBackupNode(abstractNode.getId(), backupNode.getId(), r.getProperty(Constants.PARTAGE).toString(), status);
            }
        }
    }

    /**
     * Add a backup node. If the backup node already exists, then it is replaced only by a K.O. node. Never replaced by an O.K. node.
     * This is because once a node is marked K.O., it should not become O.K. just because it is OK from another path's perspective.
     *
     * @param abstractNode ID of the abstract node, for which the backup node is being added. Assumes this exists in the analysis.
     * @param backupNode   ID of the backup node.
     * @param partage      primary or secondary.
     * @param nodeStatus   status of the backup node (OK or KO).
     */
    private void addBackupNode(long abstractNode, long backupNode, String partage, NodeStatus nodeStatus) {
        addBackupNode(abstractNode, new LightBackupNode(backupNode, partage, nodeStatus, this));
    }

    /**
     * Add a backup node. If the backup node already exists, then it is replaced only by a K.O. node. Never replaced by an O.K. node.
     * This is because once a node is marked K.O., it should not become O.K. just because it is OK from another path's perspective.
     *
     * @param abstractNode    ID of the abstract node, for which the backup node is being added. Assumes this exists in the analysis.
     * @param lightBackupNode backup node to add.
     * @throws IllegalArgumentException in case the abstract node does not exist / is not part of this analysis.
     */
    private void addBackupNode(long abstractNode, LightBackupNode lightBackupNode) {
        if (!backupNodes.containsKey(abstractNode)) {
            throw new IllegalArgumentException("Node " + abstractNode + " is not an abstract node in this analysis");
        }
        if (!backupNodes.get(abstractNode).contains(lightBackupNode) || NodeStatus.KO.equals(lightBackupNode.getStatus())) {
            backupNodes.get(abstractNode).remove(lightBackupNode);
            backupNodes.get(abstractNode).add(lightBackupNode);
        }
    }

    /**
     * Get abstract nodes from all nodes on path.
     *
     * @param nodesOnPath all nodes on path.
     * @return only abstract nodes from the nodesOnPath.
     */
    private List<Node> getAbstractNodes(Set<Node> nodesOnPath) {
        List<Node> result = new ArrayList<Node>();
        for (Node node : nodesOnPath) {
            if (isAbstract(node)) {
                result.add(node);
            }
        }
        return result;
    }

    /**
     * Check if a given node is abstract.
     *
     * @param node to check.
     * @return true if and only if the node is abstract.
     */
    private static boolean isAbstract(Node node) {
        boolean res = ABSTRACT.equalsIgnoreCase(node.getProperty(TYPO, StringUtils.EMPTY).toString());
        return res;
    }

    //****** BELOW IS READING CODE, ABOVE POPULATING CODE ******

    /**
     * Get the circuit directly below this service in the impact path.
     *
     * @return circuit, can be null!
     */
    public LightCircuitNode getCircuitNode() {
        return circuitNode;
    }

    /**
     * Get all backup nodes for a this impacted service.
     *
     * @return all backup nodes for this service.
     */
    public Set<LightBackupNode> getBackupNodes() {
        Set<LightBackupNode> result = new LinkedHashSet<LightBackupNode>();
        for (Long abstractNodeId : backupNodes.keySet()) {
            result.addAll(backupNodes.get(abstractNodeId));
        }
        return result;
    }

    public Map<Long, Set<LightBackupNode>> getBackupNodesMap() {
        return Collections.unmodifiableMap(backupNodes);
    }

    public Set<Long> getKOBackupNodesId() {
        Set<Long> res = new LinkedHashSet<Long>();

        for (Set<LightBackupNode> nodes : backupNodes.values()) {
            for (LightBackupNode node : nodes) {
                if (NodeStatus.KO.equals(node.getStatus())) {
                    res.add(node.getNodeId());
                }
            }
        }
        return Collections.unmodifiableSet(res);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDto produceDto(GraphDatabaseService database) {
        NodeDto result = super.produceDto(database);
        NodeDto clientDto = impactedClient.produceDto(database);
        result.setProperty(CLIENT + U_NAME, clientDto.getProperty(NAME));
        result.setProperty(CLIENT + U_NEO_ID, String.valueOf(clientDto.getNeoId()));
        result.setProperty(Constants.STATUS, getStatus().name());
        result.setImpactingNodes(Collections.unmodifiableSet(impactingNodes));
        return result;
    }

    /**
     * Determine the status of this service based on its backup nodes.
     *
     * @return status.
     */
    public ServiceStatus getStatus() {
        return status;
    }

    public void setStatus(ServiceStatus status) {
        this.status = status;
        //TODO update the status of all the nodes that led to this one
    }

    /**
     * Does each abstract node of this service have at least one OK backup?
     *
     * @return true if and only if (see above).
     */
    private boolean hasEachAbstractNodeAtLeastOneOKBackup() {
        if (backupNodes.isEmpty()) {
            return false;
        }
        for (long abstractNodeId : backupNodes.keySet()) {
            if (!hasAbstractNodeAtLeastOneOKBackup(abstractNodeId)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Does the given abstract node have at least 1 backup node that is OK?
     *
     * @param abstractNodeId to check.
     * @return true if and only if there is at least 1 OK backup node for the given abstract node.
     */
    private boolean hasAbstractNodeAtLeastOneOKBackup(long abstractNodeId) {
        for (LightBackupNode lightBackupNode : backupNodes.get(abstractNodeId)) {
            if (NodeStatus.OK.equals(lightBackupNode.getStatus())) {
                return true;
            }
        }
        return false;
    }

    public void addImpactingNodeId(String id) {
        impactingNodes.add(id);
    }

    public Set<String> getImpactingNodes() {
        return Collections.unmodifiableSet(impactingNodes);
    }

    public Set<Long> getAbstractNodeIds() {
        return Collections.unmodifiableSet(abstractNodeIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        LightImpactedService that = (LightImpactedService) o;

        if (!backupNodes.equals(that.backupNodes)) return false;
        if (circuitNode != null ? !circuitNode.equals(that.circuitNode) : that.circuitNode != null) return false;
        if (!impactedClient.equals(that.impactedClient)) return false;
        if (!name.equals(that.name)) return false;

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + (circuitNode != null ? circuitNode.hashCode() : 0);
        result = 31 * result + backupNodes.hashCode();
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(LightImpactedService lightImpactedService) {
        return this.name.compareTo(lightImpactedService.name);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("LightImpactedService");
        sb.append("{id='").append(getNodeId()).append('\'');
        sb.append(", name='").append(name);
        sb.append(", status=").append(status);
        sb.append('}');
        return sb.toString();
    }
}
