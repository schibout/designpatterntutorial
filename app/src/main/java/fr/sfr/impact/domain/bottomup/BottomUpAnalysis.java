package fr.sfr.impact.domain.bottomup;

import fr.sfr.impact.domain.AbstractAnalysis;
import fr.sfr.impact.domain.Analysis;
import fr.sfr.impact.domain.LightNode;
import org.neo4j.graphdb.Node;

import java.util.*;

import static fr.sfr.impact.domain.Constants.NAME;
import static org.apache.commons.lang.StringUtils.EMPTY;

/**
 * Bottom-up analysis for a set of nodes.
 * <p/>
 * The intention of this data structure is that it is very lightweight and as normalized as possible (so that it can be cached)
 * but must be able to answer all questions it needs using its data structures (without DB access), except when producing DTOs.
 *
 * @author Michal Bachman
 */
public class BottomUpAnalysis extends AbstractAnalysis implements Analysis {

    /**
     * Impacted clients, keyed by client Neo node ID.
     */
    private final Map<Long, LightImpactedClient> impactedClients = new HashMap<Long, LightImpactedClient>();

    /**
     * {@inheritDoc}
     */
    public BottomUpAnalysis(Long... nodeIds) {
        super(nodeIds);
    }

    /**
     * {@inheritDoc}
     */
    public BottomUpAnalysis(Set<Long> nodeIds) {
        super(nodeIds);
    }

    /**
     * Acknowledge an impacted client, the impacted node immediately below the client (typically, but not necessarily a service),
     * and the impacted node immediately below the service (typically, but not necessarily a circuit).
     *
     * @param client      impacted client.
     * @param service     impacted service.
     * @param circuit     circuit directly below service, can be null which means no circuit (or equivalent) can be found.
     * @param nodesOnPath all nodes on path that causes this client to be impacted.
     */
    public LightImpactedService addImpactedClientAndService(String startNodeId, Node client, Node service, Node circuit, Set<Node> nodesOnPath) {
        LightImpactedClient impactedClient = getOrCreateImpactedClient(client);
        LightImpactedService impactedService = impactedClient.getOrCreateImpactedService(startNodeId, service, circuit);
        impactedService.addBackupNodesFromPath(nodesOnPath);
        return impactedService;
    }

    /**
     * Get an impacted client by either retrieving it, or creating a new one if one does not yet exist.
     *
     * @param clientNode Neo4J node representing the impacted client.
     * @return a client, never null.
     */
    private LightImpactedClient getOrCreateImpactedClient(Node clientNode) {
        LightImpactedClient impactedClient = getImpactedClient(clientNode.getId());
        if (impactedClient == null) {
            impactedClient = new LightImpactedClient(clientNode.getId(), clientNode.getProperty(NAME, EMPTY).toString());
            impactedClients.put(clientNode.getId(), impactedClient);
        }
        return impactedClient;
    }

    /**
     * Get an impacted client by ID.
     *
     * @param client Neo4J node ID of the impacted client.
     * @return impacted client, null if one does not exist in this analysis.
     */
    public LightImpactedClient getImpactedClient(long client) {
        return impactedClients.get(client);
    }

    /**
     * Get a collection of all clients impacted by the analysis.
     *
     * @return all clients in a naturally ordered set.
     */
    public Collection<LightImpactedClient> getImpactedClients() {
        //TreeSet is needed here to display nodes in right order. Tests depends on that order too.
        return Collections.unmodifiableCollection(new TreeSet<LightImpactedClient>(impactedClients.values()));
    }

    /**
     * Get a collection of all services impacted by the analysis with circuits and potentially backups.
     *
     * @return a collection of services, each followed by the circuit immediately below the service in the analysis (if available)
     *         and backups (if available). These chunks themselves are ordered by client and then service name.
     */
    public Collection<LightNode> getImpactedServices() {
        List<LightNode> result = new LinkedList<LightNode>(); //list because backups can repeat if clients share them.
        for (LightImpactedClient impactedClient : getImpactedClients()) {
            result.addAll(impactedClient.getImpactedServicesWithCircuitAndBackups());
        }
        return result;
    }
}
