package fr.sfr.impact.domain;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Abstract super-class for results of an {@link fr.sfr.impact.service.AnalysisService}.
 *
 * @author Michal Bachman
 */
public abstract class AbstractAnalysis {

    private final Set<Long> analysedNodes = new HashSet<Long>();
    private final Set<LightNode> impactedNodes = new HashSet<LightNode>();

    /**
     * Construct a new result.
     *
     * @param nodeIds Neo4J IDs of the nodes that the analysis is for.
     */
    public AbstractAnalysis(Long... nodeIds) {
        analysedNodes.addAll(Arrays.asList(nodeIds));
    }

    /**
     * Construct a new result.
     *
     * @param nodeIds Neo4J IDs of the nodes that the analysis is for.
     */
    public AbstractAnalysis(Set<Long> nodeIds) {
        analysedNodes.addAll(nodeIds);
    }

    /**
     * Get all the analysed nodes that led to this result.
     *
     * @return all analysed nodes.
     */
    public Set<Long> getAnalysedNodes() {
        return Collections.unmodifiableSet(analysedNodes);
    }

    /**
     * Get all impacted nodes.
     *
     * @return all impacted nodes as Neo4J node IDs.
     */
    public Set<LightNode> getNodesOnPath() {
        return Collections.unmodifiableSet(impactedNodes);
    }

    /**
     * Add an impacted node.
     *
     * @param impactedNode Neo4J node ID of the impacted node.
     * @return true of and only if the node wasn't already present.
     */
    public boolean addImpactedNode(long impactedNode) {
        return impactedNodes.add(new LightNode(impactedNode));
    }
}
