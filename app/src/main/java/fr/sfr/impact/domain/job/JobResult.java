package fr.sfr.impact.domain.job;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * A result of a {@link Job}. Essentially an array of bytes to be streamed via HTTP response.
 *
 * @author Michal Bachman
 */
public class JobResult {

    private byte[] result;

    private transient ByteArrayOutputStream byteArrayOutputStream;
    private transient ZipOutputStream zipOutputStream;

    /**
     * Initialize the result by opening a zip input stream to add files to.
     */
    void initialize() {
        byteArrayOutputStream = new ByteArrayOutputStream();
        zipOutputStream = new ZipOutputStream(byteArrayOutputStream);
        zipOutputStream.setLevel(ZipOutputStream.STORED);
    }

    /**
     * Add a file to this result (i.e. to the zip file that will be produced by this result).
     *
     * @param fileName     name of the file being added to the zip archive.
     * @param fileContents contents of the file.
     */
    void addFile(String fileName, byte[] fileContents) {
        addOneFileToZipArchive(zipOutputStream, fileName, fileContents);
    }

    /**
     * Close all the streams and store the final zip file as a byte array for later use.
     *
     * @throws IOException
     */
    void terminate() throws IOException {
        zipOutputStream.flush();
        zipOutputStream.close();
        zipOutputStream = null;
        result = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream = null;
    }

    /**
     * Get this result as a byte array.
     *
     * @return this result.
     */
    public byte[] toByteArray() {
        return result;
    }

    private static void addOneFileToZipArchive(final ZipOutputStream zipStream, String fileName, byte[] content) {
        ZipEntry zipEntry = new ZipEntry(fileName);
        try {
            zipStream.putNextEntry(zipEntry);
            zipStream.write(content);
            zipStream.closeEntry();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
