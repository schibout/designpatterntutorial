package fr.sfr.impact.domain.bottomup;

/**
 * Possible statuses of a {@link LightBackupNode}.
 *
 * @author Michal Bachman
 */
public enum NodeStatus {
    OK, KO, NOT_SECURE
}
