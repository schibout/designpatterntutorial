package fr.sfr.impact.domain.job;

/**
 * Status of a {@link Job}. Contains information whether it has been started, in progress, finished, or failed (error).
 * In the success cases, it contains information about the progress. In case of an error, it contains the reason for the error.
 *
 * @author Michal Bachman
 */
public class JobStatus {

    public enum Status {NOT_STARTED, IN_PROGRESS, FINISHED, ERROR}

    private Status status = Status.NOT_STARTED;
    private String errorMessage;
    private int percentFinished = 0;

    /**
     * Get the actual status of the job.
     *
     * @return job status.
     */
    Status getStatus() {
        return status;
    }

    /**
     * Get the job's progress in percent.
     *
     * @return 0 - 100 inclusive.
     */
    int getPercentFinished() {
        return percentFinished;
    }

    /**
     * Get an error message for this job.
     *
     * @return error message. Will be null unless the status is ERROR, in which case it will return the reason for the error.
     */
    String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Set the status to error, providing a reason for that.
     *
     * @param errorMessage the reason for the error.
     */
    void error(String errorMessage) {
        setStatus(Status.ERROR);
        this.errorMessage = errorMessage;
    }

    /**
     * Set the progress to a given percentage.
     *
     * @param percentage percentage indicating progress.
     */
    void setProgress(int percentage) {
        this.percentFinished = percentage;
    }

    /**
     * Mark this status as in progress.
     */
    void start() {
        setStatus(Status.IN_PROGRESS);
    }

    /**
     * Mark this status as successfully finished.
     */
    void finish() {
        this.percentFinished = 100;
        setStatus(Status.FINISHED);
    }

    private void setStatus(Status status) {
        this.status = status;
    }
}
