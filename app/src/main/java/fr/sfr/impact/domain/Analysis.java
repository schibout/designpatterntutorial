package fr.sfr.impact.domain;

/**
 * Marker interface for a result of an {@link fr.sfr.impact.service.AnalysisService}.
 *
 * @author Michal Bachman
 */
public interface Analysis {
}
