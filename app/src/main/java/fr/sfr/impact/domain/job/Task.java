package fr.sfr.impact.domain.job;

import java.util.Set;

/**
 * Task representing an asynchronous bottom-up / top-down analysis to be performed for nodes.
 *
 * @author Michal Bachman
 */
public class Task {

    public enum TaskType {
        /**
         * Bottom-Up
         */
        BU,
        /**
         * Top-Down
         */
        TD
    }

    private final Set<String> nodeIds;
    private final TaskType taskType;

    /**
     * Construct a new task.
     *
     * @param nodeIds   SFR's IDs of the nodes to perform analysis for.
     * @param taskType type of the analysis (bottom-up/top-down).
     */
    public Task(Set<String> nodeIds, TaskType taskType) {
        this.nodeIds = nodeIds;
        this.taskType = taskType;
    }

    public Set<String> getNodeIds() {
        return nodeIds;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (!nodeIds.equals(task.nodeIds)) return false;
        if (taskType != task.taskType) return false;

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = nodeIds.hashCode();
        result = 31 * result + taskType.hashCode();
        return result;
    }
}
