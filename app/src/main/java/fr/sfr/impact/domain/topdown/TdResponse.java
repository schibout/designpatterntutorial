package fr.sfr.impact.domain.topdown;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.*;

public class TdResponse {

    private Collection<NodeDesc> nodes;
    private Collection<RelationshipDesc> relationships;

    private Set<Long> impactPath = Collections.emptySet();

    public TdResponse(Collection<NodeDesc> nodes, Collection<RelationshipDesc> relationships) {
        this.nodes = new LinkedHashSet<NodeDesc>(nodes);
        this.relationships = new LinkedHashSet<RelationshipDesc>(relationships);
    }

    @JsonProperty("nodes")
    public Collection<NodeDesc> getNodes() {
        return nodes;
    }

    @JsonProperty("relationships")
    public Collection<RelationshipDesc> getRelationships() {
        return relationships;
    }

    @JsonProperty("impactPath")
    public Set<Long> getImpactPath() {
        return impactPath;
    }

    public void setImpactPath(Set<Long> impactPath) {
        this.impactPath = impactPath;
    }

    @Override
    public String toString() {
        return "TdResponse{" +
                "nodes=" + nodes +
                ", relationships=" + relationships +
                ", impactPath=" + impactPath +
                '}';
    }
}
