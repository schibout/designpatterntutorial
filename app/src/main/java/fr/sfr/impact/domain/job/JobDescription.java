package fr.sfr.impact.domain.job;

import fr.sfr.impact.util.FileScanner;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.IOException;
import java.util.*;

/**
 * A {@link Job}'s description, containing the {@link Task}s to be executed in the form of byte array read from
 * the original file that a user has uploaded. The uploaded file should contain lines, each line should have a SFR node ID,
 * followed by a semicolon, followed by either "BU" for bottom-up analysis, or "TD" for top-down analysis to be performed on the node.
 *
 * @author Michal Bachman
 */
public class JobDescription {

    private CommonsMultipartFile fileContents;
    private byte[] fileContentsAsBytes;
    private Locale locale;

    /**
     * No-arg constructor for Spring MVC.
     */
    public JobDescription() {
    }

    /**
     * Produce tasks out of this job description. There will be as many tasks as there were lines in the description file,
     * plus one extra task for a multi-impact analysis of all the BU nodes together. This is, as if they were all down
     * at the same time.
     *
     * @return a read-only list of tasks.
     * @throws InvalidTaskException in case any of the tasks is invalid.
     */
    List<Task> produceTasks() throws InvalidTaskException {
        List<Task> result = new LinkedList<Task>();
        for (String line : FileScanner.produceLines(fileContentsAsBytes)) {
            result.add(produceTask(line));
        }
        addBottomUpMulti(result);
        return Collections.unmodifiableList(result);
    }

    /**
     * Add a special task to the list of tasks, which is a bottom-up analysis performed
     * for multiple nodes at the same time. These nodes are all the ones, for which bottom-up
     * analysis is produced.
     *
     * @param tasks to compute this special task from and to add this special task to.
     */
    private void addBottomUpMulti(List<Task> tasks) {
        Set<String> bottomUpTasksSfrNodeIds = new TreeSet<String>();
        for (Task task : tasks) {
            if (Task.TaskType.BU.equals(task.getTaskType())) {
                bottomUpTasksSfrNodeIds.addAll(task.getNodeIds());
            }
        }
        if (bottomUpTasksSfrNodeIds.size() > 1) {
            tasks.add(new Task(bottomUpTasksSfrNodeIds, Task.TaskType.BU));
        }
    }

    /**
     * Produce a task from a single line of the input file.
     *
     * @param line to produce a task from.
     * @return task.
     * @throws InvalidTaskException in case the task can't be produced, because the line has a wrong format.
     */
    private Task produceTask(String line) throws InvalidTaskException {
        String[] fields = line.split(";");
        if (fields.length != 2) {
            throw new InvalidTaskException("Input file contains a line, which does not have 2 fields separated by a semicolon. Line: " + line);
        }

        String nodeId = fields[0];
        if (StringUtils.isEmpty(nodeId)) {
            throw new InvalidTaskException("Node ID must not be empty. Line: " + line);
        }

        Task.TaskType taskType;
        try {
            taskType = Task.TaskType.valueOf(fields[1]);
        } catch (Exception e) {
            throw new InvalidTaskException(fields[1] + " is not a valid analysis type. Please use 'BU' for bottom-up and 'TD' for top-down. Line: " + line);
        }

        return new Task(Collections.singleton(nodeId), taskType);
    }

    public CommonsMultipartFile getFileContents() {
        return fileContents;
    }

    public void setFileContents(CommonsMultipartFile fileContents) throws IOException {
        this.fileContents = fileContents;
        this.fileContentsAsBytes = fileContents.getBytes();
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    /**
     * Get the locale used for creating this job description.
     *
     * @return
     */
    public Locale getLocale() {
        return locale;
    }
}
