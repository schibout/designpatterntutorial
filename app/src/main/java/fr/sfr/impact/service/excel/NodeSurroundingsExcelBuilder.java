package fr.sfr.impact.service.excel;

import fr.sfr.impact.dto.NodeSurroundingsDto;

/**
 * Interface for classes that are able to build an Excel workbook from a {@link fr.sfr.impact.dto.NodeSurroundingsDto}.
 *
 * @author Michal Bachman
 */
public interface NodeSurroundingsExcelBuilder extends ExcelBuilder<NodeSurroundingsDto> {
}
