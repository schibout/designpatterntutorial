package fr.sfr.impact.service.topdown;

import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.dto.NodeDtoSubset;
import fr.sfr.impact.dto.TopDownResultDto;

/**
 * Service for serving on-demand paged and partitioned top-down analysis result data.
 *
 * @author Michal Bachman
 */
public interface TopDownResultService {

    /**
     * Get a list of impacted nodes for a top-down analysis of a given node, respecting the paging criteria.
     *
     * @param nodeId         Neo4J node ID of the node to perform bottom-up analysis for.
     * @param pagingCriteria paging info.
     * @return subset of impacted nodes.
     */
    NodeDtoSubset getImpactedNodes(long nodeId, PagingCriteria pagingCriteria);

    /**
     * Get a list of impacted services for a top-down analysis of a given node, respecting the paging criteria.
     *
     * @param nodeId         Neo4J node ID of the node to perform bottom-up analysis for.
     * @param pagingCriteria paging info.
     * @return subset of impacted services.
     */
    NodeDtoSubset getImpactedServices(long nodeId, PagingCriteria pagingCriteria);

    /**
     * Get a list of impacted equipment for a bottom-up analysis of a given node, respecting the paging criteria.
     *
     * @param nodeId         Neo4J node ID of the node to perform bottom-up analysis for.
     * @param pagingCriteria paging info.
     * @return subset of impacted equipment.
     */
    NodeDtoSubset getImpactedEquipment(long nodeId, PagingCriteria pagingCriteria);

    /**
     * Get a top-down analysis of a given node wrapped in a DTO. Intended for generating Excel reports.
     *
     * @param nodeId Neo4J node ID of the node to perform top-down analysis for.
     * @return top-down analysis result as a DTO.
     */
    TopDownResultDto getFullResult(long nodeId);
}
