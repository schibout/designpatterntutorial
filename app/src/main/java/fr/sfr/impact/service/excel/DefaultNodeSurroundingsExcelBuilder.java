package fr.sfr.impact.service.excel;

import fr.sfr.impact.domain.RelationshipTypeAndDirection;
import fr.sfr.impact.dto.NodeDto;
import fr.sfr.impact.dto.NodeSurroundingsDto;
import fr.sfr.impact.util.DateTimeUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.util.Locale;

import static fr.sfr.impact.domain.Constants.*;

/**
 * Default production implementation of {@link NodeSurroundingsExcelBuilder}.
 *
 * @author Michal Bachman
 */
@Component
public class DefaultNodeSurroundingsExcelBuilder extends AbstractExcelBuilder<NodeSurroundingsDto> implements NodeSurroundingsExcelBuilder {

    /**
     * {@inheritDoc}
     */
    @Override
    public String enrich(XSSFWorkbook workbook, NodeSurroundingsDto nodeSurroundingsDto, Locale locale) {
        for (RelationshipTypeAndDirection relationshipTypeAndDirection : nodeSurroundingsDto.getSurroundings().keySet()) {
            XSSFSheet sheet = workbook.createSheet(buildSheetName(locale, relationshipTypeAndDirection));

            createStandardHeaderRow(sheet, locale);

            int rowNo = 1;
            for (NodeDto dto : nodeSurroundingsDto.getSurroundings().get(relationshipTypeAndDirection)) {
                createRow(sheet, rowNo++, dto.getProperty(ID), dto.getProperty(NAME), dto.getProperty(TYPO), dto.getProperty(TYPE), dto.getProperty(SOURCE), dto.getProperty(TECHNO), dto.getProperty(EQ_TYPE), dto.getProperty(CAPACITY));
            }
        }
        return buildFileName(nodeSurroundingsDto);
    }

    private String buildSheetName(Locale locale, RelationshipTypeAndDirection relationshipTypeAndDirection) {
        return resolveMessage(relationshipTypeAndDirection.getDirection().name(), locale) + "-" + resolveMessage(relationshipTypeAndDirection.getRelationshipType().name(), locale);
    }

    private String buildFileName(NodeSurroundingsDto nodeSurroundingsDto) {
        if (nodeSurroundingsDto.isSingleRelationshipAndDirection()) {
            RelationshipTypeAndDirection relationshipTypeAndDirection = nodeSurroundingsDto.getSurroundings().keySet().iterator().next();
            return "node-" + nodeSurroundingsDto.getNodeName() + "-" + relationshipTypeAndDirection.getRelationshipType().name() + "-" + relationshipTypeAndDirection.getDirection().name() + "-" + DateTimeUtils.formattedDateTime() + ".xls";
        }

        return "node-" + nodeSurroundingsDto.getNodeName() + "-" + DateTimeUtils.formattedDateTime() + ".xls";

    }
}
