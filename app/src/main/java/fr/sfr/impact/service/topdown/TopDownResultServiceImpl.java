package fr.sfr.impact.service.topdown;

import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.domain.topdown.TopDownAnalysis;
import fr.sfr.impact.dto.NodeDtoSubset;
import fr.sfr.impact.dto.TopDownResultDto;
import fr.sfr.impact.service.CachingService;
import fr.sfr.impact.service.node.NodeDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;

@Service
public class TopDownResultServiceImpl extends CachingService<TopDownAnalysisService, TopDownAnalysis> implements TopDownResultService {

    @Autowired
    private TopDownAnalysisService topDownAnalysis;

    @Autowired
    private NodeDetailService nodeDetailService;


    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDtoSubset getImpactedNodes(long nodeId, PagingCriteria pagingCriteria) {
        return nodeDtoSubset(pagingCriteria, getResult(singleton(nodeId)).getNodesOnPath());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDtoSubset getImpactedServices(long nodeId, PagingCriteria pagingCriteria) {
        return nodeDtoSubset(pagingCriteria, getResult(singleton(nodeId)).getServices());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDtoSubset getImpactedEquipment(long nodeId, PagingCriteria pagingCriteria) {
        return nodeDtoSubset(pagingCriteria, getResult(singleton(nodeId)).getEquipment());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TopDownResultDto getFullResult(long nodeId) {
        TopDownResultDto result = new TopDownResultDto();
        result.setNodeDetails(singletonList(nodeDetailService.getNodeDetail(nodeId)));
        result.setEquipment(allDtos(getResult(singleton(nodeId)).getEquipment()));
        result.setServices(allDtos(getResult(singleton(nodeId)).getServices()));
        result.setNodesOnPath(allDtos(getResult(singleton(nodeId)).getNodesOnPath()));
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected TopDownAnalysisService getAnalysis() {
        return topDownAnalysis;
    }
}
