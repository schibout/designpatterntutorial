package fr.sfr.impact.service.job;

import fr.sfr.impact.domain.job.*;
import fr.sfr.impact.service.bottomup.BottomUpResultService;
import fr.sfr.impact.service.excel.BottomUpExcelReportBuilder;
import fr.sfr.impact.service.excel.TopDownExcelReportBuilder;
import fr.sfr.impact.service.search.SearchService;
import fr.sfr.impact.service.topdown.TopDownResultService;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.neo4j.graphdb.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Default production implementation of {@link JobService}.
 *
 * @author Michal Bachman
 */
@Service
public class DefaultJobService implements JobService {

    private static final int MAX_NUMBER_OF_JOBS_IN_MEMORY = 100;

    private final Map<String, Job> jobs = new ConcurrentHashMap<String, Job>(new HashMap<String, Job>());

    @Autowired
    private BottomUpResultService bottomUpResultService;
    @Autowired
    private TopDownResultService topDownResultService;
    @Autowired
    private BottomUpExcelReportBuilder bottomUpExcelReportBuilder;
    @Autowired
    private TopDownExcelReportBuilder topDownExcelReportBuilder;
    @Autowired
    @Qualifier("myExecutor")
    private TaskExecutor taskExecutor;
    @Autowired
    private SearchService searchService;

    /**
     * {@inheritDoc}
     */
    @Override
    public String createJob(JobDescription jobDescription) {
        if (jobs.size() > MAX_NUMBER_OF_JOBS_IN_MEMORY) {
            throw new RuntimeException("The maximum number of jobs " + MAX_NUMBER_OF_JOBS_IN_MEMORY + " has been exceeded. Please try again later.");
        }

        final Job job = Job.withDescription(jobDescription);
        String jobId = UUID.randomUUID().toString();
        jobs.put(jobId, job);
        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                runJob(job);
            }
        });
        return jobId;
    }

    private void runJob(Job job) {
        try {
            job.start();
            if (job.getTasks().isEmpty()) {
                return;
            }
            doRunJob(job);
            job.finish();
        } catch (Exception e) {
            job.error(e.getMessage());
        }
    }

    private void doRunJob(Job job) throws InvalidTaskException, IOException {
        for (Task task : job.getTasks()) {
            Set<Long> nodeIds = sfrNodeIdsToNeoNodeIds(task.getNodeIds());

            XSSFWorkbook workbook = new XSSFWorkbook();
            String fileName;
            switch (task.getTaskType()) {
                case BU:
                    fileName = bottomUpExcelReportBuilder.enrich(workbook, bottomUpResultService.getFullResult(nodeIds), job.getLocale());
                    break;
                case TD:
                    if (nodeIds.size() > 1) {
                        throw new UnsupportedOperationException("Multi-analysis not supported for Top-Down yet!");
                    }
                    fileName = topDownExcelReportBuilder.enrich(workbook, topDownResultService.getFullResult(nodeIds.iterator().next()), job.getLocale());
                    break;
                default:
                    throw new RuntimeException("Invalid task type " + task.getTaskType().name() + "! This is a bug.");
            }

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            job.addResult(fileName, outputStream.toByteArray());
        }
    }

    @Scheduled(fixedDelay = 5000)
    void cleanupJobs() {
        for (Map.Entry<String, Job> entry : jobs.entrySet()) {
            if (entry.getValue().shouldBeRemoved()) {
                jobs.remove(entry.getKey());
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JobStatus.Status getJobStatus(String jobId) {
        return findJob(jobId).getStatus();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getJobProgress(String jobId) {
        return findJob(jobId).getPercentFinished();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JobResult getJobResult(String jobId) {
        return findJob(jobId).getJobResult();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getError(String jobId) {
        return findJob(jobId).getErrorMessage();
    }

    Job findJob(String jobId) {
        Job result = jobs.get(jobId);
        if (result == null) {
            throw new RuntimeException("Job with ID " + jobId + " does not exist");
        }
        return result;
    }

    private Set<Long> sfrNodeIdsToNeoNodeIds(Set<String> sfrNodeIds) throws InvalidTaskException {
        Set<Long> result = new TreeSet<Long>();
        for (String sfrId : sfrNodeIds) {
            Node node = searchService.findSingleNode(sfrId);
            if (node == null) {
                throw new InvalidTaskException("Node " + sfrId + " not found.");
            }
            result.add(node.getId());
        }
        return result;
    }
}
