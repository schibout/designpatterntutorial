package fr.sfr.impact.service;

import fr.sfr.impact.domain.Analysis;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Abstract super class for all analysis services.
 *
 * @param <T> type of the analysis the sub-class produces.
 * @author Michal Bachman
 */
public abstract class AbstractAnalysisService<T extends Analysis> implements AnalysisService<T> {

    final protected transient Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * {@inheritDoc}
     */
    @Override
    public T analyse(Long... nodeIds) {
        return analyse(new HashSet<Long>(Arrays.asList(nodeIds)));
    }
}
