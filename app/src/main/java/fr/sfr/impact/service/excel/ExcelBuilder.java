package fr.sfr.impact.service.excel;

import fr.sfr.impact.dto.Dto;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.Locale;

/**
 * Interface for classes that are able to build an Excel workbook from a DTO.
 *
 * @param <T> Dto that the implementation can build a workbook out of.
 * @author Michal Bachman
 */
public interface ExcelBuilder<T extends Dto> {

    /**
     * Enrich an existing Excel workbook with DTO contents.
     *
     * @param workbook to enrich.
     * @param locale   to use for message resolution.
     * @param dto      data to put in the workbook.
     * @return name of the file that this workbook should get when downloaded to the client.
     */
    String enrich(XSSFWorkbook workbook, T dto, Locale locale);
}
