package fr.sfr.impact.service;

import fr.sfr.impact.domain.LightNode;
import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.dto.NodeDto;
import fr.sfr.impact.dto.NodeDtoSubset;
import org.neo4j.graphdb.GraphDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.List;

/**
 * Abstract super-class for services that return paged node DTO data.
 *
 * @author Michal Bachman
 */
public abstract class PagingService {

    @Autowired
    protected GraphDatabaseService database;

    /**
     * Get a subset of node DTOs based on the paging criteria and the total count of the results.
     *
     * @param pagingCriteria paging info.
     * @param nodes        Neo4J node IDs of the nodes to be converted to DTOs.
     * @return subset of Node DTOs with total count info.
     */
    protected NodeDtoSubset nodeDtoSubset(PagingCriteria pagingCriteria, Collection<? extends LightNode> nodes) {
        NodeDtoSubset result = new NodeDtoSubset();

        int iteratedRecords = 0;
        for (LightNode impactedService : nodes) {
            if (iteratedRecords >= pagingCriteria.getFirst() && iteratedRecords <= pagingCriteria.getLast()) {
                result.addDto(impactedService.produceDto(database));
            }
            iteratedRecords++;
        }
        result.setTotalNumber(iteratedRecords);

        return result;
    }

    protected List<NodeDto> allDtos(Collection<? extends LightNode> nodes) {
        return nodeDtoSubset(PagingCriteria.EVERYTHING, nodes).getDtos();
    }
}
