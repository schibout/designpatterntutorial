package fr.sfr.impact.service;

import org.neo4j.jmx.Primitives;
import org.neo4j.kernel.GraphDatabaseAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Tareq Abedrabbo
 */
@Service
public class DefaultStatService implements StatService {

    private final Primitives primitives;

    @Autowired
    public DefaultStatService(GraphDatabaseAPI graphDatabaseAPI) {
        primitives = graphDatabaseAPI.getSingleManagementBean(Primitives.class);

    }

    @Override
    public long getNumberOfNodes() {
        return primitives.getNumberOfNodeIdsInUse();
    }

    @Override
    public long getNumberOfRelationships() {
        return primitives.getNumberOfRelationshipIdsInUse();
    }
}
