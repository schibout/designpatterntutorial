package fr.sfr.impact.service.excel;

import fr.sfr.impact.dto.NodeDto;
import fr.sfr.impact.dto.TopDownResultDto;
import fr.sfr.impact.util.DateTimeUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Locale;

import static fr.sfr.impact.domain.Constants.NAME;

/**
 * Default production implementation of {@link TopDownExcelReportBuilder}.
 *
 * @author Michal Bachman
 */
@Component
public class DefaultTopDownExcelReportBuilder extends AbstractExcelReportBuilder<TopDownResultDto> implements TopDownExcelReportBuilder {

    /**
     * {@inheritDoc}
     */
    @Override
    public String enrich(XSSFWorkbook workbook, TopDownResultDto topDownResultDto, Locale locale) {
        addNodeDetails(workbook, topDownResultDto, locale);
        addServices(workbook, topDownResultDto, locale);
        addEquipment(workbook, topDownResultDto, locale);
        addNodesOnPath(workbook, topDownResultDto, locale);
        return buildFileName(topDownResultDto.getNodeDetails());
    }

    private void addEquipment(XSSFWorkbook workbook, TopDownResultDto topDownResultDto, Locale locale) {
        XSSFSheet sheet = workbook.createSheet(resolveMessage("label_equipment", locale));
        createStandardHeaderRow(sheet, locale);
        int row = 1;
        for (NodeDto nodeDto : topDownResultDto.getEquipment()) {
            createStandardRow(nodeDto, sheet, row++);
        }
    }

    private void addServices(XSSFWorkbook workbook, TopDownResultDto topDownResultDto, Locale locale) {
        XSSFSheet sheet = workbook.createSheet(resolveMessage("label_services", locale));
        createStandardHeaderRow(sheet, locale);
        int row = 1;
        for (NodeDto nodeDto : topDownResultDto.getServices()) {
            createStandardRow(nodeDto, sheet, row++);
        }
    }

    //when (if ever) multiple nodes supported, merge this with the same method on DefaultBottomUpExcelReportBuilder
    private String buildFileName(List<NodeDto> nodeDetails) {
        Assert.notEmpty(nodeDetails);
        if (nodeDetails.size() > 1) {
            throw new UnsupportedOperationException("Multi-analysis not supported for Top-Down yet!");
        }

        return "topdown-" + nodeDetails.get(0).getProperty(NAME) + "-" + DateTimeUtils.formattedDateTime() + ".xls";
    }
}
