package fr.sfr.impact.service.topdown;

import fr.sfr.impact.domain.topdown.TopDownAnalysis;
import fr.sfr.impact.service.AnalysisService;

/**
 * Interface for classes that perform a top-down(TD) analysis for nodes.
 *
 * @author Michal Bachman
 */
public interface TopDownAnalysisService extends AnalysisService<TopDownAnalysis> {
}
