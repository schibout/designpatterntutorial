package fr.sfr.impact.service.excel;

import fr.sfr.impact.dto.BottomUpResultDto;

/**
 * Interface for classes that are able to build an Excel report from a {@link fr.sfr.impact.dto.BottomUpResultDto}.
 *
 * @author Michal Bachman
 */
public interface BottomUpExcelReportBuilder extends ExcelReportBuilder<BottomUpResultDto> {
}
