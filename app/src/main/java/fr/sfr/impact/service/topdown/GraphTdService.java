package fr.sfr.impact.service.topdown;

import fr.sfr.impact.domain.topdown.TdResponse;

public interface GraphTdService {


    TdResponse tdGraphFrom(Long id, Long impactedNode);

    TdResponse surroundings(long id);

}
