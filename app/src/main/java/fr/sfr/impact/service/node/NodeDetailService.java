package fr.sfr.impact.service.node;

import fr.sfr.impact.domain.RelationshipTypeAndDirection;
import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.dto.NodeDto;
import fr.sfr.impact.dto.NodeDtoSubset;
import fr.sfr.impact.dto.NodeSurroundingsDto;

import java.util.List;
import java.util.Set;

/**
 * Service for getting details of a node.
 *
 * @author Michal Bachman
 */
public interface NodeDetailService {

    /**
     * Get a node as a DTO
     *
     * @param nodeId Neo4J ID of the node to get.
     * @return node as a DTO.
     */
    NodeDto getNodeDetail(long nodeId);

    /**
     * Get a list of nodes as DTOs.
     *
     * @param nodeIds Neo4J IDs of the nodes to get.
     * @return nodes as a DTOs.
     */
    List<NodeDto> getNodeDetails(Set<Long> nodeIds);

    /**
     * Get all the relationship types and directions of the relationships that this node has with neighbours.
     * The relationships are ordered as they are in {@link fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes} and the directions are
     * {@link org.neo4j.graphdb.Direction#OUTGOING} and then {@link org.neo4j.graphdb.Direction#INCOMING}.
     *
     * @param nodeId Neo4J ID of the node for which to get relationships types with directions.
     * @return as stated.
     */
    List<RelationshipTypeAndDirection> getRelationshipTypesAndDirections(long nodeId);

    /**
     * Get surrounding nodes of a given node, specified by relationship type and direction, respecting the paging criteria.
     *
     * @param nodeId                       Neo4J ID of the node for which to get surroundings.
     * @param relationshipTypeAndDirection relationship type and direction to be used when looking for the neighbours.
     * @param pagingCriteria               paging info.
     * @return surrounding nodes as DTOs respecting the paging plus the total count.
     */
    NodeDtoSubset getNodeSurroundings(long nodeId, RelationshipTypeAndDirection relationshipTypeAndDirection, PagingCriteria pagingCriteria);

    /**
     * Get all surrounding nodes of a given node, specified by relationship type and direction.
     *
     * @param nodeId                       Neo4J ID of the node for which to get surroundings.
     * @param relationshipTypeAndDirection relationship type and direction to be used when looking for the neighbours.
     * @return surrounding nodes as DTOs respecting the paging plus the total count.
     */
    NodeSurroundingsDto getAllNodeSurroundings(long nodeId, RelationshipTypeAndDirection relationshipTypeAndDirection);

    /**
     * Get surrounding nodes of a given node, keyed by relationship type and direction.
     * The relationships are ordered as they are in {@link fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes} and the directions are
     * {@link org.neo4j.graphdb.Direction#OUTGOING} and then {@link org.neo4j.graphdb.Direction#INCOMING}.
     *
     * @param nodeId Neo4J ID of the node for which to get surroundings.
     * @return surrounding nodes as DTOs, keyed by relationship type and direction.
     */
    NodeSurroundingsDto getAllNodeSurroundings(long nodeId);
}
