package fr.sfr.impact.service.excel;

import fr.sfr.impact.dto.AnalysisResultDto;

/**
 * Interface for classes that are able to build an Excel report from a {@link fr.sfr.impact.dto.AnalysisResultDto}.
 *
 * @author Michal Bachman
 */
public interface ExcelReportBuilder<T extends AnalysisResultDto> extends ExcelBuilder<T> {
}
