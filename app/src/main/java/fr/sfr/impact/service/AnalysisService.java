package fr.sfr.impact.service;

import fr.sfr.impact.domain.Analysis;

import java.util.Set;

/**
 * Service able to perform some analysis on the graph.
 *
 * @param <T> type of the analysis result.
 * @author Michal Bachman
 */
public interface AnalysisService<T extends Analysis> {

    /**
     * Perform the analysis.
     *
     * @param nodeIds Neo4J node IDs of the nodes to start with.
     * @return result of the analysis.
     */
    T analyse(Set<Long> nodeIds);

    /**
     * Perform the analysis.
     *
     * @param nodeIds Neo4J node IDs of the nodes to start with.
     * @return result of the analysis.
     */
    T analyse(Long... nodeIds);
}
