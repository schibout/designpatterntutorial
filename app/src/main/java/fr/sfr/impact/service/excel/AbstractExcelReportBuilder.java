package fr.sfr.impact.service.excel;

import fr.sfr.impact.dto.AnalysisResultDto;
import fr.sfr.impact.dto.NodeDto;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.Locale;

public abstract class AbstractExcelReportBuilder<T extends AnalysisResultDto> extends AbstractExcelBuilder<T> implements ExcelReportBuilder<T> {

    protected void addNodeDetails(XSSFWorkbook workbook, AnalysisResultDto topDownResultDto, Locale locale) {
        XSSFSheet sheet = workbook.createSheet(resolveMessage("node_detail", locale));
        createStandardHeaderRow(sheet, locale);
        int i=0;
        for (NodeDto nodeDto : topDownResultDto.getNodeDetails()) {
            createStandardRow(nodeDto, sheet, ++i);
        }
    }

    protected void addNodesOnPath(XSSFWorkbook workbook, AnalysisResultDto topDownResultDto, Locale locale) {
        XSSFSheet sheet;
        int row;
        sheet = workbook.createSheet(resolveMessage("label_nodes_on_path", locale));
        createStandardHeaderRow(sheet, locale);
        row = 1;
        for (NodeDto nodeDto : topDownResultDto.getNodesOnPath()) {
            createStandardRow(nodeDto, sheet, row++);
        }
    }

    protected String translate(String string, Locale locale) {
        if (StringUtils.isNotBlank(string)) {
            return resolveMessage(string, locale);
        }
        return string;
    }
}
