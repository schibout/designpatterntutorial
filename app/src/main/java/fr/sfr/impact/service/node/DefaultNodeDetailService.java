package fr.sfr.impact.service.node;

import fr.sfr.impact.domain.Constants;
import fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes;
import fr.sfr.impact.domain.LightNode;
import fr.sfr.impact.domain.RelationshipTypeAndDirection;
import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.service.PagingService;
import fr.sfr.impact.dto.NodeDto;
import fr.sfr.impact.dto.NodeDtoSubset;
import fr.sfr.impact.dto.NodeSurroundingsDto;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Default production implementation of {@link NodeDetailService}.
 *
 * @author Michal Bachman
 */
@Service
public class DefaultNodeDetailService extends PagingService implements NodeDetailService {

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDto getNodeDetail(long nodeId) {
        return new NodeDto(database.getNodeById(nodeId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<NodeDto> getNodeDetails(Set<Long> nodeIds) {
        List<NodeDto> result = new LinkedList<NodeDto>();
        for (Long nodeId : nodeIds) {
            result.add(getNodeDetail(nodeId));
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<RelationshipTypeAndDirection> getRelationshipTypesAndDirections(long nodeId) {
        List<RelationshipTypeAndDirection> result = new LinkedList<RelationshipTypeAndDirection>();

        for (ImpactAnalysisRelationshipTypes type : ImpactAnalysisRelationshipTypes.values()) {
            for (Direction direction : new Direction[]{Direction.OUTGOING, Direction.INCOMING}) {
                if (database.getNodeById(nodeId).getRelationships(type, direction).iterator().hasNext()) {
                    result.add(new RelationshipTypeAndDirection(type, direction));
                }
            }
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDtoSubset getNodeSurroundings(long nodeId, RelationshipTypeAndDirection relationshipTypeAndDirection, PagingCriteria pagingCriteria) {
        List<LightNode> nodeIds = new LinkedList<LightNode>();
        Node node = database.getNodeById(nodeId);
        for (Relationship relationship : node.getRelationships(relationshipTypeAndDirection.getRelationshipType(), relationshipTypeAndDirection.getDirection())) {
            nodeIds.add(new LightNode(relationship.getOtherNode(node).getId()));
        }

        return nodeDtoSubset(pagingCriteria, nodeIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeSurroundingsDto getAllNodeSurroundings(long nodeId, RelationshipTypeAndDirection relationshipTypeAndDirection) {
        return new NodeSurroundingsDto(getNodeDetail(nodeId).getProperty(Constants.NAME), Collections.singletonMap(relationshipTypeAndDirection, getNodeSurroundings(nodeId, relationshipTypeAndDirection, PagingCriteria.EVERYTHING).getDtos()), true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeSurroundingsDto getAllNodeSurroundings(long nodeId) {
        Map<RelationshipTypeAndDirection, List<NodeDto>> map = new LinkedHashMap<RelationshipTypeAndDirection, List<NodeDto>>();

        for (RelationshipTypeAndDirection relationshipTypeAndDirection : getRelationshipTypesAndDirections(nodeId)) {
            map.put(relationshipTypeAndDirection, getNodeSurroundings(nodeId, relationshipTypeAndDirection, PagingCriteria.EVERYTHING).getDtos());
        }

        return new NodeSurroundingsDto(getNodeDetail(nodeId).getProperty(Constants.NAME), map, false);
    }

    //for testing
    void setDatabase(GraphDatabaseService database) {
        this.database = database;
    }
}
