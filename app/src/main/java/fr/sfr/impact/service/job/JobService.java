package fr.sfr.impact.service.job;

import fr.sfr.impact.domain.job.JobDescription;
import fr.sfr.impact.domain.job.JobResult;
import fr.sfr.impact.domain.job.JobStatus;

/**
 * Service for submitting {@link fr.sfr.impact.domain.job.Job}s, retrieving their status, progress, result and error.
 *
 * @author Michal Bachman
 */
//todo consider throwing more specific exceptions
public interface JobService {

    /**
     * Create a new job and submit for execution.
     *
     * @param jobDescription of the job to create.
     * @return ID of the new job.
     * @throws RuntimeException in case the maximum number of jobs in memory has been exceeded.
     */
    String createJob(JobDescription jobDescription);

    /**
     * Get status of a job.
     *
     * @param jobId to get status for.
     * @return the job's status.
     * @throws RuntimeException in case a job with the given ID does not exist.
     */
    JobStatus.Status getJobStatus(String jobId);

    /**
     * Get a job's progress.
     *
     * @param jobId to get progress for.
     * @return the job's progress in %, will always be 0-100 inclusive.
     * @throws RuntimeException in case a job with the given ID does not exist.
     */
    int getJobProgress(String jobId);

    /**
     * Get the result of a job.
     *
     * @param jobId to get a result for.
     * @return the result.
     * @throws fr.sfr.impact.domain.job.InvalidJobStateException
     *                          in case the job isn't finished.
     * @throws RuntimeException in case a job with the given ID does not exist.
     */
    JobResult getJobResult(String jobId);

    /**
     * Get the reason this job failed.
     *
     * @param jobId to get failure reason for.
     * @return the human-readable reason for error.
     * @throws RuntimeException in case a job with the given ID does not exist.
     */
    String getError(String jobId);
}
