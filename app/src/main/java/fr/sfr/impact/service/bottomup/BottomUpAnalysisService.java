package fr.sfr.impact.service.bottomup;

import fr.sfr.impact.domain.bottomup.BottomUpAnalysis;
import fr.sfr.impact.service.AnalysisService;

/**
 * Interface for classes that perform a bottom-up (BU) analysis for nodes.
 *
 * @author Michal Bachman
 */
public interface BottomUpAnalysisService extends AnalysisService<BottomUpAnalysis> {}
