package fr.sfr.impact.service.bottomup;

import fr.sfr.impact.domain.Constants;
import fr.sfr.impact.domain.bottomup.*;
import fr.sfr.impact.service.AbstractAnalysisService;
import org.apache.commons.lang.StringUtils;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.kernel.Traversal;
import org.neo4j.kernel.Uniqueness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StopWatch;

import java.util.*;

import static fr.sfr.impact.domain.Constants.*;
import static fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes.*;
import static fr.sfr.impact.domain.bottomup.NodeStatus.KO;
import static fr.sfr.impact.domain.bottomup.NodeStatus.NOT_SECURE;

/**
 * Default production implementation of the {@link BottomUpAnalysisService} algorithm.
 *
 * @author Michal Bachman
 */
@Component
public class BottomUpAnalysisServiceImpl extends AbstractAnalysisService<BottomUpAnalysis> implements BottomUpAnalysisService {

    @Autowired
    private GraphDatabaseService graphDb;

    /**
     * {@inheritDoc}
     */
    @Override
    public BottomUpAnalysis analyse(Set<Long> nodeIds) {

        StopWatch stopWatch = new StopWatch("Bottom-up starting from " + Arrays.toString(nodeIds.toArray()));
        stopWatch.start("bottom up traversal");
        BottomUpAnalysis nodeAnalysis = new BottomUpAnalysis(nodeIds);

        // a map to keep the status of each abstract node for every service.
        Map<Long, Map<Long, NodeStatus>> abstractNodeStatusMap = new LinkedHashMap<Long, Map<Long, NodeStatus>>();
        for (long startNodeId : nodeIds) {
            Node startNode = graphDb.getNodeById(startNodeId);
            for (Path path : bottomUpRelationships().traverse(startNode)) {
                nodeAnalysis.addImpactedNode(path.endNode().getId());
                if (isClient(path.endNode())) {
                    List<Node> nodesOnPath = nodesFromPath(path);
                    Node service = serviceNodeFromPath(nodesOnPath);
                    Node client = clientNodeFromPath(nodesOnPath);
                    Node circuit = circuitNodeFromPath(nodesOnPath);

                    abstractNodeStatusMap.put(service.getId(), abstractNodeStatus(new ArrayList<Node>(nodesOnPath)));

                    LightImpactedService impactedService = nodeAnalysis.addImpactedClientAndService(
                            (String) startNode.getProperty(ID),
                            client,
                            service,
                            circuit,
                            new LinkedHashSet<Node>(nodesOnPath));

                    // update service status
                    long lowestLevel = findLowestLevel(nodeIds);
                    boolean foundOnePath = false;
                    Map<Long, NodeStatus> serviceAbstractNodeStatusMap = abstractNodeStatusMap.get(impactedService.getNodeId());
                    TraversalDescription description = serviceStatusTraversal(impactedService, nodeIds, serviceAbstractNodeStatusMap);
                    Traverser paths = description.traverse(graphDb.getNodeById(impactedService.getNodeId()));
                    for (Path path2 : paths) {
                        Node node = path2.endNode();
                        Set<Long> koBackupNodesId = impactedService.getKOBackupNodesId();

                        long nodeId = node.getId();
                        boolean isNodeImpacted = nodeIds.contains(nodeId);
                        boolean isKoBackupNodes = koBackupNodesId.contains(nodeId);
                        boolean isOnAtLeastLowestLevel = getLevel(node, Long.MAX_VALUE) <= lowestLevel;
                        boolean containsAtLeastOneAbstractNode = containsAtLeastOneAbstractNode(path2, impactedService);
                        if (!(isNodeImpacted || isKoBackupNodes)
                                && isOnAtLeastLowestLevel
                                && containsAtLeastOneAbstractNode) {
                            foundOnePath = true;
                            break;
                        }
                    }

                    impactedService.setStatus(foundOnePath ? ServiceStatus.NOT_SECURE : ServiceStatus.KO);

                    // update abstract nodes status
                    List<Long> nodeIdsOnPath = nodeIdsOnPath(nodesOnPath);
                    Map<Long, Set<LightBackupNode>> backupNodesMap = impactedService.getBackupNodesMap();

                    boolean firstNotSecure = false;
                    for (Map.Entry<Long, Set<LightBackupNode>> entry : backupNodesMap.entrySet()) {
                        // abstract node id
                        Long abstractNodeId = entry.getKey();

                        //skip abstract node including first non secure
                        NodeStatus abstractNodeStatus = serviceAbstractNodeStatusMap.get(abstractNodeId);
                        if(abstractNodeStatus == NodeStatus.KO && !firstNotSecure) {
                            continue;
                        }
                        if(abstractNodeStatus == NodeStatus.NOT_SECURE && !firstNotSecure) {
                            firstNotSecure = true;
                            continue;
                        }

                        // associated backup nodes
                        Set<LightBackupNode> backupNodes = entry.getValue();

                        if (abstractNodeStatus == NodeStatus.NOT_SECURE) {
                            for(LightBackupNode backupNode : backupNodes) {
                                if(nodeIdsOnPath.contains(backupNode.getNodeId())) {
                                    backupNode.setStatus(NodeStatus.NOT_SECURE);
                                }
                            }
                        }
                    }
                }
            }
        }
        stopWatch.stop();

        logger.debug(stopWatch.prettyPrint());

        return nodeAnalysis;
    }

    private Map<Long, NodeStatus> abstractNodeStatus(List<Node> nodes) {
        Map<Long, NodeStatus> result = new LinkedHashMap<Long, NodeStatus>();

        // sort nodes by level
        Collections.sort(nodes, new Comparator<Node>() {
            @Override
            public int compare(Node n1, Node n2) {
                int level1 = n1.hasProperty(LEVEL) ? Integer.parseInt((String) n1.getProperty(LEVEL)) : 0;
                int level2 = n2.hasProperty(LEVEL) ? Integer.parseInt((String) n2.getProperty(LEVEL)) : 0;
                return level1 - level2;
            }
        });

        // for every abstract node, update its status. Once a status node is NOT_SECURE, we can assume all the following
        // ones will be
        NodeStatus status = KO;
        for (Node node : nodes) {
            if(isAbstractNode(node)) {
                result.put(node.getId(), status);
                for (Relationship r : node.getRelationships(Direction.OUTGOING)) {
                    if (r.hasProperty(Constants.PARTAGE)) {
                        Node backupNode = r.getOtherNode(node);

                        if (!nodes.contains(backupNode)) {
                            status = NOT_SECURE;
                            result.put(node.getId(), status);
                        }
                    }
                }
            }
        }
        return result;
    }

    private boolean isAbstractNode(Node node) {
        return ABSTRACT.equals(node.getProperty(TYPO, ""));
    }


    private boolean containsAtLeastOneAbstractNode(Path path, LightImpactedService service) {
        Set<Long> abstractNodeIds = service.getAbstractNodeIds();

        for (Node node : path.nodes()) {
            if (abstractNodeIds.contains(node.getId())) {
                return true;
            }
        }
        return false;
    }

    private TraversalDescription serviceStatusTraversal(final LightImpactedService service,
                                                        Set<Long> nodeIds,
                                                        final Map<Long, NodeStatus> abstractNodeStatusMap) {

        final Set<Long> endNodeIds = new LinkedHashSet<Long>(nodeIds);
        final Map<Long, LightBackupNode> backupNodesMap = new LinkedHashMap<Long, LightBackupNode>();
        for (LightBackupNode backupNode : service.getBackupNodes()) {
            backupNodesMap.put(backupNode.getNodeId(), backupNode);
        }

        Evaluator endEvaluator = new Evaluator() {
            @Override
            public Evaluation evaluate(Path path) {
                Node endNode = path.endNode();
                if (endNodeIds.contains(endNode.getId())) {
                    return Evaluation.INCLUDE_AND_PRUNE;
                }
                return Evaluation.INCLUDE_AND_CONTINUE;
            }
        };

        Evaluator continueEvaluator = new Evaluator() {
            @Override
            public Evaluation evaluate(Path path) {
                Node endNode = path.endNode();
                NodeStatus nodeStatus = abstractNodeStatusMap.get(endNode.getId());
                if (nodeStatus == null) {
                    return Evaluation.INCLUDE_AND_CONTINUE;
                }
                if (nodeStatus == NodeStatus.OK || nodeStatus == NodeStatus.NOT_SECURE) {
                    return Evaluation.INCLUDE_AND_CONTINUE;
                } else {
                    return Evaluation.INCLUDE_AND_PRUNE;
                }
            }
        };

        TraversalDescription description = Traversal.description()
                .depthFirst()
                .uniqueness(Uniqueness.NODE_PATH)
                .relationships(EST_INCLUS_DANS, Direction.OUTGOING)
                .relationships(EST_COMPOSE_DE, Direction.OUTGOING)
                .evaluator(endEvaluator)
                .evaluator(continueEvaluator);
        return description;
    }

    private List<Node> nodesFromPath(Path path) {
        List<Node> result = new ArrayList<Node>();
        for (Node node : path.nodes()) {
            result.add(node);
        }
        return result;
    }

    private List<Long> nodeIdsOnPath(List<Node> nodesOnPath) {
        List<Long> res = new ArrayList<Long>();
        for(Node node : nodesOnPath) {
            res.add(node.getId());
        }
        return res;
    }

    private List<Long> backupNodeIds(long abstractNodeId) {
        List<Long> nodeIds = new ArrayList<Long>();
        Node abstractNode = graphDb.getNodeById(abstractNodeId);
        for (Relationship r : abstractNode.getRelationships(Direction.OUTGOING)) {
            if (r.hasProperty(Constants.PARTAGE)) {
                Node backupNode = r.getOtherNode(abstractNode);
                nodeIds.add(backupNode.getId());
            }
        }
        return nodeIds;
    }

    private Node clientNodeFromPath(List<Node> nodesOnPath) {
        Node lastNode = nodesOnPath.get(nodesOnPath.size() - 1);
        Assert.isTrue(isClient(lastNode), "Last node on path is not a client! This is a bug");
        return lastNode;
    }

    private Node serviceNodeFromPath(List<Node> nodesOnPath) {
        Assert.isTrue(nodesOnPath.size() >= 2, "Path ending with client must have at least 2 nodes! This is a bug");
        return nodesOnPath.get(nodesOnPath.size() - 2);
    }

    private Node circuitNodeFromPath(List<Node> nodesOnPath) {
        if (!isService(serviceNodeFromPath(nodesOnPath))) {
            return null;
        }
        if (nodesOnPath.size() < 3) {
            return null;
        }

        Node potentialCircuitOrConduit = nodesOnPath.get(nodesOnPath.size() - 3);
        if (!isCircuitOrConduit(potentialCircuitOrConduit)) {
            return null;
        }

        return potentialCircuitOrConduit;
    }

    /**
     * Traversal description for bottom up analysis.
     *
     * @return traversal description.
     */
    private static TraversalDescription bottomUpRelationships() {
        return Traversal.description()
                .depthFirst()
                .uniqueness(Uniqueness.NODE_PATH)
                .relationships(EST_INCLUS_DANS, Direction.INCOMING)
                .relationships(EST_COMPOSE_DE, Direction.INCOMING)
                .relationships(EST_AFFECTE_A, Direction.OUTGOING);
    }

    private boolean isClient(Node node) {
        return isType(node, CLIENT);
    }

    private boolean isService(Node node) {
        return isType(node, SERVICE);
    }

    private boolean isCircuitOrConduit(Node node) {
        return isType(node, CIRCUIT) || isType(node, CONDUIT);
    }

    private boolean isType(Node node, String type) {
        return type.equalsIgnoreCase(node.getProperty(TYPE, StringUtils.EMPTY).toString());
    }

    /**
     * Finds the lowest level of a collection of nodes. This is used to find the lowest node of the impacted
     * nodes in a bottom up analysis.
     * @param nodeIds the node ids.
     * @return the lowest level according to the LEVEL property on each node.
     */
    private long findLowestLevel(Set<Long> nodeIds) {
        long min = Long.MAX_VALUE;
        for (Long nodeId : nodeIds) {
            Node node = graphDb.getNodeById(nodeId);
            long level = getLevel(node, Long.MAX_VALUE);
            min = min < level ? min : level;
        }
        return min;
    }

    /**
     * returns a nodes level or defaultValue if LEVEL is not defined.
     * @param node
     * @return
     */
    private long getLevel(Node node, long defaultValue) {
        Object levelProp = node.getProperty("LEVEL", null);
        if (levelProp != null && levelProp instanceof String) {
            return Long.parseLong((String) levelProp);
        }
        return defaultValue;
    }

    private void debugPath(Path path) {
        System.out.println("============");
        for (Node node : path.nodes()) {
            System.out.println(node.getId());
        }
        System.out.println("============");
    }

}
