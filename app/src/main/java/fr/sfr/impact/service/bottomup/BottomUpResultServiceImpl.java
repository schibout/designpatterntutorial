package fr.sfr.impact.service.bottomup;

import fr.sfr.impact.domain.bottomup.BottomUpAnalysis;
import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.dto.BottomUpResultDto;
import fr.sfr.impact.dto.NodeDtoSubset;
import fr.sfr.impact.service.CachingService;
import fr.sfr.impact.service.node.NodeDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Default production implementation of {@link BottomUpResultService}.
 *
 * @author Michal Bachman
 */
@Service
public class BottomUpResultServiceImpl extends CachingService<BottomUpAnalysisService, BottomUpAnalysis> implements BottomUpResultService {

    @Autowired
    private NodeDetailService nodeDetailService;

    @Autowired
    private BottomUpAnalysisService bottomUpAnalysis;

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDtoSubset getImpactedClients(Set<Long> nodeIds, PagingCriteria pagingCriteria) {
        return nodeDtoSubset(pagingCriteria, getResult(nodeIds).getImpactedClients());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDtoSubset getImpactedServices(Set<Long> nodeIds, PagingCriteria pagingCriteria) {
        return nodeDtoSubset(pagingCriteria, getResult(nodeIds).getImpactedServices());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDtoSubset getNodesOnPath(Set<Long> nodeIds, PagingCriteria pagingCriteria) {
        return nodeDtoSubset(pagingCriteria, getResult(nodeIds).getNodesOnPath());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDtoSubset getImpactedServicesForClient(Set<Long> nodeIds, long clientId, PagingCriteria pagingCriteria) {
        return nodeDtoSubset(pagingCriteria, getResult(nodeIds).getImpactedClient(clientId).getImpactedServicesWithCircuitAndBackups());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BottomUpResultDto getFullResult(Set<Long> nodeIds) {
        BottomUpResultDto result = new BottomUpResultDto();
        result.setNodeDetails(nodeDetailService.getNodeDetails(nodeIds));
        result.setClients(allDtos(getResult(nodeIds).getImpactedClients()));
        result.setServices(allDtos(getResult(nodeIds).getImpactedServices()));
        result.setNodesOnPath(allDtos(getResult(nodeIds).getNodesOnPath()));
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BottomUpAnalysisService getAnalysis() {
        return bottomUpAnalysis;
    }
}
