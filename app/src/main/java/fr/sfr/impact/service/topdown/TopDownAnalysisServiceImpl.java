package fr.sfr.impact.service.topdown;

import fr.sfr.impact.domain.topdown.TopDownAnalysis;
import fr.sfr.impact.service.AbstractAnalysisService;
import org.apache.commons.lang.StringUtils;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.kernel.Traversal;
import org.neo4j.kernel.Uniqueness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

import static fr.sfr.impact.domain.Constants.*;
import static fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes.*;

/**
 * Default production implementation of the {@link TopDownAnalysisService} algorithm.
 *
 * @author Michal Bachman
 */
@Component
public class TopDownAnalysisServiceImpl extends AbstractAnalysisService<TopDownAnalysis> implements TopDownAnalysisService {

    @Autowired
    private GraphDatabaseService graphDb;

    /**
     * {@inheritDoc}
     */
    @Override
    public TopDownAnalysis analyse(Set<Long> nodeIds) {
        if (nodeIds.size() != 1) {
            throw new UnsupportedOperationException("Top-down not yet supporting multi-analysis!");
        }
        return analyse(nodeIds.iterator().next());
    }

    /**
     * {@inheritDoc}
     */
    private TopDownAnalysis analyse(long startNodeId) {
        final TopDownAnalysis nodeAnalysis = new TopDownAnalysis(startNodeId);
        nodeAnalysis.addImpactedNode(startNodeId);

        Node startNode = graphDb.getNodeById(startNodeId);

        for (Path path : topDownRelationships().traverse(startNode)) {
            Node endNode = path.endNode();
            nodeAnalysis.addImpactedNode(endNode.getId());

            if (isService(endNode)) {
                nodeAnalysis.addImpactedService(endNode.getId());
            }

            if (isEquipment(endNode)) {
                nodeAnalysis.addImpactedEquipment(endNode.getId());
            }
        }

        return nodeAnalysis;
    }

    /**
     * Traversal description for top down analysis.
     *
     * @return traversal description.
     */
    private static TraversalDescription topDownRelationships() {
        return Traversal.description()
                .breadthFirst()
                .uniqueness(Uniqueness.RELATIONSHIP_GLOBAL)
                .relationships(EST_INCLUS_DANS, Direction.OUTGOING)
                .relationships(EST_COMPOSE_DE, Direction.OUTGOING)
                .relationships(EST_AFFECTE_A, Direction.INCOMING);
    }

    private boolean isService(Node node) {
        return SERVICE.equalsIgnoreCase(node.getProperty(TYPE, StringUtils.EMPTY).toString());
    }

    private boolean isEquipment(Node node) {
        return !isService(node)
                && !SITE.equalsIgnoreCase(node.getProperty(TYPE, StringUtils.EMPTY).toString())
                && !CLIENT.equalsIgnoreCase(node.getProperty(TYPE, StringUtils.EMPTY).toString());
    }

    void setGraphDb(GraphDatabaseService graphDb) {
        this.graphDb = graphDb;
    }
}
