package fr.sfr.impact.service.topdown;

import fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes;
import fr.sfr.impact.domain.topdown.NodeDesc;
import fr.sfr.impact.domain.topdown.RelationshipDesc;
import fr.sfr.impact.domain.topdown.TdResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.traversal.*;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.kernel.Traversal;
import org.neo4j.kernel.Uniqueness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;

@Service
public class DefaultGraphTdService implements GraphTdService {

    protected final Log log = LogFactory.getLog(getClass());

    private static final String UNKNOWN = "-";

    @Autowired
    private GraphDatabaseService graphDb;

    public TdResponse tdGraphFrom(Long id, final Long impactedNode) {
        final Node node = graphDb.getNodeById(id);

        TraversalDescription traversalDescription = Traversal
                .description()
                .depthFirst()
                .relationships(ImpactAnalysisRelationshipTypes.EST_COMPOSE_DE, Direction.OUTGOING)
                .relationships(ImpactAnalysisRelationshipTypes.EST_INCLUS_DANS, Direction.OUTGOING)
                .relationships(ImpactAnalysisRelationshipTypes.EST_AFFECTE_A, Direction.INCOMING)
                .evaluator(Evaluators.all())
                .uniqueness(Uniqueness.NODE_PATH);


        Traverser traverser = traversalDescription.traverse(node);

        List<NodeDesc> traversedNodes = getTraversedNodes(traverser);

        List<RelationshipDesc> traversedRelationships = getTraversedRelationships(traverser);

        Set<Long> impactPath = new TreeSet<Long>();

        if (impactedNode != null && impactedNode != id) {
            final List<Long> nodeIds = new ArrayList<Long>();

            for (NodeDesc nodeDesc : traversedNodes) {
                nodeIds.add(nodeDesc.getId());
            }

            final int[] found = {0};

            Evaluator boxingEvaluator = new Evaluator() {
                public Evaluation evaluate(Path path) {
                    if (!nodeIds.contains(path.endNode().getId())) {
                        return Evaluation.EXCLUDE_AND_PRUNE;
                    }
                    return Evaluation.INCLUDE_AND_CONTINUE;
                }
            };

            Evaluator pathSelectionEvaluator = new Evaluator() {
                public Evaluation evaluate(Path path) {
                    if (impactedNode.equals(path.endNode().getId())) {
//                        found[0] = 1;
                        return Evaluation.INCLUDE_AND_PRUNE;
                    }
                    return Evaluation.EXCLUDE_AND_CONTINUE;
                }
            };

            // Evaluator that limits traversal to a single solution if needed
//            Evaluator singleSolutionEvaluator = new Evaluator() {
//                public Evaluation evaluate(Path path) {
//                    switch (found[0]) {
//                        case 1:
//                            found[0] = 2;
//                            return Evaluation.INCLUDE_AND_PRUNE;
//                        case 2:
//                            return Evaluation.EXCLUDE_AND_PRUNE;
//                        default:
//                            return Evaluation.EXCLUDE_AND_CONTINUE;
//                    }
//                }
//            };

            TraversalDescription impactTraverser = Traversal.description()
                    .depthFirst()
                    .evaluator(boxingEvaluator)
                    .evaluator(pathSelectionEvaluator)
//                    .evaluator(singleSolutionEvaluator)
                    .relationships(ImpactAnalysisRelationshipTypes.EST_COMPOSE_DE, Direction.OUTGOING)
                    .relationships(ImpactAnalysisRelationshipTypes.EST_INCLUS_DANS, Direction.OUTGOING)
                    .relationships(ImpactAnalysisRelationshipTypes.EST_AFFECTE_A, Direction.INCOMING)
                    .uniqueness(Uniqueness.NODE_PATH);

            Traverser traverse = impactTraverser.traverse(node);


            Iterator<Path> pathIterator = traverse.iterator();
            while (pathIterator.hasNext()) {
                for (Relationship rel : pathIterator.next().relationships()) {
                    impactPath.add(rel.getId());
                }
            }

        }


        TdResponse response = new TdResponse(traversedNodes, traversedRelationships);
        log.info(response);
        response.setImpactPath(impactPath);

        return response;
    }

    public TdResponse surroundings(long id) {
        Node node = graphDb.getNodeById(id);

        TraversalDescription traversalDescription = Traversal
                .description()
                .depthFirst()
                .relationships(ImpactAnalysisRelationshipTypes.EST_COMPOSE_DE, Direction.OUTGOING)
                .relationships(ImpactAnalysisRelationshipTypes.EST_INCLUS_DANS, Direction.OUTGOING)
                .relationships(ImpactAnalysisRelationshipTypes.EST_AFFECTE_A, Direction.INCOMING)
                .evaluator(Evaluators.atDepth(1))
                .uniqueness(Uniqueness.NONE);


        Traverser traverser = traversalDescription.traverse(node);

        List<NodeDesc> traversedNodes = getTraversedNodes(traverser);

        List<RelationshipDesc> traversedRelationships = getTraversedRelationships(traverser);

        return new TdResponse(traversedNodes, traversedRelationships);
    }

    private List<NodeDesc> getTraversedNodes(Traverser traverser) {
        List<NodeDesc> res = new ArrayList<NodeDesc>();
        for (Node node : traverser.nodes()) {
            NodeDesc desc = new NodeDesc(node.getId(), strProp(node, "NAME"), strProp(node, "TYPE"), strProp(node, "LEVEL"));
            boolean exist = false;
            for (int i = 0; i < res.size(); i++) {
                if (desc.getId() == res.get(i).getId()) {
                    exist = true;
                }
            }
            if (!exist) {
                res.add(desc);
            }

        }
        return res;
    }

    private List<RelationshipDesc> getTraversedRelationships(Traverser traverser) {
        List<RelationshipDesc> res = new ArrayList<RelationshipDesc>();
        for (Relationship relationship : traverser.relationships()) {
            RelationshipDesc desc = new RelationshipDesc(relationship.getId(), relationship.getType().name(), relationship.getStartNode().getId(), relationship.getEndNode().getId());
            res.add(desc);
        }
        return res;
    }

    private String strProp(PropertyContainer container, String prop) {
        Object value = container.getProperty(prop, UNKNOWN);
        Assert.isInstanceOf(String.class, value, "expected property type String [" + prop + "]");
        return (String) value;
    }
}
