package fr.sfr.impact.service;

/**
 * @author Tareq Abedrabbo
 */
public interface StatService {
    long getNumberOfNodes();
    long getNumberOfRelationships();
}
