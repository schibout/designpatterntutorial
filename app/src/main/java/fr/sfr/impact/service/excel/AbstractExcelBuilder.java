package fr.sfr.impact.service.excel;

import fr.sfr.impact.dto.Dto;
import fr.sfr.impact.dto.NodeDto;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import java.util.Locale;

import static fr.sfr.impact.domain.Constants.*;

/**
 * Abstract superclass with convenience methods for implementations of {@link ExcelBuilder}.
 *
 * @param <T> type of the DTO that the subclasses can build excel workbooks for.
 * @author Michal Bachman
 */
public abstract class AbstractExcelBuilder<T extends Dto> implements ExcelBuilder<T> {

    @Autowired
    protected MessageSource messageSource;

    protected void createRow(XSSFSheet sheet, int rowNumber, String... values) {
        createRow(sheet, rowNumber, null, values);
    }

    protected void createRow(XSSFSheet sheet, int rowNumber, CellStyle cellStyle, String... values) {
        XSSFRow row = sheet.createRow(rowNumber);
        int columnNo = 0;
        for (String value : values) {
            XSSFCell cell = row.createCell(columnNo++);
            cell.setCellValue(value);
            if (cellStyle != null) {
                cell.setCellStyle(cellStyle);
            }
        }
    }

    protected void createMiniRow(NodeDto nodeDetail, XSSFSheet sheet, int rowNo) {
        createRow(sheet, rowNo, nodeDetail.getProperty(ID), nodeDetail.getProperty(NAME), nodeDetail.getProperty(TYPE));
    }

    protected void createStandardHeaderRow(XSSFSheet sheet, Locale locale) {
        createHeaderRow(sheet, locale, "label_id", "label_name", "label_typo", "label_type", "label_source", "label_techno", "label_eq_type", "label_capacity");
    }

    protected void createStandardRow(NodeDto nodeDetail, XSSFSheet sheet, int rowNo) {
        createRow(sheet, rowNo, nodeDetail.getProperty(ID), nodeDetail.getProperty(NAME), nodeDetail.getProperty(TYPO), nodeDetail.getProperty(TYPE), nodeDetail.getProperty(SOURCE), nodeDetail.getProperty(TECHNO), nodeDetail.getProperty(EQ_TYPE), nodeDetail.getProperty(CAPACITY));
    }

    protected void createHeaderRow(XSSFSheet sheet, Locale locale, String... headerCodes) {
        String[] headers = new String[headerCodes.length];
        for (int i = 0; i < headerCodes.length; i++) {
            headers[i] = resolveMessage(headerCodes[i], locale);
        }
        createRow(sheet, 0, headers);
    }

    protected String resolveMessage(String code, Locale locale) {
        return StringEscapeUtils.unescapeHtml(messageSource.getMessage(code, null, locale));
    }
}
