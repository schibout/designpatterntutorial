package fr.sfr.impact.service.excel;

import fr.sfr.impact.dto.TopDownResultDto;

/**
 * Interface for classes that are able to build an Excel report from a {@link fr.sfr.impact.dto.TopDownResultDto}.
 *
 * @author Michal Bachman
 */
public interface TopDownExcelReportBuilder extends ExcelReportBuilder<TopDownResultDto> {
}
