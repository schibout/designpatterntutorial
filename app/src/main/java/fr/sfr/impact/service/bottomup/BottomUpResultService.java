package fr.sfr.impact.service.bottomup;

import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.dto.BottomUpResultDto;
import fr.sfr.impact.dto.NodeDtoSubset;

import java.util.Set;

/**
 * Service for serving on-demand paged and partitioned bottom-up analysis result data.
 *
 * @author Michal Bachman
 */
public interface BottomUpResultService {

    /**
     * Get a list of impacted clients for a bottom-up analysis of a given node, respecting the paging criteria.
     *
     * @param nodeIds        Neo4J node IDs of the nodes to perform bottom-up analysis for.
     * @param pagingCriteria paging info.
     * @return subset of impacted clients.
     */
    NodeDtoSubset getImpactedClients(Set<Long> nodeIds, PagingCriteria pagingCriteria);

    /**
     * Get a list of impacted services for a bottom-up analysis of a given node, respecting the paging criteria.
     *
     * @param nodeIds        Neo4J node IDs of the nodes to perform bottom-up analysis for.
     * @param pagingCriteria paging info.
     * @return subset of impacted services.
     */
    NodeDtoSubset getImpactedServices(Set<Long> nodeIds, PagingCriteria pagingCriteria);

    /**
     * Get a list of nodes on path for a bottom-up analysis of a given node, respecting the paging criteria.
     *
     * @param nodeIds        Neo4J node IDs of the nodes to perform bottom-up analysis for.
     * @param pagingCriteria paging info.
     * @return subset of nodes on path.
     */
    NodeDtoSubset getNodesOnPath(Set<Long> nodeIds, PagingCriteria pagingCriteria);

    /**
     * Get a list of impacted services of an impacted client for a bottom-up analysis of a given node, respecting the paging criteria.
     *
     * @param nodeIds        Neo4J node IDs of the node to perform bottom-up analysis for.
     * @param clientId       Neo4J node ID of the client node.
     * @param pagingCriteria paging info.
     * @return subset of impacted services.
     */
    NodeDtoSubset getImpactedServicesForClient(Set<Long> nodeIds, long clientId, PagingCriteria pagingCriteria);

    /**
     * Get a bottom-up analysis of a given node wrapped in a DTO. Intended for generating Excel reports.
     *
     * @param nodeIds Neo4J node IDs of the nodes to perform bottom-up analysis for.
     * @return bottom-up analysis result as a DTO.
     */
    BottomUpResultDto getFullResult(Set<Long> nodeIds);
}
