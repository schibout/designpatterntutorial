package fr.sfr.impact.service.excel;

import fr.sfr.impact.domain.bottomup.NodeStatus;
import fr.sfr.impact.dto.BottomUpResultDto;
import fr.sfr.impact.dto.NodeDto;
import fr.sfr.impact.util.DateTimeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Locale;

import static fr.sfr.impact.domain.Constants.*;

/**
 * Default production implementation of {@link BottomUpExcelReportBuilder}.
 *
 * @author Michal Bachman
 */
@Component
public class DefaultBottomUpExcelReportBuilder extends AbstractExcelReportBuilder<BottomUpResultDto> implements BottomUpExcelReportBuilder {

    /**
     * {@inheritDoc}
     */
    @Override
    public String enrich(XSSFWorkbook workbook, BottomUpResultDto bottomUpResultDto, Locale locale) {
        addNodeDetails(workbook, bottomUpResultDto, locale);
        addClients(workbook, bottomUpResultDto, locale);
        addServices(workbook, bottomUpResultDto, locale);
        addNodesOnPath(workbook, bottomUpResultDto, locale);
        return buildFileName(bottomUpResultDto.getNodeDetails());
    }

    private void addServices(XSSFWorkbook workbook, BottomUpResultDto bottomUpResultDto, Locale locale) {
        XSSFSheet sheet = workbook.createSheet(resolveMessage("label_services", locale));
        createServiceHeaderRow(sheet, locale);
        int row = 1;
        CellStyle red = getRedCellStyle(workbook);
        for (NodeDto nodeDto : bottomUpResultDto.getServices()) {
            createServiceRow(nodeDto, sheet, row++, locale, red);
        }
    }

    private void createServiceRow(NodeDto nodeDetail, XSSFSheet sheet, int rowNo, Locale locale, CellStyle red) {
        CellStyle cellStyle = null;
        if (CIRCUIT.equals(nodeDetail.getProperty(TYPE)) && NodeStatus.KO.name().endsWith(nodeDetail.getProperty(STATUS))) {
            cellStyle = red;
        }

        createRow(sheet, rowNo, cellStyle,
                formatImpactingNodes(nodeDetail),
                nodeDetail.getProperty(CLIENT + U_NAME), nodeDetail.getProperty(ID), nodeDetail.getProperty(NAME), nodeDetail.getProperty(TYPE), translate(nodeDetail.getProperty(STATUS), locale),
                nodeDetail.getProperty(AU4 + U_ID + A), nodeDetail.getProperty(CARD + U_NAME + A), nodeDetail.getProperty(PORT + U_NAME + A), nodeDetail.getProperty(EQUIPMENT + U_NAME + A),
                nodeDetail.getProperty(AU4 + U_ID + B), nodeDetail.getProperty(CARD + U_NAME + B), nodeDetail.getProperty(PORT + U_NAME + B), nodeDetail.getProperty(EQUIPMENT + U_NAME + B),
                nodeDetail.getProperty(EQ_TYPE), nodeDetail.getProperty(CAPACITY), translate(nodeDetail.getProperty(PARTAGE), locale));
    }

    private String formatImpactingNodes(NodeDto dto) {
        return StringUtils.join(dto.getImpactingNodes(), ", ");
    }

    private CellStyle getRedCellStyle(XSSFWorkbook workbook) {
        CellStyle cellStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setColor(Font.COLOR_RED);
        cellStyle.setFont(font);
        return cellStyle;
    }

    private void createServiceHeaderRow(XSSFSheet sheet, Locale locale) {
        createHeaderRow(sheet, locale, "label_source", "label_client", "label_id", "label_name", "label_type", "label_status",
                "label_extremity_a", "label_card_a", "label_port_a", "label_equipment_a", "label_extremity_b", "label_card_b", "label_port_b", "label_equipment_b",
                "label_eq_type", "label_capacity", "label_partage");
    }

    private void addClients(XSSFWorkbook workbook, BottomUpResultDto bottomUpResultDto, Locale locale) {
        XSSFSheet sheet = workbook.createSheet(resolveMessage("label_clients", locale));
        createMiniHeaderRow(sheet, locale);
        int row = 1;
        for (NodeDto nodeDto : bottomUpResultDto.getClients()) {
            createMiniRow(nodeDto, sheet, row++);
        }
    }

    //WARNING: Assuming that there will always only be a maximum of one task with multiple nodes for now.
    private String buildFileName(List<NodeDto> nodeDetails) {
        Assert.notEmpty(nodeDetails);
        if (nodeDetails.size() > 1) {
            return "bottomup-multiple-" + DateTimeUtils.formattedDateTime() + ".xls";
        }
        return "bottomup-" + nodeDetails.get(0).getProperty(NAME) + "-" + DateTimeUtils.formattedDateTime() + ".xls";
    }

    private void createMiniHeaderRow(XSSFSheet sheet, Locale locale) {
        createHeaderRow(sheet, locale, "label_id", "label_name", "label_type");
    }
}
