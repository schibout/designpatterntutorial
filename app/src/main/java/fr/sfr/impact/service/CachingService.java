package fr.sfr.impact.service;

import fr.sfr.impact.domain.Analysis;
import org.apache.commons.collections.map.LRUMap;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Service that caches its results. Intended for analysis-invoking services.
 *
 * @param <A> type of analysis this service invokes.
 * @param <R> type of analysis result this service caches and returns.
 * @author Michal Bachman
 */
public abstract class CachingService<A extends AnalysisService<R>, R extends Analysis> extends PagingService {

    private Map<Set<Long>, R> cachedResult = new ConcurrentHashMap<Set<Long>, R>(new LRUMap(100)); //this number should be experimented with

    /**
     * Get an implementation of the {@link AnalysisService} this service invokes.
     *
     * @return analsis implementation.
     */
    protected abstract A getAnalysis();

    /**
     * Get a cached result of an analysis, or delegate to {@link AnalysisService} implementation to compute the result and store it, if not yet in cache.
     *
     * @param nodeIds to get analysis for.
     * @return analysis result.
     */
    protected R getResult(Set<Long> nodeIds) {
        if (cachedResult.containsKey(nodeIds)) {
            return cachedResult.get(nodeIds);
        }
        R nodeAnalysis = getAnalysis().analyse(nodeIds);
        cachedResult.put(nodeIds, nodeAnalysis);
        return nodeAnalysis;
    }

    /**
     * Clear all entries from cache.
     */
    public void clearCache() {
        cachedResult.clear();
    }
}
