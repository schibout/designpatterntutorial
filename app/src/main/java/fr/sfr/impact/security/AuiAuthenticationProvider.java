package fr.sfr.impact.security;

import fr.cegetel.aui.servicesAUI.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Collections;

/**
 * An authentication provider that delegates to SFR's AUI LDAP for authentication.
 * The granted authority is always a single String returned by AUI for this application, as per AUI spec.
 *
 * @author Michal Bachman
 */
public class AuiAuthenticationProvider implements AuthenticationProvider, MessageSourceAware {
    private static final Logger LOG = LoggerFactory.getLogger(AuiAuthenticationProvider.class);
    private MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();

    private final AuthentificationAUI authentificationAUI = new AuthentificationAUI();
    private final String appUsername;
    private final String appPassword;
    private final Resource properties;

    public AuiAuthenticationProvider(String appUsername, String appPassword, Resource properties) {
        this.appUsername = appUsername;
        this.appPassword = appPassword;
        this.properties = properties;
    }

    @PostConstruct
    public void init() {
        try {
            authentificationAUI.init(appUsername, appPassword, properties.getFile().getAbsolutePath());
        } catch (ExceptionAUITimeOut e) {
            LOG.error("There was a problem connecting to AUI. Connection timed out. Message: " + e.getMessage());
            throw new RuntimeException(e);
        } catch (Exception e) {
            LOG.error("There was a problem connecting to AUI. Message: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @PreDestroy
    public void stop() {
        try {
            authentificationAUI.stop();
        } catch (Exception e) {
            LOG.error("Could not disconnect AUI. Message: " + e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Assert.isInstanceOf(UsernamePasswordAuthenticationToken.class, authentication, messages.getMessage("LdapAuthenticationProvider.onlySupports", "Only UsernamePasswordAuthenticationToken is supported"));
        final UsernamePasswordAuthenticationToken userToken = (UsernamePasswordAuthenticationToken) authentication;

        String username = userToken.getName();
        String password = (String) authentication.getCredentials();

        LOG.debug("Processing authentication request for userDetails: " + username);

        if (!StringUtils.hasLength(username)) {
            throw new BadCredentialsException(messages.getMessage("AuiAuthenticationProvider.emptyUsername", "Empty Username"));
        }
        Assert.notNull(password, "Null password was supplied in authentication token");

        Authentication result = authenticate(username, password);
        LOG.debug("Successfully authenticated " + username);

        return result;
    }

    private Authentication authenticate(String username, String password) {
        UserDetails userDetails = new User(username, password, Collections.singleton(new SimpleGrantedAuthority(getAppRoleCode(getPerId(username, password)))));
        return new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());
    }

    private String getPerId(String username, String password) {
        try {
            return authentificationAUI.authentify(username, password);
        } catch (ExceptionAUITimeOut e) {
            LOG.error("There was a problem connecting to AUI. Connection timed out. Message: " + e.getMessage());
            throw new AuthenticationServiceException(e.getMessage());
        } catch (ExceptionAUIUserUnknown e) {
            LOG.info("Username " + username + " not found.");
            throw new UsernameNotFoundException(e.getMessage(), e);
        } catch (ExceptionAUIEmptyPassword e) {
            LOG.info("Password provided by " + username + " is empty.");
            throw new BadCredentialsException(e.getMessage(), e);
        } catch (ExceptionAUIWrongPassword e) {
            LOG.info("Password provided by " + username + " is incorrect.");
            throw new BadCredentialsException(e.getMessage(), e);
        } catch (ExceptionAUIUserSuspended e) {
            LOG.info("User " + username + " is suspended.");
            throw new LockedException(e.getMessage(), e);
        } catch (ExceptionAUIPasswordExpired e) {
            LOG.info("Credentials for " + username + " have expired.");
            throw new CredentialsExpiredException(e.getMessage(), e);
        } catch (ExceptionAUIConnect e) {
            LOG.error("There was a problem connecting to AUI. Message: " + e.getMessage());
            throw new AuthenticationServiceException(e.getMessage());
        }
    }

    private String getAppRoleCode(String perid) {
        try {
            return "ROLE_"+authentificationAUI.getAppCodeRole(perid);
        } catch (ExceptionAUITimeOut e) {
            LOG.error("There was a problem connecting to AUI. Connection timed out. Message: " + e.getMessage());
            throw new AuthenticationServiceException(e.getMessage());
        } catch (ExceptionAUIAppUnknown e) {
            LOG.error("This application is unknown to AUI!");
            throw new AuthenticationServiceException(e.getMessage());
        } catch (ExceptionAUIAttribut e) {
            LOG.error("Error retrieving an AUI attribute");
            throw new AuthenticationServiceException(e.getMessage());
        } catch (ExceptionAUIHabilitation e) {
            LOG.error("Error in enablement!");
            throw new AuthenticationServiceException(e.getMessage());
        } catch (ExceptionAUIConnect e) {
            LOG.error("There was a problem connecting to AUI. Message: " + e.getMessage());
            throw new AuthenticationServiceException(e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }

    /**
     * {@inheritDoc}
     */
    public void setMessageSource(MessageSource messageSource) {
        this.messages = new MessageSourceAccessor(messageSource);
    }
}
