package fr.sfr.impact.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateTimeUtils {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd-HHmmss");

    private DateTimeUtils() {
    }

    /**
     * Get current formatted date and time.
     *
     * @return now, formatted.
     */
    public static String formattedDateTime() {
        return DATE_FORMAT.format(new Date());
    }
}
