package fr.sfr.impact.util;

import org.apache.commons.configuration.Configuration;
import org.neo4j.kernel.InternalAbstractGraphDatabase;
import org.neo4j.server.WrappingNeoServerBootstrapper;
import org.neo4j.server.configuration.Configurator;
import org.neo4j.server.configuration.ServerConfigurator;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.AbstractFactoryBean;

/**
 * FactoryBean class for convenient create and configuration of Neo4j's WrappingNeoServerBootstrapper
 *
 * @author Tareq Abedrabbo
 */
public class WrappingNeoServerBootstrapperFactoryBean extends AbstractFactoryBean<WrappingNeoServerBootstrapper>
        implements InitializingBean, DisposableBean {

    private InternalAbstractGraphDatabase embeddedGraphDatabase;

    private int webServerPort = Configurator.DEFAULT_WEBSERVER_PORT;
    private String webServerAddress = "0.0.0.0";
    private WrappingNeoServerBootstrapper neoServer;

    public WrappingNeoServerBootstrapperFactoryBean(InternalAbstractGraphDatabase embeddedGraphDatabase) {
        this.embeddedGraphDatabase = embeddedGraphDatabase;
    }

    @Override
    public Class<?> getObjectType() {
        return WrappingNeoServerBootstrapper.class;
    }

    @Override
    protected WrappingNeoServerBootstrapper createInstance() throws Exception {
        setSingleton(true);
        ServerConfigurator serverConfigurator = new ServerConfigurator(embeddedGraphDatabase);
        Configuration config = serverConfigurator.configuration();
        config.setProperty(Configurator.WEBSERVER_PORT_PROPERTY_KEY, webServerPort);
        config.setProperty(Configurator.WEBSERVER_ADDRESS_PROPERTY_KEY, webServerAddress);

        neoServer = new WrappingNeoServerBootstrapper(embeddedGraphDatabase, serverConfigurator);

        return neoServer;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        super.afterPropertiesSet();
        neoServer.start();
    }

    @Override
    public void destroy() throws Exception {
        neoServer.stop();
        super.destroy();
    }

    @Override
    final public void setSingleton(boolean singleton) {
        if(!singleton) {
            throw new UnsupportedOperationException("This bean factory only supports singleton scope");
        }
        super.setSingleton(true);
    }

    @Override
    final public boolean isSingleton() {
        return super.isSingleton();
    }

    public int getWebServerPort() {
        return webServerPort;
    }

    public void setWebServerPort(int webServerPort) {
        this.webServerPort = webServerPort;
    }

    public String getWebServerAddress() {
        return webServerAddress;
    }

    public void setWebServerAddress(String webServerAddress) {
        this.webServerAddress = webServerAddress;
    }
}
