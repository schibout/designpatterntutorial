package fr.sfr.impact.web.controller;

import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.service.topdown.TopDownResultService;
import fr.sfr.impact.web.view.TopDownExcelView;
import fr.sfr.impact.service.excel.TopDownExcelReportBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.View;

/**
 * Controller for the top down analysis result pages.
 *
 * @author Michal Bachman
 */
@Controller
public class TopDownController extends PagedNodesController {

    @Autowired
    private TopDownResultService topDownService;

    @Autowired
    private TopDownExcelReportBuilder topDownExcelReportBuilder;

    @RequestMapping("/nodes/{nodeId}/topdown/**")
    public String getImpactedServices(Model model, @PathVariable long nodeId, PagingCriteria pagingCriteria) {
        model.addAttribute("activeTab", "services");
        addResultsAndPagingToModel(nodeId, model, pagingCriteria, topDownService.getImpactedServices(nodeId, pagingCriteria));
        return "topdown/result";
    }

    @RequestMapping("/nodes/{nodeId}/topdown/equipment")
    public String getImpactedEquipment(Model model, @PathVariable long nodeId, PagingCriteria pagingCriteria) {
        model.addAttribute("activeTab", "equipment");
        addResultsAndPagingToModel(nodeId, model, pagingCriteria, topDownService.getImpactedEquipment(nodeId, pagingCriteria));
        return "topdown/result";
    }

    @RequestMapping("/nodes/{nodeId}/topdown/nodes")
    public String getNodesOnPath(Model model, @PathVariable long nodeId, PagingCriteria pagingCriteria) {
        model.addAttribute("activeTab", "nodes_on_path");
        addResultsAndPagingToModel(nodeId, model, pagingCriteria, topDownService.getImpactedNodes(nodeId, pagingCriteria));
        return "topdown/result";
    }

    @RequestMapping("/nodes/{nodeId}/topdown.xls")
    public View getAllDetails(Model model, @PathVariable long nodeId) {
        model.addAttribute("analysisResult", topDownService.getFullResult(nodeId));
        return new TopDownExcelView(localeResolver, topDownExcelReportBuilder);
    }
}
