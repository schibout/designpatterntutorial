package fr.sfr.impact.web.controller;

import fr.sfr.impact.domain.job.JobDescription;
import fr.sfr.impact.domain.job.JobStatus;
import fr.sfr.impact.service.job.JobService;
import fr.sfr.impact.web.view.ZipView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import java.util.Locale;

/**
 * RESTful controller for {@link fr.sfr.impact.domain.job.Job}s.
 *
 * @author Michal Bachman
 */
@Controller
public class JobController {

    @Autowired
    private JobService jobService;

    @RequestMapping("/jobs/form")
    public String jobForm(Model model, JobDescription jobDescription) {
        model.addAttribute("pageName", "jobs");
        return "jobs/form";
    }

    @RequestMapping(value = "/jobs", method = RequestMethod.POST)
    public String createJob(Model model, JobDescription jobDescription, Locale locale) {
        jobDescription.setLocale(locale);
        return "redirect:jobs/" + jobService.createJob(jobDescription) + "/status";
    }

    @RequestMapping(value = "jobs/{jobId}/status")
    public String jobStatus(@PathVariable String jobId, Model model) {
        JobStatus.Status status = jobService.getJobStatus(jobId);
        model.addAttribute("status", status.name());
        if (JobStatus.Status.NOT_STARTED.equals(status)) {
            model.addAttribute("refresh", 6);    //refresh every 6s
        }
        if (JobStatus.Status.IN_PROGRESS.equals(status)) {
            model.addAttribute("progress", jobService.getJobProgress(jobId));
            model.addAttribute("refresh", 2);    //refresh every 2s
        }
        if (JobStatus.Status.FINISHED.equals(status)) {
            model.addAttribute("jobId", jobId);
        }
        if (JobStatus.Status.ERROR.equals(status)) {
            model.addAttribute("error", jobService.getError(jobId));
        }
        return "jobs/view";
    }

    @RequestMapping(value = "jobs/{jobId}/result")
    public View jobResult(@PathVariable String jobId, Model model) {
        model.addAttribute("result", jobService.getJobResult(jobId));
        return new ZipView();
    }
}
