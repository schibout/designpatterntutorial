package fr.sfr.impact.web.controller;

import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.service.node.NodeDetailService;
import fr.sfr.impact.dto.NodeDtoSubset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.web.servlet.LocaleResolver;

import java.util.Collections;

/**
 * Abstract super-class for controllers that return results with paged nodes.
 *
 * @author Michal Bachman
 */
public abstract class PagedNodesController {
    @Autowired
    protected MessageSource messageSource;
    @Autowired
    protected LocaleResolver localeResolver;
    @Autowired
    protected NodeDetailService nodeDetailService;

    protected void addResultsAndPagingToModel(long nodeId, Model model, PagingCriteria pagingCriteria, NodeDtoSubset dtoSubset) {
        model.addAttribute("nodeDetail", Collections.singletonList(nodeDetailService.getNodeDetail(nodeId)));
        model.addAttribute("nodes", dtoSubset.getDtos());
        model.addAttribute("totalResults", dtoSubset.getTotalNumber());
        model.addAttribute("maxPages", pagingCriteria.getNumberOfPages(dtoSubset.getTotalNumber()));
    }
}
