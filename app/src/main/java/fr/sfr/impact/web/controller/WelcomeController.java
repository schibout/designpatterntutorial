package fr.sfr.impact.web.controller;

import fr.sfr.impact.domain.search.SearchCriteria;
import fr.sfr.impact.service.StatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for the welcome page.
 *
 * @author Michal Bachman
 */
@Controller
public class WelcomeController {

    @Autowired
    private StatService statService;

    @RequestMapping("/")
    public String welcome(Model model, SearchCriteria searchCriteria) {
        model.addAttribute("pageName", "welcome");
        model.addAttribute("numberOfNodes", statService.getNumberOfNodes());
        model.addAttribute("numberOfRelationships", statService.getNumberOfRelationships());
        return "welcome";
    }
}
