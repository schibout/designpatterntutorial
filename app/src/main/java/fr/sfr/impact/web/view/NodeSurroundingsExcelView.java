package fr.sfr.impact.web.view;

import fr.sfr.impact.service.excel.ExcelBuilder;
import fr.sfr.impact.service.excel.NodeSurroundingsExcelBuilder;
import fr.sfr.impact.dto.NodeSurroundingsDto;
import org.springframework.web.servlet.LocaleResolver;

import java.util.Map;

/**
 * Excel view for node surroundings. Each relationship type and direction will be on a separate tab.
 *
 * @author Michal Bachman
 */
public class NodeSurroundingsExcelView extends ExcelView<NodeSurroundingsDto> {

    private NodeSurroundingsExcelBuilder nodeSurroundingsExcelBuilder;

    public NodeSurroundingsExcelView(LocaleResolver localeResolver, NodeSurroundingsExcelBuilder nodeSurroundingsExcelBuilder) {
        super(localeResolver);
        this.nodeSurroundingsExcelBuilder = nodeSurroundingsExcelBuilder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected NodeSurroundingsDto getDto(Map<String, Object> model) {
        return (NodeSurroundingsDto) model.get("surroundings");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ExcelBuilder<NodeSurroundingsDto> getExcelBuilder() {
        return nodeSurroundingsExcelBuilder;
    }
}
