package fr.sfr.impact.web.controller;

import fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes;
import fr.sfr.impact.domain.RelationshipTypeAndDirection;
import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.service.node.NodeDetailService;
import fr.sfr.impact.service.excel.NodeSurroundingsExcelBuilder;
import fr.sfr.impact.dto.NodeDtoSubset;
import fr.sfr.impact.web.view.NodeSurroundingsExcelView;
import org.neo4j.graphdb.Direction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.View;

import java.util.Collections;
import java.util.List;

/**
 * Controller for the node details and surroundings.
 *
 * @author Michal Bachman
 */
@Controller
public class NodeDetailController  {

    @Autowired
    protected MessageSource messageSource;
    @Autowired
    protected LocaleResolver localeResolver;

    @Autowired
    private NodeDetailService nodeDetailService;

    @Autowired
    private NodeSurroundingsExcelBuilder nodeSurroundingsExcelBuilder;

    @RequestMapping("/nodes/{nodeId}")
    public String nodeDetails(Model model, @PathVariable long nodeId, PagingCriteria pagingCriteria) {
        List<RelationshipTypeAndDirection> relationshipTypesAndDirections = populateBasicInfo(model, nodeId);
        if (!relationshipTypesAndDirections.isEmpty()) {
            populateSurroundings(model, nodeId, pagingCriteria, relationshipTypesAndDirections.get(0));
        }

        return "node/detail";
    }

    @RequestMapping("/nodes/{nodeId}/surroundings/{relationship}/{direction}")
    public String nodeSurroundings(Model model, @PathVariable long nodeId, @PathVariable String relationship, @PathVariable String direction, PagingCriteria pagingCriteria) {
        List<RelationshipTypeAndDirection> relationshipTypesAndDirections = populateBasicInfo(model, nodeId);
        RelationshipTypeAndDirection active = new RelationshipTypeAndDirection(ImpactAnalysisRelationshipTypes.valueOf(relationship.toUpperCase()), Direction.valueOf(direction.toUpperCase()));
        if (relationshipTypesAndDirections.contains(active)) {
            populateSurroundings(model, nodeId, pagingCriteria, active);
        }

        return "node/detail";
    }

    private void populateSurroundings(Model model, long nodeId, PagingCriteria pagingCriteria, RelationshipTypeAndDirection active) {
        model.addAttribute("activeTab", active);
        NodeDtoSubset nodeSurroundings = nodeDetailService.getNodeSurroundings(nodeId, active, pagingCriteria);
        model.addAttribute("surroundings", nodeSurroundings.getDtos());
        model.addAttribute("totalResults", nodeSurroundings.getTotalNumber());
        model.addAttribute("maxPages", pagingCriteria.getNumberOfPages(nodeSurroundings.getTotalNumber()));
    }

    private List<RelationshipTypeAndDirection> populateBasicInfo(Model model, long nodeId) {
        model.addAttribute("nodeDetail", Collections.singletonList(nodeDetailService.getNodeDetail(nodeId)));
        List<RelationshipTypeAndDirection> relationshipTypesAndDirections = nodeDetailService.getRelationshipTypesAndDirections(nodeId);
        model.addAttribute("relationshipTypesAndDirections", relationshipTypesAndDirections);
        return relationshipTypesAndDirections;
    }

    @RequestMapping("/nodes/{nodeId}.xls")
    public View nodeDetailsExcel(Model model, @PathVariable long nodeId) {
        model.addAttribute("surroundings", nodeDetailService.getAllNodeSurroundings(nodeId));
        return new NodeSurroundingsExcelView(localeResolver, nodeSurroundingsExcelBuilder);
    }

    @RequestMapping("/nodes/{nodeId}/surroundings/{relationship}/{direction}.xls")
    public View nodeSurroundingsExcel(Model model, @PathVariable long nodeId, @PathVariable String relationship, @PathVariable String direction) {
        RelationshipTypeAndDirection active = new RelationshipTypeAndDirection(ImpactAnalysisRelationshipTypes.valueOf(relationship.toUpperCase()), Direction.valueOf(direction.toUpperCase()));
        model.addAttribute("surroundings", nodeDetailService.getAllNodeSurroundings(nodeId, active));
        return new NodeSurroundingsExcelView(localeResolver, nodeSurroundingsExcelBuilder);
    }
}
