package fr.sfr.impact.web.view;

import fr.sfr.impact.domain.job.JobResult;
import fr.sfr.impact.util.DateTimeUtils;
import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * A view that produces a zip file (for {@link fr.sfr.impact.domain.job.Job} results).
 */
public class ZipView extends AbstractView {

    private static final String CONTENT_TYPE = "application/zip";
    private static final String EXTENSION = ".zip";

    /**
     * Default Constructor.
     * Sets the content type of the view to "application/vnd.ms-excel".
     */
    public ZipView() {
        setContentType(CONTENT_TYPE);
    }

    @Override
    protected boolean generatesDownloadContent() {
        return true;
    }

    /**
     * Renders the Excel view, given the specified model.
     */
    @Override
    protected final void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setHeader("Content-Disposition", "attachment; filename=\"job-" + DateTimeUtils.formattedDateTime() + EXTENSION + "\"");
        response.setContentType(getContentType());
        ServletOutputStream out = response.getOutputStream();
        out.write(((JobResult) model.get("result")).toByteArray());
        out.flush();
    }
}
