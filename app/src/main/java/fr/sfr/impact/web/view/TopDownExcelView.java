package fr.sfr.impact.web.view;

import fr.sfr.impact.service.excel.ExcelBuilder;
import fr.sfr.impact.service.excel.TopDownExcelReportBuilder;
import fr.sfr.impact.dto.TopDownResultDto;
import org.springframework.web.servlet.LocaleResolver;

import java.util.Map;

/**
 * Excel view for top-down analysis.
 *
 * @author Michal Bachman
 */
public class TopDownExcelView extends ExcelView<TopDownResultDto> {

    private final TopDownExcelReportBuilder topDownExcelReportBuilder;

    public TopDownExcelView(LocaleResolver localeResolver, TopDownExcelReportBuilder topDownExcelReportBuilder) {
        super(localeResolver);
        this.topDownExcelReportBuilder = topDownExcelReportBuilder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected TopDownResultDto getDto(Map<String, Object> model) {
        return (TopDownResultDto) model.get("analysisResult");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ExcelBuilder<TopDownResultDto> getExcelBuilder() {
        return topDownExcelReportBuilder;
    }
}
