package fr.sfr.impact.web.controller;

import fr.sfr.impact.domain.topdown.TdResponse;
import fr.sfr.impact.service.topdown.GraphTdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GraphTdAnalysisController {

    @Autowired
    private GraphTdService graphTdService;

    @RequestMapping("/nodes/{nodeId}/topdown/graph")
    public String renderPage(@PathVariable long nodeId, @RequestParam(value = "impactedNode", required = false) Long impactedNode, Model model) {
        model.addAttribute("nodeId", nodeId);
        model.addAttribute("impactedNode", impactedNode);
        return "topdown/graph";
    }

    @RequestMapping("/nodes/{nodeId}/topdown/graph/data")
    public ResponseEntity<TdResponse> getTdGraph(@PathVariable long nodeId, @RequestParam(value = "impactedNode", required = false) Long impactedNode) {
        TdResponse response = graphTdService.tdGraphFrom(nodeId, impactedNode);
        ResponseEntity<TdResponse> responseEntity = createResponse(response);
        return responseEntity;
    }

    @RequestMapping(value = "/nodes/{nodeId}/topdown/graph/surroundings", method = RequestMethod.GET)
    public ResponseEntity<TdResponse> surrounding(@PathVariable("nodeId") long nodeId) {
        TdResponse response = graphTdService.surroundings(nodeId);
        ResponseEntity<TdResponse> responseEntity = createResponse(response);
        return responseEntity;
    }

    private ResponseEntity<TdResponse> createResponse(TdResponse response) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<TdResponse>(response, headers, HttpStatus.OK);
    }
}
