package fr.sfr.impact.web.controller;

import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.service.bottomup.BottomUpResultService;
import fr.sfr.impact.service.excel.BottomUpExcelReportBuilder;
import fr.sfr.impact.web.view.BottomUpExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.View;

import java.util.Collections;

/**
 * Controller for the bottom up analysis result pages.
 *
 * @author Michal Bachman
 */
@Controller
public class BottomUpController extends PagedNodesController {

    @Autowired
    private BottomUpResultService bottomUpService;

    @Autowired
    private BottomUpExcelReportBuilder bottomUpExcelReportBuilder;

    @RequestMapping("/nodes/{nodeId}/bottomup/**")
    public String getImpactedClients(Model model, @PathVariable long nodeId, PagingCriteria pagingCriteria) {
        model.addAttribute("activeTab", "clients");
        addResultsAndPagingToModel(nodeId, model, pagingCriteria, bottomUpService.getImpactedClients(Collections.singleton(nodeId), pagingCriteria));
        return "bottomup/result";
    }

    @RequestMapping("/nodes/{nodeId}/bottomup/services")
    public String getImpactedServices(Model model, @PathVariable long nodeId, PagingCriteria pagingCriteria) {
        model.addAttribute("activeTab", "services");
        addResultsAndPagingToModel(nodeId, model, pagingCriteria, bottomUpService.getImpactedServices(Collections.singleton(nodeId), pagingCriteria));
        return "bottomup/result";
    }

    @RequestMapping("/nodes/{nodeId}/bottomup/nodes")
    public String getNodesOnPath(Model model, @PathVariable long nodeId, PagingCriteria pagingCriteria) {
        model.addAttribute("activeTab", "nodes_on_path");
        addResultsAndPagingToModel(nodeId,  model, pagingCriteria, bottomUpService.getNodesOnPath(Collections.singleton(nodeId), pagingCriteria));
        return "bottomup/result";
    }

    @RequestMapping("/nodes/{nodeId}/bottomup/clients/{clientId}/services")
    public String getImpactedServicesForClient(Model model, @PathVariable long nodeId, @PathVariable long clientId, PagingCriteria pagingCriteria) {
        model.addAttribute("activeTab", "client_services");
        model.addAttribute("client", nodeDetailService.getNodeDetail(clientId));
        addResultsAndPagingToModel(nodeId,  model, pagingCriteria, bottomUpService.getImpactedServicesForClient(Collections.singleton(nodeId), clientId, pagingCriteria));
        return "bottomup/result";
    }

    @RequestMapping("/nodes/{nodeId}/bottomup.xls")
    public View getAllDetails(Model model, @PathVariable long nodeId) {
        model.addAttribute("analysisResult", bottomUpService.getFullResult(Collections.singleton(nodeId)));
        return new BottomUpExcelView(localeResolver, bottomUpExcelReportBuilder);
    }
}
