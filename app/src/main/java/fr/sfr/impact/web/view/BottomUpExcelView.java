package fr.sfr.impact.web.view;

import fr.sfr.impact.service.excel.BottomUpExcelReportBuilder;
import fr.sfr.impact.service.excel.ExcelBuilder;
import fr.sfr.impact.dto.BottomUpResultDto;
import org.springframework.web.servlet.LocaleResolver;

import java.util.Map;

/**
 * Excel view for bottom-up analysis.
 *
 * @author Michal Bachman
 */
public class BottomUpExcelView extends ExcelView<BottomUpResultDto> {

    private final BottomUpExcelReportBuilder bottomUpExcelReportBuilder;

    public BottomUpExcelView(LocaleResolver localeResolver, BottomUpExcelReportBuilder bottomUpExcelReportBuilder) {
        super(localeResolver);
        this.bottomUpExcelReportBuilder = bottomUpExcelReportBuilder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BottomUpResultDto getDto(Map<String, Object> model) {
        return (BottomUpResultDto) model.get("analysisResult");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ExcelBuilder<BottomUpResultDto> getExcelBuilder() {
        return bottomUpExcelReportBuilder;
    }
}
