package fr.sfr.impact.web.controller;

import fr.sfr.impact.domain.Constants;
import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.domain.search.SearchCriteria;
import fr.sfr.impact.service.search.SearchService;
import fr.sfr.impact.dto.NodeDtoSubset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for all the node searches.
 *
 * @author Michal Bachman
 */
@Controller
public class SearchController {

    @Autowired
    private SearchService searchService;

    @RequestMapping("/clients")
    public String searchClients(Model model, SearchCriteria searchCriteria, PagingCriteria pagingCriteria) {
        searchCriteria.setType(Constants.CLIENT);
        String redirect = performSearch(model, searchCriteria, pagingCriteria);
        if (redirect != null) {
            return redirect;
        }
        return enrichClientModelAndReturnView(model);
    }

    @RequestMapping("/clients/form")
    public String clientsSearchForm(Model model, SearchCriteria searchCriteria) {
        return enrichClientModelAndReturnView(model);
    }

    private String enrichClientModelAndReturnView(Model model) {
        model.addAttribute("fixedType", Constants.CLIENT);
        model.addAttribute("pageName", "clients");
        return "clients/list";
    }

    @RequestMapping("/services")
    public String searchServices(Model model, SearchCriteria searchCriteria, PagingCriteria pagingCriteria) {
        searchCriteria.setType(Constants.SERVICE);
        String redirect = performSearch(model, searchCriteria, pagingCriteria);
        if (redirect != null) {
            return redirect;
        }
        return enrichServiceModelAndReturnView(model);
    }

    @RequestMapping("/services/form")
    public String servicesSearchForm(Model model, SearchCriteria searchCriteria) {
        return enrichServiceModelAndReturnView(model);
    }

    private String enrichServiceModelAndReturnView(Model model) {
        model.addAttribute("fixedType", Constants.SERVICE);
        model.addAttribute("pageName", "services");
        return "services/list";
    }

    @RequestMapping("/sites")
    public String searchSites(Model model, SearchCriteria searchCriteria, PagingCriteria pagingCriteria) {
        searchCriteria.setType(Constants.SITE);
        String redirect = performSearch(model, searchCriteria, pagingCriteria);
        if (redirect != null) {
            return redirect;
        }
        return enrichSiteModelAndReturnView(model);
    }

    @RequestMapping("/sites/form")
    public String sitesSearchForm(Model model, SearchCriteria searchCriteria) {
        return enrichSiteModelAndReturnView(model);
    }

    private String enrichSiteModelAndReturnView(Model model) {
        model.addAttribute("fixedType", Constants.SITE);
        model.addAttribute("pageName", "sites");
        return "sites/list";
    }

    @RequestMapping("/equipment")
    public String searchEquipment(Model model, SearchCriteria searchCriteria, PagingCriteria pagingCriteria) {
        String redirect = performSearch(model, searchCriteria, pagingCriteria);
        if (redirect != null) {
            return redirect;
        }
        return enrichEquipmentModelAndReturnView(model);
    }

    @RequestMapping("/equipment/form")
    public String equipmentSearchForm(Model model, SearchCriteria searchCriteria) {
        return enrichEquipmentModelAndReturnView(model);
    }

    private String enrichEquipmentModelAndReturnView(Model model) {
        model.addAttribute("pageName", "equipment");
        return "equipment/list";
    }

    private String performSearch(Model model, SearchCriteria searchCriteria, PagingCriteria pagingCriteria) {
        NodeDtoSubset searchResult = searchService.search(searchCriteria, pagingCriteria);

        if (searchResult.getTotalNumber() == 1) {
            return "redirect:/nodes/" + searchResult.getDtos().get(0).getNeoId();
        }

        model.addAttribute("searchResults", searchResult.getDtos());
        model.addAttribute("totalResults", searchResult.getTotalNumber());
        model.addAttribute("maxPages", pagingCriteria.getNumberOfPages(searchResult.getTotalNumber()));

        return null;
    }
}
