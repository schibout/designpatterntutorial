package fr.sfr.impact.web.view;

import fr.sfr.impact.dto.Dto;
import fr.sfr.impact.service.excel.ExcelBuilder;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * A {@link org.springframework.web.servlet.View} that produces an excel workbook from a {@link Dto} and streams it into the HTTP response.
 *
 * @param <T> type of the {@link Dto} that this is a view for.
 * @author Michal Bachman
 */
public abstract class ExcelView<T extends Dto> extends AbstractExcelView {

    private final LocaleResolver localeResolver;

    /**
     * Construct a new view.
     *
     * @param localeResolver to resolve locale from requests.
     */
    protected ExcelView(LocaleResolver localeResolver) {
        this.localeResolver = localeResolver;
    }

    /**
     * Get the DTO with data for the view.
     *
     * @param model to get the data from.
     * @return DTO.
     */
    protected abstract T getDto(Map<String, Object> model);

    /**
     * Get the builder class that the actual construction of the excel workbook will be delegated to.
     *
     * @return excel builder.
     */
    protected abstract ExcelBuilder<T> getExcelBuilder();

    /**
     * {@inheritDoc}
     */
    @Override
    protected void buildExcelDocument(Map<String, Object> model, XSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String fileName = getExcelBuilder().enrich(workbook, getDto(model), localeResolver.resolveLocale(request));
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
    }
}
