package fr.sfr.impact.importer;

import fr.sfr.impact.domain.Constants;
import fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes;
import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.domain.search.SearchCriteria;
import fr.sfr.impact.dto.NodeDto;
import fr.sfr.impact.dto.NodeDtoSubset;
import fr.sfr.impact.service.search.SearchService;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.tooling.GlobalGraphOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@ContextConfiguration(locations = {"classpath:importer.xml", "classpath:test-database-importer.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class ImportIntegrationTest {

    @Autowired
    private NodeImporter nodeImporter;
    @Autowired
    private RelationshipImporter relationshipImporter;
    @Autowired
    private RelationshipPropertiesImporter relationshipPropertiesImporter;
    @Autowired
    private SearchService searchService;
    @Autowired
    private GraphDatabaseService database;

    @Test
    public void allDataShouldBeCorrectlyImported() {
        nodeImporter.importData();
        relationshipImporter.importData();
        relationshipPropertiesImporter.importData();

        Iterable<Node> nodes = GlobalGraphOperations.at(database).getAllNodes();
        int count = 0;
        for (Node node : nodes) {
            count++;
        }

        assertEquals(8, count);
        assertTrue(searchService.nodeExists("NODE15"));
        Assert.assertEquals(searchService.findSingleNode("NODE16"), searchService.findSingleNode("NODE15").getSingleRelationship(ImpactAnalysisRelationshipTypes.EST_CONNECTE_A, Direction.OUTGOING).getEndNode());
        Assert.assertEquals(Constants.PRIMARY, searchService.findSingleNode("NODE4").getSingleRelationship(ImpactAnalysisRelationshipTypes.EST_COMPOSE_DE, Direction.OUTGOING).getProperty(Constants.PARTAGE));

        NodeDtoSubset nodeDtos = searchService.search(new SearchCriteria("*#*", null, null, null, null, null, null), PagingCriteria.EVERYTHING);
        Assert.assertEquals(1, nodeDtos.getTotalNumber());

        NodeDto nodeDto = nodeDtos.getDtos().get(0);
        Assert.assertEquals("NODE#éàè17", nodeDto.getProperty(Constants.ID));
        Assert.assertEquals("17", nodeDto.getProperty(Constants.NAME));
        Assert.assertEquals("220", nodeDto.getProperty(Constants.LEVEL));
        Assert.assertEquals("PORT", nodeDto.getProperty(Constants.TYPE));
        Assert.assertEquals("PHYSIQUE", nodeDto.getProperty(Constants.TYPO));
        Assert.assertEquals("GATO", nodeDto.getProperty(Constants.SOURCE));
        Assert.assertEquals("SDH", nodeDto.getProperty(Constants.TECHNO));
        Assert.assertEquals("test", nodeDto.getProperty(Constants.EQ_TYPE));
        Assert.assertEquals("2Mb/s", nodeDto.getProperty(Constants.CAPACITY));
    }
}
