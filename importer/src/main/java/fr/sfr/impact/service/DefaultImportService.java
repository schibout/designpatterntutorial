package fr.sfr.impact.service;

import fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes;
import fr.sfr.impact.domain.search.SearchCriteria;
import fr.sfr.impact.service.search.SearchService;
import fr.sfr.impact.util.FileScanner;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.neo4j.graphdb.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.util.List;

import static fr.sfr.impact.domain.Constants.*;
import static fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes.EST_AFFECTE_A;
import static fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes.EST_CONNECTE_A;

/**
 * Default production implementation of {@link ImportService}.
 *
 * @author Michal Bachman
 */
public class DefaultImportService implements ImportService {
    private static final Logger LOG = Logger.getLogger(DefaultImportService.class);
    private static final int BATCH_SIZE = 1000;
    private static final String SEMICOLON = ";";

    @Autowired
    private GraphDatabaseService database;
    @Autowired
    protected SearchService searchService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void importNodes(final File file) {
        processLines(FileScanner.produceLines(file), new LineProcessor() {
            @Override
            public boolean processLine(int lineNumber, String line) {
                String cols[] = line.split(SEMICOLON);

                if (cols.length < 7) {
                    LOG.warn("Line '" + line + "' does not have 7 columns! (File: " + file.getName() + ", Line No.: " + lineNumber + ")");
                    return false;
                }

                String id = cols[0];
                if (StringUtils.isBlank(id)) {
                    LOG.warn("Node on line '" + line + "' has blank ID. It will not be imported! (File: " + file.getName() + ", Line No.: " + lineNumber + ")");
                    return false;
                }

                if (searchService.nodeExists(id)) {
                    LOG.warn("Node " + id + " already exists! (File: " + file.getName() + ", Line No.: " + lineNumber + ")");
                    return false;
                }

                Node node = database.createNode();
                setAndIndexProperty(node, ID, id);
                setAndIndexProperty(node, NAME, cols[1]);
                setProperty(node, LEVEL, cols[2]);
                setAndIndexProperty(node, TYPE, cols[3]);
                setProperty(node, TYPO, cols[4]);
                setAndIndexProperty(node, SOURCE, cols[5]);
                setAndIndexProperty(node, TECHNO, cols[6]);
                if (cols.length >= 8) {
                    setAndIndexProperty(node, EQ_TYPE, cols[7]);
                }
                if (cols.length >= 9) {
                    setAndIndexProperty(node, CAPACITY, cols[8]);
                }
                return true;
            }
        });
    }

    /**
     * Set a property on a node and index it. Only happens if the property value is not blank.
     *
     * @param node          to set property on.
     * @param propertyName  name.
     * @param propertyValue value.
     */
    private void setAndIndexProperty(Node node, String propertyName, String propertyValue) {
        if (setProperty(node, propertyName, propertyValue)) {
            database.index().forNodes(SFR_NODES_INDEX).add(node, propertyName, SearchCriteria.encodeForSearch(propertyValue));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void importRelationships(final File file) {
        processLines(FileScanner.produceLines(file), new LineProcessor() {
            @Override
            public boolean processLine(int lineNumber, String line) {
                String cols[] = line.split(SEMICOLON);

                if (cols.length < 3) {
                    LOG.warn("Line '" + line + "' does not have 3 columns! (File: " + file.getName() + ", Line No.: " + lineNumber + ")");
                    return false;
                }

                Node firstNode = searchService.findSingleNode(cols[0].trim());
                if (firstNode == null) {
                    LOG.warn("First node does not exist: " + cols[0] + " (File: " + file.getName() + ", Line No.: " + lineNumber + ")");
                    return false;
                }

                Node secondNode = searchService.findSingleNode(cols[2].trim());
                if (secondNode == null) {
                    LOG.warn("Second node does not exist: " + cols[2] + " (File: " + file.getName() + ", Line No.: " + lineNumber + ")");
                    return false;
                }

                RelationshipType relationshipType = findRelationshipType(cols[1], file, lineNumber);
                if (relationshipType == null) {
                    return false;
                }

                if (relationshipExists(firstNode, secondNode, relationshipType)) {
                    LOG.warn("Relationship between " + firstNode + " and " + secondNode + " of type " + relationshipType.name() + " already exists. (File: " + file.getName() + ", Line No.: " + lineNumber + ")");
                    return false;
                }

                firstNode.createRelationshipTo(secondNode, relationshipType);
                if (EST_CONNECTE_A.name().equals(relationshipType.name())) {
                    secondNode.createRelationshipTo(firstNode, relationshipType);
                }
                return true;
            }
        });
    }

    /**
     * Set property on a node. Only happens if the property value is not blank.
     *
     * @param node          to set property on.
     * @param propertyName  name.
     * @param propertyValue value.
     * @return true if and only if the property has been actually set.
     */
    private boolean setProperty(Node node, String propertyName, String propertyValue) {
        if (StringUtils.isBlank(propertyValue)) {
            return false;
        }
        node.setProperty(propertyName, propertyValue);
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void importRelationshipProperties(final File file) {
        processLines(FileScanner.produceLines(file), new LineProcessor() {
            @Override
            public boolean processLine(int lineNumber, String line) {
                String cols[] = line.split(SEMICOLON);

                if (cols.length < 4) {
                    LOG.warn("Line '" + line + "' does not have 4 columns! (File: " + file.getName() + ", Line No.: " + lineNumber + ")");
                    return false;
                }

                Node firstNode = searchService.findSingleNode(cols[0].trim());
                if (firstNode == null) {
                    LOG.warn("First node does not exist: " + cols[0] + " (File: " + file.getName() + ", Line No.: " + lineNumber + ")");
                    return false;
                }

                Node secondNode = searchService.findSingleNode(cols[2].trim());
                if (secondNode == null) {
                    LOG.warn("Second node does not exist: " + cols[2] + " (File: " + file.getName() + ", Line No.: " + lineNumber + ")");
                    return false;
                }

                RelationshipType relationshipType = findRelationshipType(cols[1], file, lineNumber);
                if (relationshipType == null) {
                    return false;
                }

                Relationship relationship = findRelationship(firstNode, secondNode, relationshipType);
                if (relationship == null) {
                    LOG.warn("Relationship between " + firstNode + " and " + secondNode + " of type " + relationshipType.name() + " does not exist! (File: " + file.getName() + ", Line No.: " + lineNumber + ")");
                    return false;
                }

                relationship.setProperty(cols[3].trim(), cols[4].trim());
                return true;
            }
        });
    }

    /**
     * Find a relationship type from its name.
     *
     * @param relationshipName name of the type.
     * @param file             where this information originated, for logging purposes.
     * @param lineNumber       where this information originated, for logging purposes.
     * @return the type, null if none exists.
     */
    private RelationshipType findRelationshipType(String relationshipName, File file, int lineNumber) {
        relationshipName = relationshipName.trim();
        //temp hack
        if ("EST_AFFECTER_A".equals(relationshipName)) {
            relationshipName = EST_AFFECTE_A.name();
        }

        try {
            return ImpactAnalysisRelationshipTypes.valueOf(relationshipName);
        } catch (IllegalArgumentException e) {
            LOG.warn("Relationship type " + relationshipName + " does not exist! (File: " + file.getName() + ", Line No.: " + lineNumber + ")");
            return null;
        }
    }

    /**
     * Find out whether a relationship exists between two nodes.
     *
     * @param startNode        startNode.
     * @param endNode          endNode.
     * @param relationshipType type to check.
     * @return true if and only if a relationship between these two nodes exists.
     */
    private boolean relationshipExists(Node startNode, Node endNode, RelationshipType relationshipType) {
        for (Relationship relationship : startNode.getRelationships(relationshipType, Direction.OUTGOING)) {
            if (relationship.getOtherNode(startNode).equals(endNode)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Find a relationship between two nodes.
     *
     * @param startNode        start node.
     * @param endNode          end node.
     * @param relationshipType to find.
     * @return the relationship, null if none exists.
     */
    private Relationship findRelationship(Node startNode, Node endNode, RelationshipType relationshipType) {
        for (Relationship relationship : startNode.getRelationships(relationshipType, Direction.OUTGOING)) {
            if (relationship.getOtherNode(startNode).equals(endNode)) {
                return relationship;
            }
        }
        return null;
    }

    /**
     * Process all lines in batches.
     *
     * @param lines         to process.
     * @param lineProcessor to use.
     */
    private void processLines(List<String> lines, LineProcessor lineProcessor) {
        LOG.info("About to import " + lines.size() + " items");

        int importedBatches = 0;
        int importedEntries = 0;
        while (importedBatches * BATCH_SIZE < lines.size()) {
            importedEntries += processBatch(lines, importedBatches * BATCH_SIZE, Math.min(importedBatches * BATCH_SIZE + BATCH_SIZE - 1, lines.size() - 1), lineProcessor);
            importedBatches++;
        }

        LOG.info("Imported " + importedEntries + " items (out of " + lines.size() + ") in " + importedBatches + " batches. Skipped " + (lines.size() - importedEntries) + " items, see previous log entries.");
    }

    /**
     * Process a batch of lines in transaction.
     *
     * @param lines         all lines to process.
     * @param startIndex    0-based start index.
     * @param endIndex      0-based end index.
     * @param lineProcessor to use.
     * @return how many lines actually produced an entry.
     */
    private int processBatch(final List<String> lines, int startIndex, int endIndex, LineProcessor lineProcessor) {
        int result = 0;
        Transaction tx = database.beginTx();
        try {
            for (int i = startIndex; i <= endIndex; i++) {
                if (lineProcessor.processLine(i, lines.get(i))) {
                    result++;
                }
            }
            LOG.debug("Committing transaction for batch " + startIndex + " - " + endIndex);
            tx.success();
        } catch (Exception e) {
            LOG.warn("Failed committing transaction for batch " + startIndex + " - " + endIndex + ", exception: " + e);
            tx.failure();
        } finally {
            tx.finish();
        }
        return result;
    }

    /**
     * Interface intended for anonymous implementations that can process individual lines.
     */
    interface LineProcessor {
        /**
         * Process a single line.
         *
         * @param lineNumber line number for logging purposes.
         * @param line       the actual line.
         * @return true if and only if this line produced an entry.
         */
        boolean processLine(int lineNumber, String line);
    }
}
