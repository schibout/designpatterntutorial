package fr.sfr.impact.service;

import java.io.File;

/**
 * Service for importing nodes and relationships.
 *
 * @author Michal Bachman
 */
public interface ImportService {

    /**
     * Import nodes from a CSV file's lines.
     *
     * @param file from which to import nodes.
     */
    void importNodes(File file);

    /**
     * Import relationships from a CSV file's lines.
     *
     * @param file from which to import relationships.
     */
    void importRelationships(File file);

    /**
     * Import relationship properties from a CSV file's lines.
     *
     * @param file from which to import relationship properties.
     */
    void importRelationshipProperties(File file);
}
