package fr.sfr.impact.importer;

import fr.sfr.impact.service.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;

/**
 * Importer for relationship properties.
 *
 * @author Michal Bachman
 */
public class RelationshipPropertiesImporter extends Importer {

    @Autowired
    private ImportService importService;
    private String relationshipPropertiesDirectory = "/attributRelation/";

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("importer.xml", "database-importer.xml");
        context.registerShutdownHook();
        RelationshipPropertiesImporter relationshipImporter = context.getBean(RelationshipPropertiesImporter.class);
        relationshipImporter.importData();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getDirectory() {
        return relationshipPropertiesDirectory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void processFile(File file) {
        importService.importRelationshipProperties(file);
    }

    public void setRelationshipPropertiesDirectory(String relationshipPropertiesDirectory) {
        this.relationshipPropertiesDirectory = relationshipPropertiesDirectory;
    }
}
