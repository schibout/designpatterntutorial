package fr.sfr.impact.importer;

import fr.sfr.impact.service.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;

/**
 * Importer for relationships.
 *
 * @author Michal Bachman
 */
public class RelationshipImporter extends Importer {

    @Autowired
    private ImportService importService;
    private String relationshipDirectory = "/relation/";

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("importer.xml", "database-importer.xml");
        context.registerShutdownHook();
        RelationshipImporter relationshipImporter = context.getBean(RelationshipImporter.class);
        relationshipImporter.importData();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getDirectory() {
        return relationshipDirectory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void processFile(File file) {
        importService.importRelationships(file);
    }

    public void setRelationshipDirectory(String relationshipDirectory) {
        this.relationshipDirectory = relationshipDirectory;
    }
}
