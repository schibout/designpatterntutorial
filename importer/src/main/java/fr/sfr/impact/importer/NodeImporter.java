package fr.sfr.impact.importer;

import fr.sfr.impact.service.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;

/**
 * Importer for nodes.
 *
 * @author Michal Bachman
 */
public class NodeImporter extends Importer {

    @Autowired
    private ImportService importService;
    private String nodeDirectory = "/node/";

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("importer.xml", "database-importer.xml");
        context.registerShutdownHook();
        NodeImporter nodeImporter = context.getBean(NodeImporter.class);
        nodeImporter.importData();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getDirectory() {
        return nodeDirectory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void processFile(File file) {
        importService.importNodes(file);
    }

    public void setNodeDirectory(String nodeDirectory) {
        this.nodeDirectory = nodeDirectory;
    }
}