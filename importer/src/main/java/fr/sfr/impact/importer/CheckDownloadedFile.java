package fr.sfr.impact.importer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class CheckDownloadedFile {

	protected static PrintWriter outFileLog = null;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// on charge dans une arrayList la liste des fichiers pr�sents dans
		// chaque repertoire rm71, rm21, et mobile
		// on parcours de la liste des fichiers � controler de chaque module

		// r�cup�ration des properties
		Properties propGatoProvionning = new Properties();
		try {
			propGatoProvionning = loadProperties();
		} catch (IOException e1) {
			e1.getMessage();
		}

		// fichier de log
		String logFilePath_out_LogFile = "";
		String propFile = propGatoProvionning.getProperty("prop.ctrl.downloadLogFile").trim();
		String propDir = propGatoProvionning.getProperty("prop.ctrl.downloadLogDir").trim();
		logFilePath_out_LogFile = propDir + returnStringDate() + "_" + propFile;
		try {
			outFileLog = new PrintWriter(new FileWriter(logFilePath_out_LogFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		outFileLog.print("D�but du traitement  :   controle des fichiers : Heure de D�but :" + returnStringDate() + "\n");

		List<String> repFixRm71List = new ArrayList<String>();
		List<String> repFixRm21List = new ArrayList<String>();
		List<String> repMobileList = new ArrayList<String>();

		File t1 = new File(propGatoProvionning.getProperty("prop.inFileDownloadDirRm71").trim());
		File t2 = new File(propGatoProvionning.getProperty("prop.inFileDownloadDirRm21").trim());
		File t3 = new File(propGatoProvionning.getProperty("prop.inFileDownloadDirMobile").trim());

		repFixRm71List = listerRepertoire(t1);
		repFixRm21List = listerRepertoire(t2);
		repMobileList = listerRepertoire(t3);

		List<String> l = new ArrayList<String>();
		String s1 = propGatoProvionning.getProperty("prop.chkGatoFixeRm71XlsFileList").trim() + ";" + propGatoProvionning.getProperty("prop.resultChkGatoFixeRm71XlsFileList").trim() + ";"
				+ propGatoProvionning.getProperty("prop.koChkGatoFixeRm71XlsFileList").trim();

		String s2 = propGatoProvionning.getProperty("prop.chkGatoFixeRm21XlsFileList").trim() + ";" + propGatoProvionning.getProperty("prop.resultChkGatoFixeRm21XlsFileList").trim() + ";"
				+ propGatoProvionning.getProperty("prop.koChkGatoFixeRm21XlsFileList").trim();

		String s3 = propGatoProvionning.getProperty("prop.chkGatoMobileXlsFileList").trim() + ";" + propGatoProvionning.getProperty("prop.resultChkGatoMobileXlsFileList").trim() + ";"
				+ propGatoProvionning.getProperty("prop.koChkGatoMobileXlsFileList").trim();

		l.add(s1);
		l.add(s2);
		l.add(s3);

		for (int i = 0; i < l.size(); i++)
		// D�but boucle download en fonction des r�f�rentiels gato-fixe rm21,
		// rm71, et gato-mobile
		{
			String[] ts = l.get(i).split(";");
			String outFile = "";
			String inFileList = "";
			String koFile = "";

			inFileList = ts[0];
			outFile = ts[1];
			koFile = ts[2];

			System.out.println("fichier lu : " + inFileList);

			// Lecture de la liste des fichiers � controler

			try {
				Scanner scanner;
				try {
					String s = inFileList;
					scanner = new Scanner(new File(s));
					List<String> ficList = new ArrayList<String>();
					while (scanner.hasNextLine()) {
						String inFile = scanner.nextLine().trim();
						ficList.add(inFile);
						System.out.println("fichiers � controler :" + inFile);
					}

					// controle des fichiers rm71
					if (s.equalsIgnoreCase(propGatoProvionning.getProperty("prop.chkGatoFixeRm71XlsFileList").trim())) {
						boolean ko = false;

						outFileLog.println("D�but contr�le des fichiers attendu sur rm71 : " + returnStringDate());
						// nombre de fichiers attendu
						if (repFixRm71List.size() != ficList.size()) {
							outFileLog.println("Nombre de fichiers attendu incorrect! ");
							outFileLog.println("Nombre de fichiers attendu :  " + ficList.size());
							outFileLog.println("Nombre de fichiers re�u : " + repFixRm71List.size());

						} else {
							outFileLog.println("Nombre de fichiers attendu et re�u ok ");
						}

						// fichier absent

						for (int j = 0; j < ficList.size(); j++) {
							boolean exist = false;
							for (int k = 0; k < repFixRm71List.size(); k++) {
								if (ficList.get(j).equalsIgnoreCase(repFixRm71List.get(k))) {
									exist = true;
								}
							}
							if (!exist) {
								outFileLog.println("Fichier absent : " + ficList.get(j));
								ko = true;
							} else {
								outFileLog.println("Fichier pr�sent : " + ficList.get(j));
							}
						}
						outFileLog.println("Fin contr�le des fichiers attendu sur rm71 : " + returnStringDate());

						if (ko) {
							PrintWriter outKoFile = null;
							try {
								outKoFile = new PrintWriter(new FileWriter(koFile));
								outKoFile.println("fichier manquant");
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							outKoFile.close();
						}

					}

					// controle des fichiers rm21

					if (s.equalsIgnoreCase(propGatoProvionning.getProperty("prop.chkGatoFixeRm21XlsFileList").trim())) {
						boolean ko = false;

						outFileLog.println("D�but contr�le des fichiers attendu sur rm21 : " + returnStringDate());
						// nombre de fichiers attendu
						if (repFixRm21List.size() != ficList.size()) {
							outFileLog.println("Nombre de fichiers attendu incorrect! ");
							outFileLog.println("Nombre de fichiers attendu :  " + ficList.size());
							outFileLog.println("Nombre de fichiers re�u : " + repFixRm21List.size());

						} else {
							outFileLog.println("Nombre de fichiers attendu et re�u ok ");
						}

						// fichier absent

						for (int j = 0; j < ficList.size(); j++) {
							boolean exist = false;
							for (int k = 0; k < repFixRm21List.size(); k++) {
								if (ficList.get(j).equalsIgnoreCase(repFixRm21List.get(k))) {
									exist = true;
								}
							}
							if (!exist) {
								outFileLog.println("Fichier absent : " + ficList.get(j));
								ko = true;
							} else {
								outFileLog.println("Fichier pr�sent : " + ficList.get(j));
							}
						}
						outFileLog.println("Fin contr�le des fichiers attendu sur rm21 : " + returnStringDate());

						if (ko) {
							PrintWriter outKoFile = null;
							try {
								outKoFile = new PrintWriter(new FileWriter(koFile));
								outKoFile.println("fichier manquant");
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							outKoFile.close();
						}
					}

					// controle des fichiers mobile

					if (s.equalsIgnoreCase(propGatoProvionning.getProperty("prop.chkGatoMobileXlsFileList").trim())) {
						boolean ko = false;

						outFileLog.println("D�but contr�le des fichiers attendu sur gato_mobile : " + returnStringDate());
						// nombre de fichiers attendu
						if (repMobileList.size() != ficList.size()) {
							outFileLog.println("Nombre de fichiers attendu incorrect! ");
							outFileLog.println("Nombre de fichiers attendu :  " + ficList.size());
							outFileLog.println("Nombre de fichiers re�u : " + repMobileList.size());

						} else {
							outFileLog.println("Nombre de fichiers attendu et re�u ok ");
						}

						// fichier absent

						for (int j = 0; j < ficList.size(); j++) {
							boolean exist = false;
							for (int k = 0; k < repMobileList.size(); k++) {
								if (ficList.get(j).equalsIgnoreCase(repMobileList.get(k))) {
									exist = true;
								}
							}
							if (!exist) {
								outFileLog.println("Fichier absent : " + ficList.get(j));
								ko = true;
							} else {
								outFileLog.println("Fichier pr�sent : " + ficList.get(j));
							}
						}
						outFileLog.println("Fin contr�le des fichiers attendu sur gato_mobile : " + returnStringDate());

						if (ko) {
							PrintWriter outKoFile = null;
							try {
								outKoFile = new PrintWriter(new FileWriter(koFile));
								outKoFile.println("fichier manquant");
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							outKoFile.close();
						}
					}

					scanner.close();

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} finally {

			}

			// fin boucle
		}

		outFileLog.print("Fin du traitement  :   controle des fichiers : " + returnStringDate() + "\n");
		outFileLog.close();
	}

	public static List<String> listerRepertoire(File repertoire) {

		List<String> ls = new ArrayList<String>();
		String[] listefichiers;

		int i;
		listefichiers = repertoire.list();
		for (i = 0; i < listefichiers.length; i++) {
			if (listefichiers[i].endsWith(".xls") == true) {
				ls.add(listefichiers[i]);
				// System.out.println(listefichiers[i].substring(0,listefichiers[i].length()));
				// on choisit la sous chaine - les 4 derniers caracteres ".xls"
			}
		}

		return ls;
	}

	// ---------------------------------------------------------------------

	static Properties loadProperties() throws IOException {

		String propertiesPath = "C:/000-AI-DIR/provisionning/gatoDataProvisionning.properties";

		Properties properties = null;
		FileInputStream fileInputStream = new FileInputStream(propertiesPath);
		properties = new Properties();
		// logger.debug("Chargement des propri�t�s :" + propertiesPath);

		try {
			properties.load(fileInputStream);
			// logger.debug("fichier : " + propertiesPath);
		} catch (IOException e) {
			// logger.error("Erreur sur le chargement des propri�t�s "+
			// propertiesPath);
			throw e;
		} finally {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException ioe) {
					// logger.error("Erreur lors de la fermeture du fichier de propri�t�s "+
					// propertiesPath);
				}
			}
		}
		return properties;
	}

	private static String returnStringDate() {
		return new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
	}

}
