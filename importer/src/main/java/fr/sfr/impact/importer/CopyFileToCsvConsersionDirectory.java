package fr.sfr.impact.importer;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class CopyFileToCsvConsersionDirectory {

	protected static PrintWriter outFileLog = null;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/*
		 * avant de copier on controle la pr�sence des fichiers ko
		 */
		Properties propGatoProvionning = new Properties();
		try {
			propGatoProvionning = loadProperties();
		} catch (IOException e1) {
			e1.getMessage();
		}

		// fichier de log
		String logFilePath_out_LogFile = "";
		String propFile = propGatoProvionning.getProperty("prop.copy.downloadLogFile").trim();
		String propDir = propGatoProvionning.getProperty("prop.copy.downloadLogDir").trim();
		logFilePath_out_LogFile = propDir + returnStringDate() + "_" + propFile;
		try {
			outFileLog = new PrintWriter(new FileWriter(logFilePath_out_LogFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		outFileLog.print("D�but du traitement  :   copie des fichiers : Heure de D�but :" + returnStringDate() + "\n");

		// Repertoire des fichiers � copier
		String copyFileDirRm71 = propGatoProvionning.getProperty("prop.inFileDownloadDirRm71").trim();
		String copyFileDirRm21 = propGatoProvionning.getProperty("prop.inFileDownloadDirRm21").trim();
		String copyFileDirMobile = propGatoProvionning.getProperty("prop.inFileDownloadDirMobile").trim();

		// Liste des fichiers � copier
		String listFileDirRm71 = propGatoProvionning.getProperty("prop.chkGatoFixeRm71XlsFileList").trim();
		String listFileDirRm21 = propGatoProvionning.getProperty("prop.chkGatoFixeRm21XlsFileList").trim();
		String listFileDirMobile = propGatoProvionning.getProperty("prop.chkGatoMobileXlsFileList").trim();

		// Repertoire destination des fichiers copier
		String copyDestFileDirRm71 = propGatoProvionning.getProperty("prop.destDirConvertCsvRm71").trim();
		String copyDestFileDirRm21 = propGatoProvionning.getProperty("prop.destDirConvertCsvRm21").trim();
		String copyDestFileDirMobile = propGatoProvionning.getProperty("prop.destDirConvertCsvMobile").trim();

		// Fichier control ko
		String koFileRm71 = propGatoProvionning.getProperty("prop.koChkFileRm71").trim();
		String koFileRm21 = propGatoProvionning.getProperty("prop.koChkFileRm21").trim();
		String koFileMobile = propGatoProvionning.getProperty("prop.koChkFileRmMobile").trim();

		// on recherche les fichiers de controle
		List<String> repList = new ArrayList<String>();
		File t1 = new File(propGatoProvionning.getProperty("prop.chkDir").trim());
		repList = listerRepertoire(t1);

		boolean copyRm71 = true;
		boolean copyRm21 = true;
		boolean copyMobile = true;

		for (int i = 0; i < repList.size(); i++) {
			if (repList.get(i).trim().equalsIgnoreCase(koFileRm71)) {
				copyRm71 = false;
			}
			if (repList.get(i).trim().equalsIgnoreCase(koFileRm21)) {
				copyRm21 = false;
			}
			if (repList.get(i).trim().equalsIgnoreCase(koFileMobile)) {
				copyMobile = false;
			}
		}

		// copie des fichiers rm71
		if (copyRm71) {
			outFileLog.println("Copie des fichiers RM71");
			File rm71File = new File(listFileDirRm71);
			try {
				Scanner scanner;
				try {
					String s = listFileDirRm71;
					scanner = new Scanner(new File(s));

					while (scanner.hasNextLine()) {
						String fic = scanner.nextLine();
						outFileLog.println("Copie du fichier : " + fic);
						String fileToCopy = copyFileDirRm71 + fic;
						String fileDest = copyDestFileDirRm71 + fic;
						copyFile(fileToCopy, fileDest);
					}

					scanner.close();

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} finally {

			}

		} else {
			outFileLog.println("Pas de fichiers de RM71 � copier ...");
		}

		// copie des fichiers rm21
		if (copyRm21) {
			outFileLog.println("Copie des fichiers RM21");
			File rm21File = new File(listFileDirRm21);

			try {
				Scanner scanner;
				try {
					String s = listFileDirRm21;
					scanner = new Scanner(new File(s));

					while (scanner.hasNextLine()) {
						String fic = scanner.nextLine();
						outFileLog.println("Copie du fichier : " + fic);
						String fileToCopy = copyFileDirRm21 + fic;
						String fileDest = copyDestFileDirRm21 + fic;
						copyFile(fileToCopy, fileDest);
					}

					scanner.close();

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} finally {

			}

		} else {
			outFileLog.println("Pas de fichiers RM21 � copier ...");
		}

		// copie des fichiers mobile
		if (copyMobile) {
			outFileLog.println("Copie des fichiers mobile");
			File mobileFile = new File(listFileDirMobile);

			// Lecture de la liste des fichiers � copier

			try {
				Scanner scanner;
				try {
					String s = listFileDirMobile;
					scanner = new Scanner(new File(s));

					while (scanner.hasNextLine()) {
						String fic = scanner.nextLine();
						outFileLog.println("Copie du fichier : " + fic);
						String fileToCopy = copyFileDirMobile + fic;
						String fileDest = copyDestFileDirMobile + fic;
						copyFile(fileToCopy, fileDest);
					}

					scanner.close();

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} finally {

			}

		} else {
			outFileLog.println("Pas de fichiers MOBILE � copier ...");
		}

		outFileLog.print("Fin du traitement  :   copie des fichiers :" + returnStringDate() + "\n");
		outFileLog.close();
	}

	public static void copyFile(String fileToCopy, String fileDest) {

		InputStream input = null;
		OutputStream output = null;

		try {
			input = new FileInputStream(fileToCopy);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			output = new FileOutputStream(fileDest);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			IOUtils.copy(input, output);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static List<String> listerRepertoire(File repertoire) {

		List<String> ls = new ArrayList<String>();
		String[] listefichiers;

		int i;
		listefichiers = repertoire.list();
		for (i = 0; i < listefichiers.length; i++) {
			if (listefichiers[i].endsWith(".log") == true) {
				ls.add(listefichiers[i]);
				// System.out.println(listefichiers[i].substring(0,listefichiers[i].length()));
				// on choisit la sous chaine - les 4 derniers caracteres ".xls"
			}
		}

		return ls;
	}

	// ---------------------------------------------------------------------

	static Properties loadProperties() throws IOException {

		String propertiesPath = "C:/000-AI-DIR/provisionning/gatoDataProvisionning.properties";

		Properties properties = null;
		FileInputStream fileInputStream = new FileInputStream(propertiesPath);
		properties = new Properties();
		// logger.debug("Chargement des propri�t�s :" + propertiesPath);

		try {
			properties.load(fileInputStream);
			// logger.debug("fichier : " + propertiesPath);
		} catch (IOException e) {
			// logger.error("Erreur sur le chargement des propri�t�s "+
			// propertiesPath);
			throw e;
		} finally {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException ioe) {
					// logger.error("Erreur lors de la fermeture du fichier de propri�t�s "+
					// propertiesPath);
				}
			}
		}
		return properties;
	}

	private static String returnStringDate() {
		return new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
	}

}
