package fr.sfr.impact.importer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class ControlLinkDataInCSV {

	protected static PrintWriter out_LogCsvRelation = null;
	protected static int nbrLinkControled = 0;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// r�cup�ration des properties
		Properties propGatoProvionning = new Properties();
		try {
			propGatoProvionning = loadProperties();
		} catch (IOException e1) {
			e1.getMessage();
		}

		// Fichier control ko
		String koFileRm71 = propGatoProvionning.getProperty("prop.koChkFileRm71").trim();
		String koFileRm21 = propGatoProvionning.getProperty("prop.koChkFileRm21").trim();
		String koFileMobile = propGatoProvionning.getProperty("prop.koChkFileRmMobile").trim();

		// on recherche les fichiers de controle
		List<String> repList = new ArrayList<String>();
		File t1 = new File(propGatoProvionning.getProperty("prop.chkDir").trim());
		repList = listerRepertoire(t1, ".log");

		boolean trtRm71 = true;
		boolean trtRm21 = true;
		boolean trtMobile = true;

		for (int i = 0; i < repList.size(); i++) {
			if (repList.get(i).trim().equalsIgnoreCase(koFileRm71)) {
				trtRm71 = false;
			}
			if (repList.get(i).trim().equalsIgnoreCase(koFileRm21)) {
				trtRm21 = false;
			}
			if (repList.get(i).trim().equalsIgnoreCase(koFileMobile)) {
				trtMobile = false;
			}
		}

		List<String> repListRm71 = new ArrayList<String>();
		File t1Rm71 = new File(propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVdirPathRm71").trim());
		repListRm71 = listerRepertoire(t1Rm71, ".csv");

		List<String> repListRm21 = new ArrayList<String>();
		File t1Rm21 = new File(propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVdirPathRm21").trim());
		repListRm21 = listerRepertoire(t1Rm21, ".csv");

		List<String> repListMobile = new ArrayList<String>();
		File t1Mobile = new File(propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVdirPathMobile").trim());
		repListMobile = listerRepertoire(t1Mobile, ".csv");

		if (trtRm71) {

			String logFilePath_out_LogCsvRelation = "";
			int nStart = 0;
			int nEnd = 0;
			int nbrTrt = 0;
			String namePrefixe = "";
			String suffixe = "";
			String filesDirectory = "";

			logFilePath_out_LogCsvRelation = propGatoProvionning.getProperty("prop.logFilePathoutLogCsvRelationRm71").trim();
			nStart = Integer.parseInt(propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVnStart").trim());
			nEnd = repListRm71.size() - 1;
			nbrTrt = Integer.parseInt(propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVnbrTrt").trim());
			namePrefixe = propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVnamePrefixe").trim();
			suffixe = propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVnameSuffixe").trim();
			filesDirectory = propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVdirPathRm71").trim();

			controleAllNodesLinksInCSV(logFilePath_out_LogCsvRelation, nStart, nEnd, nbrTrt, namePrefixe, suffixe, filesDirectory);
		}
		if (trtRm21) {
			String logFilePath_out_LogCsvRelation = "";
			int nStart = 0;
			int nEnd = 0;
			int nbrTrt = 0;
			String namePrefixe = "";
			String suffixe = "";
			String filesDirectory = "";

			logFilePath_out_LogCsvRelation = propGatoProvionning.getProperty("prop.logFilePathoutLogCsvRelationRm21").trim();
			nStart = Integer.parseInt(propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVnStart").trim());
			nEnd = repListRm21.size() - 1;
			nbrTrt = Integer.parseInt(propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVnbrTrt").trim());
			namePrefixe = propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVnamePrefixe").trim();
			suffixe = propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVnameSuffixe").trim();
			filesDirectory = propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVdirPathRm21").trim();

			controleAllNodesLinksInCSV(logFilePath_out_LogCsvRelation, nStart, nEnd, nbrTrt, namePrefixe, suffixe, filesDirectory);

		}
		if (trtMobile) {
			String logFilePath_out_LogCsvRelation = "";
			int nStart = 0;
			int nEnd = 0;
			int nbrTrt = 0;
			String namePrefixe = "";
			String suffixe = "";
			String filesDirectory = "";

			logFilePath_out_LogCsvRelation = propGatoProvionning.getProperty("prop.logFilePathoutLogCsvRelationMobile").trim();
			nStart = Integer.parseInt(propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVnStart").trim());
			nEnd = repListMobile.size() - 1;
			nbrTrt = Integer.parseInt(propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVnbrTrt").trim());
			namePrefixe = propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVnamePrefixe").trim();
			suffixe = propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVnameSuffixe").trim();
			filesDirectory = propGatoProvionning.getProperty("prop.controleAllNodesLinksInCSVdirPathMobile").trim();

			controleAllNodesLinksInCSV(logFilePath_out_LogCsvRelation, nStart, nEnd, nbrTrt, namePrefixe, suffixe, filesDirectory);

		}

	}

	public static void controleAllNodesLinksInCSV(String logFilePath_out_LogCsvRelation, int nStart, int nEnd, int nbrTrt, String namePrefixe, String surfixe, String filesDirectory) {

		try {
			out_LogCsvRelation = new PrintWriter(new FileWriter(logFilePath_out_LogCsvRelation));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String heureDebut = returnStringDate();
		out_LogCsvRelation.print("D�but du traitement : Contr�le des relations dans le fichier CSV : Heure de D�but :" + heureDebut + "\n");
		System.out.println("D�but du traitement : Contr�le des relations dans le fichier CSV : Heure de D�but :" + heureDebut + "\n");
		List mNodeLinkListeToCreate = new ArrayList();

		for (int i = nStart; i <= nEnd; i++) {
			String filePath = filesDirectory + namePrefixe + i + surfixe;

			try {
				Scanner scanner;
				try {
					scanner = new Scanner(new File(filePath));

					while (scanner.hasNextLine()) {

						String line = scanner.nextLine();
						mNodeLinkListeToCreate.add(line);
						if (mNodeLinkListeToCreate.size() == nbrTrt) {
							controleNodesRelationsShipInCSV(mNodeLinkListeToCreate);
							mNodeLinkListeToCreate = new ArrayList();
						}
					}

					out_LogCsvRelation.print("Dernier fichier trait� :" + filePath + "\n");
					scanner.close();

				} catch (FileNotFoundException e) {

					out_LogCsvRelation.print("Fichier des relations � contr�l�es inexistant :" + filePath + "\n");
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} finally {

			}

		}
		out_LogCsvRelation.print("Fin du traitement : Contr�le des relations dans le fichier CSV  : Heure de D�but :" + heureDebut + "\n");
		out_LogCsvRelation.print("Fin du traitement : Contr�le des relations dans le fichier CSV  : Heure de Fin :" + returnStringDate() + "\n");
		System.out.println("Fin du traitement : Contr�le des relations dans le fichier CSV  : Heure de Fin :" + returnStringDate() + "\n");
		out_LogCsvRelation.close();
	}

	// creation des relations dans la base

	private static void controleNodesRelationsShipInCSV(List mNodeRelationShipListeToCreate) {

		try {

			for (int i = 0; i < mNodeRelationShipListeToCreate.size(); i++) {
				String line = mNodeRelationShipListeToCreate.get(i).toString();
				String cols[] = line.split(";");

				// Contr�le du nombre de colonnes
				if (cols.length >= 3) {
					// out_LogCsvRelation.print("OK : 3 champs : line : " + line
					// +"\n");
				} else {
					out_LogCsvRelation.print("KO : pas 3 champss : line : " + line + "\n");
				}

				if (cols[0].trim() != null && cols[1].trim() != null && cols[2].trim() != null) {
				} else {
					out_LogCsvRelation.print("KO : ID_SOURCE, RELATION et ID_DESTINATION : " + line + "\n");
				}
				if (cols[0].trim().equalsIgnoreCase("") || cols[1].trim().equalsIgnoreCase("") || cols[2].trim().equalsIgnoreCase("")) {
					out_LogCsvRelation.print("KO : ID_SOURCE, RELATION et ID_DESTINATION : line : " + line + "\n");
				} else {
					// out_LogCsvRelation.print("OK : ID_SOURCE, RELATION et ID_DESTINATION : "
					// + line +"\n");
				}

				if (!cols[1].trim().equalsIgnoreCase("EST_AFFECTE_A") && !cols[1].trim().equalsIgnoreCase("EST_COMPOSE_DE") && !cols[1].trim().equalsIgnoreCase("EST_CONNECTE_A")
						&& !cols[1].trim().equalsIgnoreCase("EST_INCLUS_DANS")

				) {
					out_LogCsvRelation.print("KO : TYPE RELATION INCORRECT  : line : " + line + "\n");
				} else {
					// out_LogCsvRelation.print("OK : TYPE RELATION CORRECT : "
					// + line +"\n");
				}
				nbrLinkControled = nbrLinkControled + 1;
			}

		} finally {

			//
		}
		System.out.println("Nombre de liens control�s en tout :" + nbrLinkControled + " Le : " + returnStringDate() + "\n");
		out_LogCsvRelation.print("Nombre de liens control�s en tout :" + nbrLinkControled + " Le : " + returnStringDate() + "\n");
	}

	private static String returnStringDate() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}

	static Properties loadProperties() throws IOException {

		String propertiesPath = "C:/000-AI-DIR/provisionning/gatoDataProvisionning.properties";

		Properties properties = null;
		FileInputStream fileInputStream = new FileInputStream(propertiesPath);
		properties = new Properties();
		// logger.debug("Chargement des propri�t�s :" + propertiesPath);

		try {
			properties.load(fileInputStream);
			// logger.debug("fichier : " + propertiesPath);
		} catch (IOException e) {
			// logger.error("Erreur sur le chargement des propri�t�s "+
			// propertiesPath);
			throw e;
		} finally {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException ioe) {
					// logger.error("Erreur lors de la fermeture du fichier de propri�t�s "+
					// propertiesPath);
				}
			}
		}
		return properties;
	}

	public static List<String> listerRepertoire(File repertoire, String suffixe) {

		List<String> ls = new ArrayList<String>();
		String[] listefichiers;

		int i;
		listefichiers = repertoire.list();
		for (i = 0; i < listefichiers.length; i++) {
			if (listefichiers[i].endsWith(suffixe) == true) {
				ls.add(listefichiers[i]);
				// System.out.println(listefichiers[i].substring(0,listefichiers[i].length()));
				// on choisit la sous chaine - les 4 derniers caracteres ".xls"
			}
		}

		return ls;
	}

}
