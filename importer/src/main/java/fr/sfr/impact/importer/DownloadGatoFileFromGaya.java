package fr.sfr.impact.importer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class DownloadGatoFileFromGaya {

	protected static PrintWriter out_LogDownloadFile = null;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// r�cup�ration des properties

		Properties propGatoProvionning = new Properties();
		try {
			propGatoProvionning = loadProperties();
		} catch (IOException e1) {
			e1.getMessage();
		}

		// fichier de log
		String logFilePath_out_LogDownloadFile = "";
		String propFile = propGatoProvionning.getProperty("prop.downloadLogFile").trim();
		String propDir = propGatoProvionning.getProperty("prop.downloadLogDir").trim();
		logFilePath_out_LogDownloadFile = propDir + returnStringDate() + "_" + propFile;
		try {
			out_LogDownloadFile = new PrintWriter(new FileWriter(logFilePath_out_LogDownloadFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		out_LogDownloadFile.print("D�but du traitement :   download des fichiers : Heure de D�but :" + returnStringDate() + "\n");

		List<String> l = new ArrayList<String>();
		String s1 = propGatoProvionning.getProperty("prop.inFileDownloadDirRm71").trim() + ";" + propGatoProvionning.getProperty("prop.listInFileDownloadGatoFixeRm71").trim();
		String s2 = propGatoProvionning.getProperty("prop.inFileDownloadDirRm21").trim() + ";" + propGatoProvionning.getProperty("prop.listInFileDownloadGatoFixeRm21").trim();
		String s3 = propGatoProvionning.getProperty("prop.inFileDownloadDirMobile").trim() + ";" + propGatoProvionning.getProperty("prop.listInFileDownloadGatoMobile").trim();

		l.add(s1);
		l.add(s2);
		l.add(s3);

		for (int i = 0; i < l.size(); i++)
		// D�but boucle download en fonction des r�f�rentiels gato-fixe rm21,
		// rm71, et gato-mobile
		{
			String[] ts = l.get(i).split(";");
			String outFileDir = "";
			String inFileList = "";

			outFileDir = ts[0];
			inFileList = ts[1];

			// Lecture de la liste des fichiers � t�l�charger

			try {
				Scanner scanner;
				try {
					String s = inFileList;
					scanner = new Scanner(new File(s));

					while (scanner.hasNextLine()) {
						String inFileUrlDownload = scanner.nextLine();
						inFileUrlDownload = inFileUrlDownload.trim();

						String[] t = inFileUrlDownload.split("/");
						String name = t[t.length - 1];

						String adresse = inFileUrlDownload;
						File dest = new File(outFileDir + name);

						downloadFile(adresse, dest);
					}

					scanner.close();

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} finally {

			}

			// fin boucle download en fonction des r�f�rentiels gato-fixe rm21
			// rm71 et gato-mobile
		}
		out_LogDownloadFile.print("Fin du traitement :   download des fichiers : " + returnStringDate() + "\n");
		out_LogDownloadFile.close();
	}

	public static void downloadFile(String adresse, File dest) {

		out_LogDownloadFile.println("T�l�chargement encours : " + adresse);
		BufferedReader reader = null;
		FileOutputStream fos = null;
		InputStream in = null;
		try {
			// cr�ation de la connection
			URL url = new URL(adresse);
			URLConnection conn = url.openConnection();
			System.out.println(adresse);
			String FileType = "";
			try {
				FileType = conn.getContentType();
				System.out.println("FileType : " + FileType);
				int FileLenght = conn.getContentLength();
				if (FileLenght == -1) {
					throw new IOException("Fichier non valide.");
				}
				// lecture de la r�ponse
				in = conn.getInputStream();
				reader = new BufferedReader(new InputStreamReader(in));
				if (dest == null) {
					String FileName = url.getFile();
					FileName = FileName.substring(FileName.lastIndexOf('/') + 1);
					dest = new File(FileName);
				}
				fos = new FileOutputStream(dest);
				byte[] buff = new byte[1024];
				int l = in.read(buff);
				while (l > 0) {
					fos.write(buff, 0, l);
					l = in.read(buff);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Fichier inexistent!");
				out_LogDownloadFile.println("T�l�chargement ko sur :" + adresse);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			if (fos != null) {
				try {
					fos.flush();
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

				//
			}
			try {
				reader.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	static Properties loadProperties() throws IOException {

		String propertiesPath = "C:/000-AI-DIR/provisionning/gatoDataProvisionning.properties";

		Properties properties = null;
		FileInputStream fileInputStream = new FileInputStream(propertiesPath);
		properties = new Properties();
		// logger.debug("Chargement des propri�t�s :" + propertiesPath);

		try {
			properties.load(fileInputStream);
			// logger.debug("fichier : " + propertiesPath);
		} catch (IOException e) {
			// logger.error("Erreur sur le chargement des propri�t�s "+
			// propertiesPath);
			throw e;
		} finally {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException ioe) {
					// logger.error("Erreur lors de la fermeture du fichier de propri�t�s "+
					// propertiesPath);
				}
			}
		}
		return properties;
	}

	private static String returnStringDate() {
		return new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
	}

}
