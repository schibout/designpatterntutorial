package fr.sfr.impact.importer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class ControlNodeDataInCSV {

	protected static PrintWriter out_LogCsvNoeuds = null;
	protected static int nbrNodeControled = 0;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// r�cup�ration des properties
		Properties propGatoProvionning = new Properties();
		try {
			propGatoProvionning = loadProperties();
		} catch (IOException e1) {
			e1.getMessage();
		}

		// Fichier control ko
		String koFileRm71 = propGatoProvionning.getProperty("prop.koChkFileRm71").trim();
		String koFileRm21 = propGatoProvionning.getProperty("prop.koChkFileRm21").trim();
		String koFileMobile = propGatoProvionning.getProperty("prop.koChkFileRmMobile").trim();

		// on recherche les fichiers de controle
		List<String> repList = new ArrayList<String>();
		File t1 = new File(propGatoProvionning.getProperty("prop.chkDir").trim());
		repList = listerRepertoire(t1, ".log");

		boolean trtRm71 = true;
		boolean trtRm21 = true;
		boolean trtMobile = true;

		for (int i = 0; i < repList.size(); i++) {
			if (repList.get(i).trim().equalsIgnoreCase(koFileRm71)) {
				trtRm71 = false;
			}
			if (repList.get(i).trim().equalsIgnoreCase(koFileRm21)) {
				trtRm21 = false;
			}
			if (repList.get(i).trim().equalsIgnoreCase(koFileMobile)) {
				trtMobile = false;
			}
		}

		List<String> repListRm71 = new ArrayList<String>();
		File t1Rm71 = new File(propGatoProvionning.getProperty("prop.controleAllNodesInCSVdirPathRm71").trim());
		repListRm71 = listerRepertoire(t1Rm71, ".csv");

		List<String> repListRm21 = new ArrayList<String>();
		File t1Rm21 = new File(propGatoProvionning.getProperty("prop.controleAllNodesInCSVdirPathRm21").trim());
		repListRm21 = listerRepertoire(t1Rm21, ".csv");

		List<String> repListMobile = new ArrayList<String>();
		File t1Mobile = new File(propGatoProvionning.getProperty("prop.controleAllNodesInCSVdirPathMobile").trim());
		repListMobile = listerRepertoire(t1Mobile, ".csv");

		if (trtRm71) {

			String logFilePath_out_LogCsvNoeuds = "";
			int nStart = 0;
			int nEnd = 0;
			int nbrTrt = 0;
			String namePrefixe = "";
			String suffixe = "";
			String filesDirectory = "";

			logFilePath_out_LogCsvNoeuds = propGatoProvionning.getProperty("prop.logFilePathoutLogCsvNoeudsRm71").trim();
			nStart = Integer.parseInt(propGatoProvionning.getProperty("prop.controleAllNodesInCSVnStart").trim());
			nEnd = repListRm71.size() - 1;
			nbrTrt = Integer.parseInt(propGatoProvionning.getProperty("prop.controleAllNodesInCSVnbrTrt").trim());
			namePrefixe = propGatoProvionning.getProperty("prop.controleAllNodesInCSVnamePrefixe").trim();
			suffixe = propGatoProvionning.getProperty("prop.controleAllNodesInCSVnameSuffixe").trim();
			filesDirectory = propGatoProvionning.getProperty("prop.controleAllNodesInCSVdirPathRm71").trim();

			controleAllNodesInCSV(logFilePath_out_LogCsvNoeuds, nStart, nEnd, nbrTrt, namePrefixe, suffixe, filesDirectory);
		}
		if (trtRm21) {
			String logFilePath_out_LogCsvNoeuds = "";
			int nStart = 0;
			int nEnd = 0;
			int nbrTrt = 0;
			String namePrefixe = "";
			String suffixe = "";
			String filesDirectory = "";

			logFilePath_out_LogCsvNoeuds = propGatoProvionning.getProperty("prop.logFilePathoutLogCsvNoeudsRm21").trim();
			nStart = Integer.parseInt(propGatoProvionning.getProperty("prop.controleAllNodesInCSVnStart").trim());
			nEnd = repListRm21.size() - 1;
			nbrTrt = Integer.parseInt(propGatoProvionning.getProperty("prop.controleAllNodesInCSVnbrTrt").trim());
			namePrefixe = propGatoProvionning.getProperty("prop.controleAllNodesInCSVnamePrefixe").trim();
			suffixe = propGatoProvionning.getProperty("prop.controleAllNodesInCSVnameSuffixe").trim();
			filesDirectory = propGatoProvionning.getProperty("prop.controleAllNodesInCSVdirPathRm21").trim();

			controleAllNodesInCSV(logFilePath_out_LogCsvNoeuds, nStart, nEnd, nbrTrt, namePrefixe, suffixe, filesDirectory);

		}
		if (trtMobile) {
			String logFilePath_out_LogCsvNoeuds = "";
			int nStart = 0;
			int nEnd = 0;
			int nbrTrt = 0;
			String namePrefixe = "";
			String suffixe = "";
			String filesDirectory = "";

			logFilePath_out_LogCsvNoeuds = propGatoProvionning.getProperty("prop.logFilePathoutLogCsvNoeudsMobile").trim();
			nStart = Integer.parseInt(propGatoProvionning.getProperty("prop.controleAllNodesInCSVnStart").trim());
			nEnd = repListMobile.size() - 1;
			nbrTrt = Integer.parseInt(propGatoProvionning.getProperty("prop.controleAllNodesInCSVnbrTrt").trim());
			namePrefixe = propGatoProvionning.getProperty("prop.controleAllNodesInCSVnamePrefixe").trim();
			suffixe = propGatoProvionning.getProperty("prop.controleAllNodesInCSVnameSuffixe").trim();
			filesDirectory = propGatoProvionning.getProperty("prop.controleAllNodesInCSVdirPathMobile").trim();

			controleAllNodesInCSV(logFilePath_out_LogCsvNoeuds, nStart, nEnd, nbrTrt, namePrefixe, suffixe, filesDirectory);

		}

	}

	public static void controleAllNodesInCSV(String logFilePath_out_LogCsvNoeuds, int nStart, int nEnd, int nbrTrt, String namePrefixe, String suffixe, String filesDirectory) {
		try {
			out_LogCsvNoeuds = new PrintWriter(new FileWriter(logFilePath_out_LogCsvNoeuds));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String heureDebut = returnStringDate();
		out_LogCsvNoeuds.print("D�but du traitement : Contr�le des noeuds dans le fichier CSV : Heure de D�but :" + heureDebut + "\n");
		System.out.println("D�but du traitement : Contr�le des noeuds dans le fichier CSV : Heure de D�but :" + heureDebut + "\n");
		List mNodeListeToCreate = new ArrayList();

		for (int i = nStart; i <= nEnd; i++) {
			String filePath = filesDirectory + namePrefixe + i + suffixe;

			try {
				Scanner scanner;
				try {
					scanner = new Scanner(new File(filePath));

					while (scanner.hasNextLine()) {

						String line = scanner.nextLine();
						mNodeListeToCreate.add(line);
						if (mNodeListeToCreate.size() == nbrTrt) {
							controleNodesInCSV(mNodeListeToCreate);
							mNodeListeToCreate = new ArrayList();
						}
					}
					out_LogCsvNoeuds.print("Dernier fichier trait� :" + filePath + "\n");
					scanner.close();

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} finally {

			}

		}
		out_LogCsvNoeuds.print("Fin du traitement : Contr�le des noeuds dans le fichier CSV : Heure de D�but :" + heureDebut + "\n");
		out_LogCsvNoeuds.print("Fin du traitement : Contr�le des noeuds dans le fichier CSV : Heure de Fin :" + returnStringDate() + "\n");
		System.out.println("Fin du traitement : Contr�le des noeuds dans le fichier CSV : Heure de Fin :" + returnStringDate() + "\n");

		out_LogCsvNoeuds.close();
	}

	public static void controleNodesInCSV(List mNodeListeToCreate) {

		try {

			for (int i = 0; i < mNodeListeToCreate.size(); i++) {
				String line = mNodeListeToCreate.get(i).toString();
				String cols[] = line.split(";");
				// v�rification de la pr�sence des 9 attributs
				if (cols.length >= 9) {
					// out_LogCsvNoeuds.print("OK : 9 attributs : line : " +
					// line +"\n");

					// v�rification de la pr�sence des 4 attributs non vide
					// ID, NAME,LEVEL,TYPE
					if (cols[0].trim() != null && cols[1].trim() != null && cols[2].trim() != null && cols[3].trim() != null) {
						// out_LogCsvNoeuds.print("OK : 4 principaux attributs : line : "
						// + line +"\n");
					} else {
						out_LogCsvNoeuds.print("KO : les 4 attributs ID, NAME,TYPE, LEVEL doivent etre non vide : line : " + line + "\n");
					}

				} else {
					out_LogCsvNoeuds.print("KO : pas 9 attributs : line : " + line + "\n");
				}

				nbrNodeControled = nbrNodeControled + 1;
			}

			//
		} finally {

			//
		}
		System.out.println("Nombre de noeuds contr�l�s en tout :" + nbrNodeControled + "\n");
		out_LogCsvNoeuds.print("Nombre de noeuds contr�l�s en tout :" + nbrNodeControled + "\n");
	}

	private static String returnStringDate() {

		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}

	static Properties loadProperties() throws IOException {

		String propertiesPath = "C:/000-AI-DIR/provisionning/gatoDataProvisionning.properties";

		Properties properties = null;
		FileInputStream fileInputStream = new FileInputStream(propertiesPath);
		properties = new Properties();
		// logger.debug("Chargement des propri�t�s :" + propertiesPath);

		try {
			properties.load(fileInputStream);
			// logger.debug("fichier : " + propertiesPath);
		} catch (IOException e) {
			// logger.error("Erreur sur le chargement des propri�t�s "+
			// propertiesPath);
			throw e;
		} finally {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException ioe) {
					// logger.error("Erreur lors de la fermeture du fichier de propri�t�s "+
					// propertiesPath);
				}
			}
		}
		return properties;
	}

	public static List<String> listerRepertoire(File repertoire, String suffixe) {

		List<String> ls = new ArrayList<String>();
		String[] listefichiers;

		int i;
		listefichiers = repertoire.list();
		for (i = 0; i < listefichiers.length; i++) {
			if (listefichiers[i].endsWith(suffixe) == true) {
				ls.add(listefichiers[i]);
				// System.out.println(listefichiers[i].substring(0,listefichiers[i].length()));
				// on choisit la sous chaine - les 4 derniers caracteres ".xls"
			}
		}

		return ls;
	}
}