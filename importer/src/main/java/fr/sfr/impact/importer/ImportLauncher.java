package fr.sfr.impact.importer;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public final class ImportLauncher {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("importer.xml", "database-importer.xml");
        context.registerShutdownHook();

        context.getBean(NodeImporter.class).importData();
        context.getBean(RelationshipImporter.class).importData();
        context.getBean(RelationshipPropertiesImporter.class).importData();
    }
}
