package fr.sfr.impact.importer;

import au.com.bytecode.opencsv.CSVReader;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class ConvertCsvFileToCsvFile {

	protected static PrintWriter outFileLog = null;

	public static void main(String[] args) {

		// r�cup�ration des properties
		Properties propGatoProvionning = new Properties();
		try {
			propGatoProvionning = loadProperties();
		} catch (IOException e1) {
			e1.getMessage();
		}

		// fichier de log
		String logFilePath_out_LogFile = "";
		String propFile = propGatoProvionning.getProperty("prop.convert.File").trim();
		String propDir = propGatoProvionning.getProperty("prop.convert.FileDir").trim();
		logFilePath_out_LogFile = propDir + returnStringDate() + "_" + propFile;
		try {
			outFileLog = new PrintWriter(new FileWriter(logFilePath_out_LogFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		outFileLog.print("D�but du traitement : conversion des fichiers xls : Heure de D�but :" + returnStringDate() + "\n");

		// Fichier control ko
		String koFileRm71 = propGatoProvionning.getProperty("prop.koChkFileRm71").trim();
		String koFileRm21 = propGatoProvionning.getProperty("prop.koChkFileRm21").trim();
		String koFileMobile = propGatoProvionning.getProperty("prop.koChkFileRmMobile").trim();

		// on recherche les fichiers de controle
		List<String> repList = new ArrayList<String>();
		File t1 = new File(propGatoProvionning.getProperty("prop.chkDir").trim());
		repList = listerRepertoire(t1);

		boolean trtRm71 = true;
		boolean trtRm21 = true;
		boolean trtMobile = true;

		for (int i = 0; i < repList.size(); i++) {
			if (repList.get(i).trim().equalsIgnoreCase(koFileRm71)) {
				trtRm71 = false;
				outFileLog.println("Pas de fichiers RM71 � traiter ...");
			}
			if (repList.get(i).trim().equalsIgnoreCase(koFileRm21)) {
				trtRm21 = false;
				outFileLog.println("Pas de fichiers RM21 � traiter ...");
			}
			if (repList.get(i).trim().equalsIgnoreCase(koFileMobile)) {
				trtMobile = false;
				outFileLog.println("Pas de fichiers MOBILE � traiter ...");
			}
		}

		// r�cup�ration des properties

		String outSuffixe = propGatoProvionning.getProperty("prop.outSuffixe").trim();
		// rm71
		if (trtRm71) {
			outFileLog.println("Traitement des fichiers RM71 ...");
			String inFileDirRm71 = propGatoProvionning.getProperty("prop.inFileDirRm71").trim();
			String outFileDirRm71 = propGatoProvionning.getProperty("prop.outFileDirRm71").trim();
			String inFileListRm71 = propGatoProvionning.getProperty("prop.rm71ListInFile").trim();

			// on lance la conversion
			appelTraitementFichier(inFileDirRm71, outFileDirRm71, inFileListRm71, outSuffixe);

		}

		// rm21
		if (trtRm21) {
			outFileLog.println("Traitement des fichiers RM21 ...");
			String inFileDirRm21 = propGatoProvionning.getProperty("prop.inFileDirRm21").trim();
			String outFileDirRm21 = propGatoProvionning.getProperty("prop.outFileDirRm21").trim();
			String inFileListRm21 = propGatoProvionning.getProperty("prop.rm21ListInFile").trim();

			appelTraitementFichier(inFileDirRm21, outFileDirRm21, inFileListRm21, outSuffixe);
		}

		// mobile
		if (trtMobile) {
			outFileLog.println("Traitement des fichiers MOBILE ...");
			String inFileDirMobile = propGatoProvionning.getProperty("prop.inFileDirMobile").trim();
			String outFileDirMobile = propGatoProvionning.getProperty("prop.outFileDirMobile").trim();
			String inFileListMobile = propGatoProvionning.getProperty("prop.mobileListInFile").trim();

			appelTraitementFichier(inFileDirMobile, outFileDirMobile, inFileListMobile, outSuffixe);

		}
		outFileLog.print("Fin du traitement  :   conversion des fichiers xls :" + returnStringDate() + "\n");
		outFileLog.close();
	}

	// ---------------------------------------------------------------------

	public static void appelTraitementFichier(String inFileDir, String outFileDir, String inFileList, String outSuffixe) {

		// Lecture de la liste des fichiers � traiter

		try {
			Scanner scanner;
			try {
				String s = inFileList;
				scanner = new Scanner(new File(s));

				while (scanner.hasNextLine()) {
					String inFile = scanner.nextLine();
					inFile = inFile.trim();

					outFileLog.println("Traitement du fichier:" + inFile);

					String outFile = inFile.toLowerCase();
					outFile = outFile.replace(".xls", outSuffixe);

					lectureEcriture(inFileDir + inFile, outFileDir + outFile);

				}

				scanner.close();

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} finally {

		}

	}

	public static void lectureEcriture(String inFile, String outFile) {

		PrintWriter outCsvFile = null;
		try {
			outCsvFile = new PrintWriter(new FileWriter(outFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		CSVReader reader = null;

		try {
			reader = new CSVReader(new FileReader(inFile), '\t');
			try {
				// myEntries = reader.readAll();
				String[] nextLine;
				while ((nextLine = reader.readNext()) != null) {
					// System.out.println(nextLine[0]
					// +";"+nextLine[1]+";"+nextLine[2] +";"+nextLine[3]);
					String s = "startline";
					for (int i = 0; i < nextLine.length; i++) {
						String el = nextLine[i].trim();
						// if (el.equalsIgnoreCase("")) {
						// el = "-";
						// }
						s = s + ";" + el;
					}
					s = s.replace("startline;", "").trim();
					// System.out.println(s);
					outCsvFile.println(s);
				}

				// myEntries.add(reader.)
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		outCsvFile.close();

	}

	public static List<String> listerRepertoire(File repertoire) {

		List<String> ls = new ArrayList<String>();
		String[] listefichiers;

		int i;
		listefichiers = repertoire.list();
		for (i = 0; i < listefichiers.length; i++) {
			if (listefichiers[i].endsWith(".log") == true) {
				ls.add(listefichiers[i]);
				// System.out.println(listefichiers[i].substring(0,listefichiers[i].length()));
				// on choisit la sous chaine - les 4 derniers caracteres ".xls"
			}
		}

		return ls;
	}

	// ---------------------------------------------------------------------

	static Properties loadProperties() throws IOException {

		String propertiesPath = "C:/000-AI-DIR/provisionning/gatoDataProvisionning.properties";

		Properties properties = null;
		FileInputStream fileInputStream = new FileInputStream(propertiesPath);
		properties = new Properties();
		// logger.debug("Chargement des propri�t�s :" + propertiesPath);

		try {
			properties.load(fileInputStream);
			// logger.debug("fichier : " + propertiesPath);
		} catch (IOException e) {
			// logger.error("Erreur sur le chargement des propri�t�s "+
			// propertiesPath);
			throw e;
		} finally {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException ioe) {
					// logger.error("Erreur lors de la fermeture du fichier de propri�t�s "+
					// propertiesPath);
				}
			}
		}
		return properties;
	}

	private static String returnStringDate() {
		return new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
	}

}