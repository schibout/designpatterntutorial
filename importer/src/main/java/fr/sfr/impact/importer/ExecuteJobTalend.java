package fr.sfr.impact.importer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class ExecuteJobTalend {

	/**
	 * @param args
	 */
	protected static PrintWriter outFileLog = null;

	public static void main(String[] args) {

		System.out.println(args[0]);
		// r�cup�ration des properties
		Properties propGatoProvionning = new Properties();
		try {
			propGatoProvionning = loadProperties();
		} catch (IOException e1) {
			e1.getMessage();
		}

		// fichier de log
		String logFilePath_out_LogFile = "";
		String propFile = propGatoProvionning.getProperty("prop.execute.talend.File").trim();
		String propDir = propGatoProvionning.getProperty("prop.execute.talend.FileDir").trim();
		logFilePath_out_LogFile = propDir + returnStringDate() + "_" + propFile;
		try {
			outFileLog = new PrintWriter(new FileWriter(logFilePath_out_LogFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		outFileLog.print("D�but du traitement : Ex�cution du job Talend : Heure de D�but :" + returnStringDate() + "\n");

		// on recherche les fichiers de controle
		List<String> repList = new ArrayList<String>();
		File t1 = new File(propGatoProvionning.getProperty("prop.chkDir").trim());
		repList = listerRepertoire(t1);

		// job talend
		String jobTalend = propGatoProvionning.getProperty("prop.job.talend").trim();

		String koFileRm71 = propGatoProvionning.getProperty("prop.koChkFileRm71").trim();
		if (args[0].equalsIgnoreCase(koFileRm71)) {
			System.out.println("jle ----rm71");
			boolean trtRm71 = true;
			for (int i = 0; i < repList.size(); i++) {
				if (repList.get(i).trim().equalsIgnoreCase(koFileRm71)) {
					trtRm71 = false;
					outFileLog.println("Pas de fichiers RM71 � traiter ...");
				}
			}
			if (trtRm71) {
				outFileLog.println("Traitement des fichiers RM71 ...");

				// on lance le jobTalend
				try {
					Runtime.getRuntime().exec(jobTalend);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		String koFileRm21 = propGatoProvionning.getProperty("prop.koChkFileRm21").trim();
		if (args[0].equalsIgnoreCase(koFileRm21)) {
			System.out.println("jle ----rm21");
			boolean trtRm21 = true;
			for (int i = 0; i < repList.size(); i++) {
				if (repList.get(i).trim().equalsIgnoreCase(koFileRm21)) {
					trtRm21 = false;
					outFileLog.println("Pas de fichiers RM21 � traiter ...");
				}
			}
			// rm21
			if (trtRm21) {
				outFileLog.println("Traitement des fichiers RM21 ...");

				// on lance le jobTalend
				try {
					Runtime.getRuntime().exec(jobTalend);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		String koFileMobile = propGatoProvionning.getProperty("prop.koChkFileRmMobile").trim();

		if (args[0].equalsIgnoreCase(koFileMobile)) {
			System.out.println("jle ----Mobile");
			boolean trtMobile = true;
			for (int i = 0; i < repList.size(); i++) {

				if (repList.get(i).trim().equalsIgnoreCase(koFileMobile)) {
					trtMobile = false;
					outFileLog.println("Pas de fichiers MOBILE � traiter ...");
				}
			}

			// mobile
			if (trtMobile) {
				outFileLog.println("Traitement des fichiers MOBILE ...");
				// on lance le jobTalend
				try {
					Runtime.getRuntime().exec(jobTalend);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		outFileLog.print("Fin du traitement  : Ex�cution du job Talend :" + returnStringDate() + "\n");
		outFileLog.close();
	}

	// ---------------------------------------------------------------------

	public static List<String> listerRepertoire(File repertoire) {

		List<String> ls = new ArrayList<String>();
		String[] listefichiers;

		int i;
		listefichiers = repertoire.list();
		for (i = 0; i < listefichiers.length; i++) {
			if (listefichiers[i].endsWith(".log") == true) {
				ls.add(listefichiers[i]);
				// System.out.println(listefichiers[i].substring(0,listefichiers[i].length()));
				// on choisit la sous chaine - les 4 derniers caracteres ".xls"
			}
		}

		return ls;
	}

	// ---------------------------------------------------------------------

	static Properties loadProperties() throws IOException {

		String propertiesPath = "C:/000-AI-DIR/provisionning/gatoDataProvisionning.properties";

		Properties properties = null;
		FileInputStream fileInputStream = new FileInputStream(propertiesPath);
		properties = new Properties();
		// logger.debug("Chargement des propri�t�s :" + propertiesPath);

		try {
			properties.load(fileInputStream);
			// logger.debug("fichier : " + propertiesPath);
		} catch (IOException e) {
			// logger.error("Erreur sur le chargement des propri�t�s "+
			// propertiesPath);
			throw e;
		} finally {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException ioe) {
					// logger.error("Erreur lors de la fermeture du fichier de propri�t�s "+
					// propertiesPath);
				}
			}
		}
		return properties;
	}

	private static String returnStringDate() {
		return new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
	}

}
