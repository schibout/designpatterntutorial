package fr.sfr.impact.importer;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract super-class for all importers.
 *
 * @author Michal Bachman
 */
public abstract class Importer {

    private static final Logger LOG = Logger.getLogger(Importer.class);

    private static final String CSV = ".csv";

    private String rootDirectory;

    /**
     * Find all the CSV files in the directory relevant to the concrete importer and process them, one by one.
     */
    public void importData() {
        String directory = rootDirectory + getDirectory();
        for (final String fileName : getCsvFilesInDirectory(directory)) {
            final String fullFileName = directory + fileName;
            LOG.info("*** Processing file " + fullFileName + " ***");
            processFile(new File(fullFileName));
            LOG.info("*** Finished processing file " + fullFileName + " ***");
        }
    }

    /**
     * Get the subdirectory where this implementation of importer looks for CSV files.
     *
     * @return directory path, relative to the rootDirectory.
     */
    protected abstract String getDirectory();

    /**
     * Process a file.
     *
     * @param file to process.
     */
    protected abstract void processFile(File file);


    /**
     * List all CSV files in a directory.
     *
     * @param dirPath to list files from.
     * @return list of file names.
     */
    private static List<String> getCsvFilesInDirectory(String dirPath) {
        List<String> result = new ArrayList<String>();

        File dir = new File(dirPath);
        if (dir.exists()) {
            for (String fileName : dir.list()) {
                if (fileName.endsWith(CSV)) {
                    result.add(fileName);
                }
            }
        } else {
            LOG.warn("Directory " + dir.toString() + " does not exist!");
        }

        return result;
    }

    @Required
    public void setRootDirectory(String rootDirectory) {
        this.rootDirectory = rootDirectory;
    }
}
