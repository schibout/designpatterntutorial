package fr.sfr.impact.test;

import org.neo4j.graphdb.*;

public class TransactionalGraphDataBuilder extends GraphDataBuilder {

    public TransactionalGraphDataBuilder(GraphDatabaseService graphDb) {
        super(graphDb);
    }

    public Node createNodeWithProps(String... props) {
        Node node;

        Transaction tx = graphDb.beginTx();
        try {
            node = super.createNodeWithProps(props);
            tx.success();
        } catch (Exception e) {
            tx.failure();
            throw new RuntimeException(e);
        } finally {
            tx.finish();
        }

        return node;
    }

    @Override
    public void deleteNode(Node node) {
        Transaction tx = graphDb.beginTx();
        try {
            super.deleteNode(node);
            tx.success();
        } catch (Exception e) {
            tx.failure();
            throw new RuntimeException(e);
        } finally {
            tx.finish();
        }
    }

    public Relationship createRelationship(Node fromNode, Node toNode, RelationshipType relationshipType, Object... props) {
        Relationship relationship;

        Transaction tx = graphDb.beginTx();
        try {
            relationship = super.createRelationship(fromNode, toNode, relationshipType, props);
            tx.success();
        } finally {
            tx.finish();
        }

        return relationship;
    }
}
