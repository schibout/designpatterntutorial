package fr.sfr.impact.test;

import fr.sfr.impact.domain.search.SearchCriteria;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.helpers.collection.MapUtil;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static fr.sfr.impact.domain.Constants.*;

public class GraphDataBuilder {

    protected final GraphDatabaseService graphDb;

    protected final Set<String> indexedProperties = new HashSet<String>(Arrays.asList(ID, NAME, TYPE));

    public GraphDataBuilder(GraphDatabaseService graphDb) {
        this.graphDb = graphDb;
    }

    public void reset() {
        //((ImpermanentGraphDatabase) graphDb).cleanContent();
    }

    public void shutdown() {
        this.graphDb.shutdown();
    }

    public Node createNode(String id, String name, String type) {
        return createNodeWithProps(ID, id, NAME, name, TYPE, type);
    }

    public Node createAbstractNode(String id, String name, String type) {
        return createNodeWithProps(ID, id, NAME, name, TYPE, type, TYPO, ABSTRACT);
    }

    public Node createNodeWithProps(String... props) {
        Node node = graphDb.createNode();

        Map<String, String> propsMap = MapUtil.stringMap(props);
        for (String key : propsMap.keySet()) {
            node.setProperty(key, propsMap.get(key));
            if (indexedProperties.contains(key)) {
                graphDb.index().forNodes(SFR_NODES_INDEX).add(node, key, SearchCriteria.encodeForSearch(propsMap.get(key)));
            }

        }

        return node;
    }

    public void deleteNode(Node node) {
        for (Relationship r : node.getRelationships()) {
            r.delete();
        }
        node.delete();
    }

    public Relationship createRelationship(Node fromNode, Node toNode, RelationshipType relationshipType, Object... props) {
        Relationship relationship;

        relationship = fromNode.createRelationshipTo(toNode, relationshipType);
        Map<String, Object> propsMap = MapUtil.genericMap(props);
        for (String key : propsMap.keySet()) {
            relationship.setProperty(key, propsMap.get(key));
        }

        return relationship;
    }
}
