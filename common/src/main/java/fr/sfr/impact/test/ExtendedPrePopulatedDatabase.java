package fr.sfr.impact.test;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.index.IndexProvider;
import org.neo4j.kernel.KernelExtension;
import org.neo4j.kernel.impl.cache.CacheProvider;

import java.util.Map;

import static fr.sfr.impact.domain.Constants.*;
import static fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes.EST_AFFECTE_A;
import static fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes.EST_COMPOSE_DE;

public class ExtendedPrePopulatedDatabase extends PrePopulatedDatabase {

    public ExtendedPrePopulatedDatabase(Map<String, String> params) {
        super(params);
    }

    public ExtendedPrePopulatedDatabase(Map<String, String> params, Iterable<IndexProvider> indexProviders, Iterable<KernelExtension> kernelExtensions, Iterable<CacheProvider> cacheProviders) {
        super(params, indexProviders, kernelExtensions, cacheProviders);
    }

    public ExtendedPrePopulatedDatabase() {
    }

    @Override
    protected void populateDatabase() {
        super.populateDatabase();

        Node serviceWithoutCircuit = createNode(1000, SERVICE, SERVICE);
        createRelationship(serviceWithoutCircuit, node("A Client", 1), EST_AFFECTE_A);
        createRelationship(serviceWithoutCircuit, node(CONDUIT, 7), EST_COMPOSE_DE);

        /**
         * Strange setup:
         * 1) abstract node with no backups
         * 2) circuit with only one extremity, which does not have a port and card
         */
        Node clientWithStrangeSetup = createNode(1005, CLIENT, CLIENT);
        Node service = createNode(1002, SERVICE, SERVICE);
        Node circuit = createNode(1004, CIRCUIT, CIRCUIT);
        Node abstractWithoutBackups = createAbstractNode(1006, ROUTECIRCUIT, ROUTECIRCUIT);
        Node auWithoutPortAndCard = createNode(1001, AU4, AU4);
        createRelationship(service, clientWithStrangeSetup, EST_AFFECTE_A);
        createRelationship(service, circuit, EST_COMPOSE_DE);
        createRelationship(circuit, abstractWithoutBackups, EST_COMPOSE_DE);
        createRelationship(abstractWithoutBackups, node(CONDUIT, 7), EST_COMPOSE_DE);
        createRelationship(circuit, auWithoutPortAndCard, EST_COMPOSE_DE);

        //one more extremity for circuit3
        createRelationship(node(CIRCUIT, 3), auWithoutPortAndCard, EST_COMPOSE_DE);

        //client directly connected to au4
        Node veryStrangeClient = createNode(1007, CLIENT, CLIENT);
        createRelationship(veryStrangeClient, node(AU4, 21), EST_COMPOSE_DE);
    }
}
