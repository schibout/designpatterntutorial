package fr.sfr.impact.test;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.index.IndexProvider;
import org.neo4j.kernel.KernelExtension;
import org.neo4j.kernel.impl.cache.CacheProvider;
import org.neo4j.test.ImpermanentGraphDatabase;

import java.util.HashMap;
import java.util.Map;

import static fr.sfr.impact.domain.Constants.*;
import static fr.sfr.impact.domain.ImpactAnalysisRelationshipTypes.*;

public class PrePopulatedDatabase extends ImpermanentGraphDatabase {

    public static final String MAIN = "Main";
    public static final String SPARE = "SPARE";
    public static final String ROUTECIRCUIT = "ROUTECIRCUIT";

    private GraphDataBuilder b;
    private final Map<String, Node> nodesByName = new HashMap<String, Node>();

    public PrePopulatedDatabase(Map<String, String> params) {
        super(params);
    }

    public PrePopulatedDatabase(Map<String, String> params, Iterable<IndexProvider> indexProviders, Iterable<KernelExtension> kernelExtensions, Iterable<CacheProvider> cacheProviders) {
        super(params, indexProviders, kernelExtensions, cacheProviders);
    }

    public PrePopulatedDatabase() {
    }

    public final void populate() {
        b = new TransactionalGraphDataBuilder(this);
        populateDatabase();
    }

    public final Node node(String name, long id) {
        return nodesByName.get(name + id);
    }

    protected void populateDatabase() {
        createNode(15, EQUIPMENT, EQUIPMENT);
        createInEquipment(15, 16, 17, 18);
        createInEquipment(15, 14, 13, 11);
        createInEquipment(15, 19, 20, 21);

        createNode(24, EQUIPMENT, EQUIPMENT);
        createInEquipment(24, 23, 22, 12);
        createInEquipment(24, 25, 26, 27);

        createNode(44, EQUIPMENT, EQUIPMENT);
        createInEquipment(44, 40, 39, 38);
        createInEquipment(44, 41, 42, 43);
        createInEquipment(44, 67, 68, 69);
        createInEquipment(44, 67, 54, 55);
        createInEquipment(44, 40, 71, 72);

        createNode(31, EQUIPMENT, EQUIPMENT);
        createInEquipment(31, 30, 29, 28);
        createInEquipment(31, 35, 36, 37);
        createInEquipment(31, 32, 33, 34);
        createInEquipment(31, 63, 64, 65);
        createInEquipment(31, 56, 57, 58);
        createInEquipment(31, 73, 74, 75);

        createConduit(7, 11, 12);
        createConduit(8, 27, 28);
        createConduit(9, 21, 43);
        createConduit(10, 38, 37);
        createConduit(52, 55, 58);
        createConduit(66, 72, 75);

        Node abstractConduit = createAbstractNode(51, CONDUIT, CONDUIT);
        b.createRelationship(abstractConduit, node(CONDUIT, 10), EST_COMPOSE_DE);
        b.createRelationship(abstractConduit, node(CONDUIT, 52), EST_COMPOSE_DE);
        Node conduit = createNode(50, CONDUIT, CONDUIT);
        b.createRelationship(conduit, abstractConduit, EST_COMPOSE_DE);

        createRouteCircuit(5, MAIN, 7, 8);
        createRouteCircuit(6, SPARE, 9, 10);
        createRouteCircuit(60, MAIN, 50);
        createRouteCircuit(62, SPARE, 66);

        Node abstract4 = createAbstractRouteCircuit(4, 5, 6);
        Node abstract61 = createAbstractRouteCircuit(61, 60, 62);

        Node circuit3 = createNode(3, CIRCUIT, CIRCUIT);
        b.createRelationship(circuit3, abstract4, EST_COMPOSE_DE);
        b.createRelationship(circuit3, node(AU4, 18), EST_COMPOSE_DE);
        b.createRelationship(circuit3, node(AU4, 34), EST_COMPOSE_DE);

        Node circuit63 = createNode(63, CIRCUIT, CIRCUIT);
        b.createRelationship(circuit63, abstract61, EST_COMPOSE_DE);
        b.createRelationship(circuit63, node(AU4, 69), EST_COMPOSE_DE);
        b.createRelationship(circuit63, node(AU4, 65), EST_COMPOSE_DE);

        Node service2 = createNode(2, "ZZ Service", SERVICE);
        b.createRelationship(service2, circuit3, EST_COMPOSE_DE);

        Node service80 = createNode(80, SERVICE, SERVICE);
        b.createRelationship(service80, circuit63, EST_COMPOSE_DE);

        Node client1 = createNode(1, "A Client", CLIENT);
        Node client100 = createNode(100, CLIENT, CLIENT);
        b.createRelationship(service2, client1, EST_AFFECTE_A);
        b.createRelationship(service2, client100, EST_AFFECTE_A);
        b.createRelationship(service80, client100, EST_AFFECTE_A);
    }

    protected final Node createAbstractRouteCircuit(long id, long mainId, long spareId) {
        Node abstractNode = createAbstractNode(id, ROUTECIRCUIT, ROUTECIRCUIT);
        b.createRelationship(abstractNode, node(ROUTECIRCUIT + MAIN, mainId), EST_COMPOSE_DE, PARTAGE, PRIMARY);
        b.createRelationship(abstractNode, node(ROUTECIRCUIT + SPARE, spareId), EST_COMPOSE_DE, PARTAGE, SECONDARY);
        return abstractNode;
    }

    protected final void createRouteCircuit(long id, String mainSpare, long... conduitIds) {
        Node node = createNode(id, ROUTECIRCUIT + mainSpare, ROUTECIRCUIT);
        for (long conduitId : conduitIds) {
            b.createRelationship(node, node(CONDUIT, conduitId), EST_COMPOSE_DE);
        }
    }

    protected final void createConduit(long conduitId, long au4A, long au4B) {
        Node conduit = createNode(conduitId, CONDUIT, CONDUIT);
        b.createRelationship(conduit, node(AU4, au4A), EST_COMPOSE_DE);
        b.createRelationship(conduit, node(AU4, au4B), EST_COMPOSE_DE);
    }

    protected final void createInEquipment(long equipmentId, long cardId, long portId, long au4Id) {
        Node equipment = node(EQUIPMENT, equipmentId);
        Node card = node(CARD, cardId);
        if (card == null) {
            card = createNode(cardId, CARD, CARD);
            b.createRelationship(equipment, card, EST_CONNECTE_A);
            b.createRelationship(card, equipment, EST_INCLUS_DANS);
            b.createRelationship(card, equipment, EST_CONNECTE_A);
        }
        Node port = node(PORT, portId);
        if (port == null) {
            port = createNode(portId, PORT, PORT);
            b.createRelationship(port, card, EST_INCLUS_DANS);
            b.createRelationship(port, card, EST_CONNECTE_A);
            b.createRelationship(card, port, EST_CONNECTE_A);
        }
        Node au4 = node(AU4, au4Id);
        if (au4 == null) {
            au4 = createNode(au4Id, AU4, AU4);
            b.createRelationship(au4, port, EST_INCLUS_DANS);
            b.createRelationship(au4, port, EST_CONNECTE_A);
            b.createRelationship(port, au4, EST_CONNECTE_A);
        }
    }

    protected final Node createNode(long id, String name, String type) {
        Node node = b.createNode(String.valueOf(id), name + id, type);
        nodesByName.put(name + id, node);
        return node;
    }

    protected final Node createAbstractNode(long id, String name, String type) {
        Node node = b.createAbstractNode(String.valueOf(id), name + id, type);
        nodesByName.put(name + id, node);
        return node;
    }

    protected final Relationship createRelationship(Node fromNode, Node toNode, RelationshipType relationshipType, Object... props) {
        return b.createRelationship(fromNode, toNode, relationshipType, props);
    }
}
