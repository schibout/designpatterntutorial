package fr.sfr.impact.dto;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Subset of {@link NodeDto}s, usually for paged results.
 *
 * @author Michal Bachman
 */
public class NodeDtoSubset {

    private final List<NodeDto> dtos = new LinkedList<NodeDto>();
    private int totalNumber;

    public NodeDtoSubset() {
    }

    public NodeDtoSubset(int totalNumber) {
        this.totalNumber = totalNumber;
    }

    public void addDto(NodeDto nodeDto) {
        dtos.add(nodeDto);
    }

    public void setTotalNumber(int totalNumber) {
        this.totalNumber = totalNumber;
    }

    public List<NodeDto> getDtos() {
        return Collections.unmodifiableList(dtos);
    }

    /**
     * Get the total number of results that would be held if this wasn't a subset (i.e. if there was no paging).
     *
     * @return all records available.
     */
    public int getTotalNumber() {
        return totalNumber;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NodeDtoSubset that = (NodeDtoSubset) o;

        if (totalNumber != that.totalNumber) return false;
        if (!dtos.equals(that.dtos)) return false;

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = dtos.hashCode();
        result = 31 * result + totalNumber;
        return result;
    }
}
