package fr.sfr.impact.dto;

import fr.sfr.impact.domain.Constants;
import org.apache.commons.lang.StringUtils;
import org.neo4j.graphdb.Node;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * DTO for Neo4J node details.
 *
 * @author Michal Bachman
 */
public class NodeDto implements Dto {

    private long neoId;
    private final Map<String, String> properties = new HashMap<String, String>();
    private Set<String> impactingNodes = Collections.emptySet();

    public NodeDto() {
    }

    /**
     * Construct the DTO from a node.
     *
     * @param node to construct the DTO from.
     */
    public NodeDto(Node node) {
        this.neoId = node.getId();
        populatePropertyFromNode(Constants.ID, node);
        populatePropertyFromNode(Constants.NAME, node);
        populatePropertyFromNode(Constants.TYPO, node);
        populatePropertyFromNode(Constants.TYPE, node);
        populatePropertyFromNode(Constants.SOURCE, node);
        populatePropertyFromNode(Constants.TECHNO, node);
        populatePropertyFromNode(Constants.EQ_TYPE, node);
        populatePropertyFromNode(Constants.CAPACITY, node);
        populatePropertyFromNode(Constants.LEVEL, node);
    }

    private void populatePropertyFromNode(String property, Node node) {
        if (node.hasProperty(property)) {
            setProperty(property, node.getProperty(property).toString());
        }
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public String getProperty(String key) {
        if (!properties.containsKey(key)) {
            return StringUtils.EMPTY;
        }
        return properties.get(key);
    }

    public NodeDto setProperty(String key, String value) {
        properties.put(key, value);
        return this;
    }

    public NodeDto setNameAndNeoId(String propertyName, String aOrB, String... nameAndNeoId) {
        setProperty(propertyName + Constants.U_NAME + aOrB, nameAndNeoId[0]);
        setProperty(propertyName + Constants.U_NEO_ID + aOrB, nameAndNeoId[1]);
        return this;
    }

    public NodeDto setIdAndNeoId(String propertyName, String aOrB, String... idAndNeoId) {
        setProperty(propertyName + Constants.U_ID + aOrB, idAndNeoId[0]);
        setProperty(propertyName + Constants.U_NEO_ID + aOrB, idAndNeoId[1]);
        return this;
    }

    public NodeDto setNameAndNeoId(String propertyName, String... nameAndNeoId) {
        return setNameAndNeoId(propertyName, StringUtils.EMPTY, nameAndNeoId);
    }

    public long getNeoId() {
        return neoId;
    }

    public Set<String> getImpactingNodes() {
        return impactingNodes;
    }

    public void setImpactingNodes(Set<String> impactingNodes) {
        this.impactingNodes = Collections.unmodifiableSet(impactingNodes);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NodeDto nodeDto = (NodeDto) o;

        if (neoId != nodeDto.neoId) return false;
        if (!properties.equals(nodeDto.properties)) return false;

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = (int) (neoId ^ (neoId >>> 32));
        result = 31 * result + properties.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "NodeDto{" +
                "neoId=" + neoId +
                ", properties=" + properties +
                '}';
    }
}
