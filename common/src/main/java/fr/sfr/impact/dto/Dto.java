package fr.sfr.impact.dto;

import java.io.Serializable;

/**
 * Marker interface for DTOs.
 *
 * @author Michal Bachman
 */
public interface Dto extends Serializable {
}
