package fr.sfr.impact.domain;

import org.neo4j.graphdb.RelationshipType;

// Relations types
public enum ImpactAnalysisRelationshipTypes implements RelationshipType {
    EST_INCLUS_DANS, EST_COMPOSE_DE, EST_CONNECTE_A, EST_AFFECTE_A
}
