package fr.sfr.impact.domain;

/**
 * Application constants.
 */
public final class Constants {

    private Constants() {
    }

    public static final String SFR_NODES_INDEX = "SFR_NODES_INDEX";

    public static final String ID = "ID";
    public static final String NAME = "NAME";
    public static final String TYPE = "TYPE";
    public static final String LEVEL = "LEVEL";
    public static final String SOURCE = "SOURCE";
    public static final String TYPO = "TYPO";
    public static final String EQ_TYPE = "EQ_TYPE";
    public static final String TECHNO = "TECHNO";
    public static final String CAPACITY = "CAPACITY";

    public static final String SITE = "SITE";
    public static final String SERVICE = "SERVICE";
    public static final String CIRCUIT = "CIRCUIT";
    public static final String CONDUIT = "CONDUIT";
    public static final String CLIENT = "CLIENT";
    public static final String AU4 = "AU4";
    public static final String CARD = "CARD";
    public static final String PORT = "PORT";
    public static final String EQUIPMENT = "EQUIPMENT";

    public static final String U_ID = "_ID";
    public static final String U_NAME = "_NAME";
    public static final String U_NEO_ID = "_NEO_ID";
    public static final String EXTREMITY = "EXTREMITY";
    public static final String A = "_A";
    public static final String B = "_B";

    public static final String UTF8 = "UTF-8";

    public static final String ABSTRACT = "ABSTRACT";
    public static final String PARTAGE = "PARTAGE";
    public static final String PRIMARY = "PRIMAIRE";
    public static final String SECONDARY = "SECONDAIRE";
    public static final String STATUS = "STATUS";
}
