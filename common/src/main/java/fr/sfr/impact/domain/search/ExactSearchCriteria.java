package fr.sfr.impact.domain.search;

import org.apache.lucene.queryParser.QueryParser;

/**
 * Search criteria matching the search terms exactly (literally). This means the search terms are completely escaped
 * including the "*" character; thus, wildcard searches will not work. This is intended for the importer.
 * <p/>
 * Note: Some nodes contain "*" in their names. The consequence of this is:
 * If I have two nodes: ABCDEF and ABC*EF, then when I search for 'ABC*EF' using this search criteria, I will only get the second one.
 * This is different to {@link SearchCriteria}.
 *
 * @author Michal Bachman
 */
public class ExactSearchCriteria extends SearchCriteria {

    public ExactSearchCriteria() {
    }

    /**
     * Construct a search by ID.
     *
     * @param id to search for.
     */
    public ExactSearchCriteria(String id) {
        super(id, null, null, null, null, null, null);
    }

    /**
     * {@inheritDoc}
     */
    public ExactSearchCriteria(String id, String name, String type, String source, String techno, String equipmentType, String capacity) {
        super(id, name, type, source, techno, equipmentType, capacity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String escape(String s) {
        return QueryParser.escape(s);
    }
}
