package fr.sfr.impact.domain.search;

import fr.sfr.impact.domain.Constants;
import org.apache.commons.lang.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Encapsulates all the search fields and builds the Lucene search query.
 * When building the query, it escapes all the characters that Lucene needs to escape, except for "*", which is used as a wildcard.
 *
 * Note: Some nodes contain "*" in their names. The consequence of this is:
 * If I have two nodes: ABCDEF and ABC*EF, then when I search for 'ABC*EF' using this search criteria, I will get both. This is different to {@link ExactSearchCriteria}.
 *
 * @author Michal Bachman
 */
public class SearchCriteria {
    private static final String COLON = ":";
    private static final String ASTERIX = "*";

    private String id;
    private String name;
    private String type;
    private String source;
    private String techno;
    private String equipmentType;
    private String capacity;

    /**
     * Construct empty search criteria.
     */
    public SearchCriteria() {
    }

    /**
     * Construct populated search criteria.
     *
     * @param id   ID.
     * @param name name.
     * @param type type.
     */
    public SearchCriteria(String id, String name, String type, String source, String techno, String equipmentType, String capacity) {
        setId(id);
        setName(name);
        setType(type);
        setSource(source);
        setTechno(techno);
        setEquipmentType(equipmentType);
        setCapacity(capacity);
    }

    /**
     * Build the Lucene query from this criteria. The search terms are URL encoded using UTF-8.
     *
     * @return query.
     */
    public String buildQuery() {
        String result = StringUtils.EMPTY;
        result = addSearchTerm(result, Constants.ID, id);
        result = addSearchTerm(result, Constants.NAME, name);
        result = addSearchTerm(result, Constants.TYPE, type);
        result = addSearchTerm(result, Constants.SOURCE, source);
        result = addSearchTerm(result, Constants.TECHNO, techno);
        result = addSearchTerm(result, Constants.EQ_TYPE, equipmentType);
        result = addSearchTerm(result, Constants.CAPACITY, capacity);
        result = addDefaultIfBlank(result);
        return result;
    }

    /**
     * Add a search term to the query.
     *
     * @param query      to add to.
     * @param fieldName  name of the field to search for.
     * @param fieldValue value of the field to search for.
     * @return amended query.
     */
    private String addSearchTerm(String query, String fieldName, String fieldValue) {
        if (StringUtils.isNotBlank(fieldValue)) {
            if (StringUtils.isNotBlank(query)) {
                query += " AND ";
            }
            query += fieldName + COLON + escape(encodeForSearch(fieldValue));
        }
        return query;
    }

    /**
     * Add a default query if the query if blank.
     *
     * @param query to add to.
     * @return amended query.
     */
    private String addDefaultIfBlank(String query) {
        if (StringUtils.isBlank(query)) {
            query = Constants.ID + COLON + ASTERIX;
        }
        return query;
    }

    /**
     * Encode a term for searching. This URL encodes the term using UTF-8.
     *
     * @param toEncode string to encode.
     * @return encoded string.
     */
    public static String encodeForSearch(String toEncode) {
        try {
            return URLEncoder.encode(toEncode.toUpperCase(), Constants.UTF8);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns a String where those characters that QueryParser
     * expects to be escaped are escaped by a preceding <code>\</code>.
     */
    protected String escape(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            // These characters are part of the query syntax and must be escaped
            if (c == '\\' || c == '+' || c == '-' || c == '!' || c == '(' || c == ')' || c == ':'
                    || c == '^' || c == '[' || c == ']' || c == '\"' || c == '{' || c == '}' || c == '~'
                    || c == '?' || c == '|' || c == '&') {
                sb.append('\\');
            }
            sb.append(c);
        }
        return sb.toString();
    }

    //Getters and setters

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getSource() {
        return source;
    }

    public String getTechno() {
        return techno;
    }

    public String getEquipmentType() {
        return equipmentType;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setId(String id) {
        if (id != null) {
            this.id = id.trim();
        } else {
            this.id = StringUtils.EMPTY;
        }
    }

    public void setName(String name) {
        if (name != null) {
            this.name = name.trim();
        } else {
            this.name = StringUtils.EMPTY;
        }
    }

    public void setType(String type) {
        if (type != null) {
            this.type = type.trim();
        } else {
            this.type = StringUtils.EMPTY;
        }
    }

    public void setSource(String source) {
        if (source != null) {
            this.source = source.trim();
        } else {
            this.source = StringUtils.EMPTY;
        }
    }

    public void setTechno(String techno) {
        if (techno != null) {
            this.techno = techno.trim();
        } else {
            this.techno = StringUtils.EMPTY;
        }
    }

    public void setEquipmentType(String equipmentType) {
        if (equipmentType != null) {
            this.equipmentType = equipmentType.trim();
        } else {
            this.equipmentType = StringUtils.EMPTY;
        }
    }

    public void setCapacity(String capacity) {
        if (capacity != null) {
            this.capacity = capacity.trim();
        } else {
            this.capacity = StringUtils.EMPTY;
        }
    }
}
