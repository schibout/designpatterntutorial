package fr.sfr.impact.domain.search;

import java.io.Serializable;

/**
 * Class encapsulating paging information.
 *
 * @author Michal Bachman
 */
public class PagingCriteria implements Serializable {
    public static final PagingCriteria EVERYTHING = new PagingCriteria(1, Integer.MAX_VALUE);
    public static final PagingCriteria SINGLE = new PagingCriteria(1, 1);

    private int size = 10; //page size
    private int page = 1; //page number starting with 1

    /**
     * Create new paging criteria with default values.
     */
    public PagingCriteria() {
    }

    /**
     * Create new paging criteria.
     *
     * @param page page number.
     * @param size page size.
     */
    public PagingCriteria(int page, int size) {
        this.size = size;
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    /**
     * What is the index of the first record that we display? Assumes a 0-indexed result list.
     *
     * @return index of the first element.
     */
    public int getFirst() {
        return (page - 1) * size;
    }

    /**
     * What is the index of the last record that we display? Assumes a 0-indexed result list.
     *
     * @return index of the last element.
     */
    public int getLast() {
        return getFirst() + size - 1;
    }

    /**
     * What is the number of pages, given this criteria and the total number of results to be paged?
     *
     * @param totalRecords number of results to be paged.
     * @return number of pages.
     */
    public int getNumberOfPages(int totalRecords) {
        float nrOfPages = (float) totalRecords / getSize();
        return (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages);
    }
}
