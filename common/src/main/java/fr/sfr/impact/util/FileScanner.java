package fr.sfr.impact.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Utility class that converts a various inputs (files, byte arrays,...) into a list of lines.
 *
 * @author Michal Bachman
 */
public final class FileScanner {

    private FileScanner() {
    }

    /**
     * Produce lines from a file.
     *
     * @param file to produce lines from.
     * @return lines.
     */
    public static List<String> produceLines(File file) {
        try {
            return produceLines(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Produce lines from a byte array.
     *
     * @param bytes to produce lines from.
     * @return lines.
     */
    public static List<String> produceLines(byte[] bytes) {
        return produceLines(new ByteArrayInputStream(bytes));
    }

    /**
     * Produce lines from an input stream.
     *
     * @param inputStream to produce lines from.
     * @return lines.
     */
    public static List<String> produceLines(InputStream inputStream) {
        final List<String> lines = new ArrayList<String>();

        Scanner scanner = new Scanner(inputStream, "ISO-8859-1");
        while (scanner.hasNextLine()) {
            lines.add(scanner.nextLine());
        }
        scanner.close();

        return lines;
    }
}
