package fr.sfr.impact.service.search;

import fr.sfr.impact.domain.Constants;
import fr.sfr.impact.domain.search.ExactSearchCriteria;
import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.domain.search.SearchCriteria;
import fr.sfr.impact.dto.NodeDto;
import fr.sfr.impact.dto.NodeDtoSubset;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.index.IndexHits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Default production implementation of {@link SearchService}.
 *
 * @author Michal Bachman
 */
@Service
public class DefaultSearchService implements SearchService {

    @Autowired
    private GraphDatabaseService database;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean nodeExists(String nodeId) {
        return nodeIndexHits(new ExactSearchCriteria(nodeId)).size() > 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Node findSingleNode(String nodeId) {
        return nodeIndexHits(new ExactSearchCriteria(nodeId)).getSingle();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDtoSubset search(SearchCriteria searchCriteria, PagingCriteria pagingCriteria) {
        IndexHits<Node> indexHits = nodeIndexHits(searchCriteria);
        NodeDtoSubset result = new NodeDtoSubset(indexHits.size());

        int iteratedMatches = 0;
        while (indexHits.hasNext() && iteratedMatches <= pagingCriteria.getLast()) {
            Node next = indexHits.next();
            if (iteratedMatches >= pagingCriteria.getFirst()) {
                result.addDto(new NodeDto(next));
            }

            iteratedMatches++;
        }

        return result;
    }

    private IndexHits<Node> nodeIndexHits(SearchCriteria searchCriteria) {
        return database.index().forNodes(Constants.SFR_NODES_INDEX).query(searchCriteria.buildQuery());
    }

    void setDatabase(GraphDatabaseService database) {
        this.database = database;
    }
}
