package fr.sfr.impact.service.search;

import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.domain.search.SearchCriteria;
import fr.sfr.impact.dto.NodeDtoSubset;
import org.neo4j.graphdb.Node;

/**
 * Service that allows searching for nodes.
 *
 * @author Michal Bachman
 */
public interface SearchService {

    /**
     * Find out whether a node exists.
     *
     * @param nodeId ID of the Node (SFR ID, not Neo4J ID).
     * @return true if and only if the node exists.
     */
    boolean nodeExists(String nodeId);

    /**
     * Returns the first and only Node, or {@code null} if there was none.
     * If there were more than one Node in the result a
     * {@link java.util.NoSuchElementException} will be thrown.
     *
     * @return the first and only Node, or {@code null} if none.
     */
    Node findSingleNode(String nodeId);

    /**
     * Perform a search.
     *
     * @param searchCriteria encapsulating what to search for.
     * @param pagingCriteria encapsulating paging information.
     * @return search result, encapsulating a subset of results (based on paging criteria) and the total number of results that matched the search.
     */
    NodeDtoSubset search(SearchCriteria searchCriteria, PagingCriteria pagingCriteria);
}
