package fr.sfr.impact.service.search;

import fr.sfr.impact.domain.Constants;
import fr.sfr.impact.domain.search.PagingCriteria;
import fr.sfr.impact.domain.search.SearchCriteria;
import fr.sfr.impact.dto.NodeDtoSubset;
import fr.sfr.impact.service.search.DefaultSearchService;
import fr.sfr.impact.test.GraphDataBuilder;
import fr.sfr.impact.test.TransactionalGraphDataBuilder;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.test.ImpermanentGraphDatabase;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

public class DefaultSearchServiceTest {

    private static DefaultSearchService testedClass;
    private static GraphDatabaseService database;

    @BeforeClass
    public static void setUp() throws IOException {
        testedClass = new DefaultSearchService();
        database = new ImpermanentGraphDatabase();
        testedClass.setDatabase(database);
        populateDatabase();
    }

    @AfterClass
    public static void tearDown() throws IOException {
        database.shutdown();
    }

    @Test
    public void searchByIdPositive() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("*est*", "", "",  null, null, null, null), new PagingCriteria());
        Assert.assertEquals(1, result.getDtos().size());
        Assert.assertEquals(1, result.getTotalNumber());
        Assert.assertEquals("TestId1", result.getDtos().get(0).getProperty(Constants.ID));
    }

    @Test
    public void searchByIdPositive2() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("*stId1", "", "", null, null, null, null), new PagingCriteria());
        Assert.assertEquals(1, result.getDtos().size());
        Assert.assertEquals(1, result.getTotalNumber());
        Assert.assertEquals("TestId1", result.getDtos().get(0).getProperty(Constants.ID));
    }

    @Test
    public void searchByIdPositive3() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("TestId1", "", "", null, null, null, null), new PagingCriteria());
        Assert.assertEquals(1, result.getDtos().size());
        Assert.assertEquals(1, result.getTotalNumber());
        Assert.assertEquals("TestId1", result.getDtos().get(0).getProperty(Constants.ID));
    }

    @Test
    public void searchByIdNegative() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("*est1", "", "", null, null, null, null), new PagingCriteria());
        assertTrue(result.getDtos().isEmpty());
        Assert.assertEquals(0, result.getTotalNumber());
    }

    @Test
    public void searchByNamePositive() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("", "*est*", "", null, null, null, null), new PagingCriteria());
        Assert.assertEquals(1, result.getDtos().size());
        Assert.assertEquals(1, result.getTotalNumber());
        Assert.assertEquals("TestId1", result.getDtos().get(0).getProperty(Constants.ID));
    }

    @Test
    public void searchByNameNegative() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("", "*est1", "", null, null, null, null), new PagingCriteria());
        assertTrue(result.getDtos().isEmpty());
        Assert.assertEquals(0, result.getTotalNumber());
    }

    @Test
    public void searchByTypePositive() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("", "", "test*", null, null, null, null), new PagingCriteria());
        Assert.assertEquals(1, result.getDtos().size());
        Assert.assertEquals(1, result.getTotalNumber());
        Assert.assertEquals("TestId1", result.getDtos().get(0).getProperty(Constants.ID));
    }

    @Test
    public void searchByTypeNegative() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("", "", "*est1*", null, null, null, null), new PagingCriteria());
        assertTrue(result.getDtos().isEmpty());
        Assert.assertEquals(0, result.getTotalNumber());
    }

    @Test
    public void searchByIdAndNamePositive() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("*est*", "T*", "", null, null, null, null), new PagingCriteria());
        Assert.assertEquals(1, result.getDtos().size());
        Assert.assertEquals(1, result.getTotalNumber());
        Assert.assertEquals("TestId1", result.getDtos().get(0).getProperty(Constants.ID));
    }

    @Test
    public void searchByIdAndNameNegative() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("est1*", "T*", "", null, null, null, null), new PagingCriteria());
        assertTrue(result.getDtos().isEmpty());
        Assert.assertEquals(0, result.getTotalNumber());
    }

    @Test
    public void searchByIdAndTypePositive() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("Test*", "", "Test*", null, null, null, null), new PagingCriteria());
        Assert.assertEquals(1, result.getDtos().size());
        Assert.assertEquals(1, result.getTotalNumber());
        Assert.assertEquals("TestId1", result.getDtos().get(0).getProperty(Constants.ID));
    }

    @Test
    public void searchByIdAndTypeNegative() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("est1", "", "Test", null, null, null, null), new PagingCriteria());
        assertTrue(result.getDtos().isEmpty());
        Assert.assertEquals(0, result.getTotalNumber());
    }

    @Test
    public void searchByNameAndTypePositive() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("", "Te*", "T*", null, null, null, null), new PagingCriteria());
        Assert.assertEquals(1, result.getDtos().size());
        Assert.assertEquals(1, result.getTotalNumber());
        Assert.assertEquals("TestId1", result.getDtos().get(0).getProperty(Constants.ID));
    }

    @Test
    public void searchByNameAndTypeNegative() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("", "T*", "*2*", null, null, null, null), new PagingCriteria());
        assertTrue(result.getDtos().isEmpty());
        Assert.assertEquals(0, result.getTotalNumber());
    }

    @Test
    public void searchByAllPositive() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("Te*", "T*", "T*", null, null, null, null), new PagingCriteria());
        Assert.assertEquals(1, result.getDtos().size());
        Assert.assertEquals(1, result.getTotalNumber());
        Assert.assertEquals("TestId1", result.getDtos().get(0).getProperty(Constants.ID));
    }

    @Test
    public void searchByAllNegative() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("T*", "T*", "*2*", null, null, null, null), new PagingCriteria());
        assertTrue(result.getDtos().isEmpty());
        Assert.assertEquals(0, result.getTotalNumber());
    }

    @Test
    public void rootNodeIsNotReturned() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria(), new PagingCriteria());
        Assert.assertEquals(3, result.getDtos().size());
        Assert.assertEquals(3, result.getTotalNumber());
    }

    @Test
    public void pagingIsRespected1() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria(), new PagingCriteria(1, 1));
        Assert.assertEquals(1, result.getDtos().size());
        Assert.assertEquals(3, result.getTotalNumber());
        Assert.assertEquals("TestId1", result.getDtos().get(0).getProperty(Constants.ID));
    }

    @Test
    public void pagingIsRespected2() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria(), new PagingCriteria(2, 1));
        Assert.assertEquals(1, result.getDtos().size());
        Assert.assertEquals(3, result.getTotalNumber());
        Assert.assertEquals("AnotherId1", result.getDtos().get(0).getProperty(Constants.ID));
    }

    @Test
    public void testSearchingWithHash() {
        NodeDtoSubset result = testedClass.search(new SearchCriteria("*#*", "","", null, null, null, null), new PagingCriteria());
        Assert.assertEquals(1, result.getDtos().size());
        Assert.assertEquals("IdWith#Hash", result.getDtos().get(0).getProperty(Constants.ID));
    }

    private static void populateDatabase() {
        GraphDataBuilder b = new TransactionalGraphDataBuilder(database);
        b.createNode("TestId1", "TestName1", "TestType1");
        b.createNode("AnotherId1", "AnotherName1", "AnotherType1");
        b.createNode("IdWith#Hash", "NameWith#Hash", "TypeWith#Hash");
    }
}
