package fr.sfr.impact.util;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

public class FileScannerTest {

    @Test
    public void verifyCorrectNumberOfScannedLines() throws IOException {
        Assert.assertEquals(5000, FileScanner.produceLines(new ClassPathResource("node6.csv").getFile()).size());
    }
}
