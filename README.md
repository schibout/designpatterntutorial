# Impact Analysis

## List of Issues

1) DONE: more than 65k rows in excel
2) DONE: change job to look for IDs not Neo4J IDs
3) think about multiple start points for bottom-up
4) cosmetic things - logo, SFR colours
5) information about analysis in progress
6) on the front page, information about how many nodes etc.
7) change importer so that you can do one step at a time
8) red: service or circuit?
9) Somehow separate groups of results that belong together (4 lines per result!)
10) UI error handling, e.g. job ID does not exist

This is essential documentation for some of the topics that can't be clearly understood from reading the code and associated JavaDoc.

## Introduction

The purpose of the application is to bring together data from different ERP systems, connect them into a graph, and perform analysis on them. There are two types of analysis:
- Bottom-Up: answers the question which components, services and clients will be impacted and how, in case a part/component in the network fails.
- Top-Down: answers the question in the opposite direction, i.e. what parts are essential for a given part/service/client to be OK

The application with be used with IE8, so it should always be tested with that.

## Modules

- common: functionality shared by all other modules
- importer: classes that allow you to create a new database from scratch by importing data from CSV files
- app: web application for browsing data and performing analysis on them

## Importer

As a part of the
    mvn clean package
command, a .jar file with all dependencies will be created in the importer/target directory. The main class for this is ImportLauncher.java. It can be run by:

    java -Xms512m -Xmx1024m -jar importer.jar

It expects that in the same directory where this jar is, you will have the following structure. The file names do not matter as long as they have the .csv suffix.

    import.directory
     import.directory.nodes
       node0.csv
       node1.csv
       nodeX.csv
     import.directory.relationships
       relation0.csv
       relation1.csv
       relationX.csv
     import.directory.relationshipProperties
       prop0.csv
       prop1.csv
       propX.csv

This can be changed in importer-gato.properties at the end of the file by editing the following lines accordingly:

    import.directory=./input/GATO/GAYA
    import.directory.nodes=/node/
    import.directory.relationships=/relation/
    import.directory.relationshipProperties=/attributRelation/

The importer will generate the database in a directory called "imported-data" in the same directory as the .jar file, and the log will be in a directory called "log".

If it is desired to run the node, relationship, and relationship property imports separately, it can be done directly by running the main methods of _NodeImporter_, _RelationshipImporter_, and _RelationshipPropertiesImporter_, respectively, provided that the input directory with the right structure is present in the importer directory.

The rest of the classes in fr.sfr.impact.importer need to be consulted with Jean-Louis at SFR (OpenCredo didn't work on these classes).

## Web Application

### Setup

    mvn jetty:run-war
will run the application at localhost:8080/impact-analysis.

Before this can be done, you need to obtain a copy of the data (or generate it using the importer) and store it under impact-analysis/app/data. The first (and only) directory you should see there is graph.db. This data must not be committed to github for security reasons.

### Search

All the search screens are driven by a Lucene Wildcard search. The user can search for any combination of indexed node properties, using a "*" as the wildcard anywhere in the search term. What properties are indexed is defined in _DefaultImportService#importNodes_.

### Analysis

Computation of analysis for a node can take a while. Because the controllers are RESTful, results must be paged and we don't want to recalculate the whole thing for each page, the results are cached. See _CachingService_ for more details.

### Bottom-Up Analysis

Bottom-up analysis is the only business-logic heavy part of the application. This business logic is clearly JavaDoced in the classes in the "bottomup" packages. Parts of the logic deal with:
 - how we determine, whether a service for a client is impacted (KO), semi-impacted (Desecurise), or not impacted at all (OK, won't show up in the analysis)
 - how the data is presented

Typically, one would perform a bottom up analysis on a part of the network, such as an equipment, port, card, etc. The presentation of the analysis contains 3 tabs:
- All Impacted Clients
- All Impacted Services
- All Impacted parts (network elements)
- When selecting an Impacted Client, a list of impacted services for the client

Each list of services contains up to 5 lines per client-service combination.
* Impacted Client
* Impacted Service for the above client
* Impacted Circuit causing the above service to be impacted
* Main RouteCircuit for the above circuit (if exists)
* Spare RouteCircuit for the above circuit (if exists)

The existence of main/spare RouteCircuits determines, whether the service is down or only "désecurisé". Generally, when there is an impacted part which has a secondary link (backup) that isn't impacted, it is not secure. Otherwise it's down. The rules are different for client "DGAC", it is down even if there is a backup.

There is a notion of abstract nodes, which are virtual nodes not displayed to the user. They exist to indicate that their "children" are main/backup of the same link, rather than two separate links.

### Top-Down Analysis

Top-down analysis is a very simple traversal of certain relationships, results of which are presented on a screen.

### Security

SFR provided two .jar libs to integrate with SFR's LDAP (also referred to as AUI), which must be used. One is for production, the other one for testing. Both are located in the lib folder.

Since the project uses Maven, the lib folder is referenced as a repository in pom.xml. The libraries have to be Mavenized manually, should they ever get updated.

Because both the prod and testing libs have the same packages and class names, Maven profiles are used to determine, which one to use.

AuiAuthenticationProvider is a Spring Security extension that delegates to the provided libraries and translates their exceptions.

### "Travaux-Programmes" a.k.a. "Jobs"

There is a feature, whereby a user can upload a CSV file containing (on each line) a node ID, semicolon, and "BU" for bottom-up or "TD" for top-down. The result of this job should be a .zip file downloaded by the browser, with one excel sheet in it per node's analysis.

This is implemented in the "job" packages. Jobs are submitted and processed asynchronously, indicating progress back to the user (all RESTful, user GETs status of the job). Everything is done in memory, see _DefaultJobService_.
